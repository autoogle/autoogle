Dropzone.autoDiscover = false;
(function( $ ) {
  var url;
  var el;

  $.fn.assetManager = function(action,options) {
      var myDropzone = null;
      el = $(this);

      if(typeof(action) == 'undefined' || action == '') {
        el.data('opt',options);

        myDropzone = new Dropzone("form.dropzone");
        myDropzone.on("success", function(file,data) {
          loadAssets(1);
        });
        el.data('dz',myDropzone);

        $('.results-meta').on('click','a.btn',function(e) {
          e.preventDefault();
          loadAssets($(this).data('page'));
        });

        $('.results').on('click','.asset_image',function(e) {
          e.preventDefault();
          $('.results .asset_image').removeClass('selected');
          $(this).addClass('selected');
        });

        $('.btn-primary',el).click(function(e) {
          e.preventDefault();
          var res = $('.results .asset_image.selected').data('img');
          el.data('callback')(res);
          el.modal('hide');
        });
      }
      else if(action == 'show') {
        el.data('callback',options.callback);
        el.modal();
        loadAssets(1);
      }

      function loadAssets(pageNo) {
        $('.results .asset_image').fadeOut('slow');

        var dd = el.data('opt');
        console.log(dd.url+'/?page='+pageNo);
        $.getJSON(dd.url+'/?page='+pageNo,function(pge) {
          $('.results').html("");
          $('.results-meta').html("");

          $.each(pge.data,function(i,itm) {
            var d = $('<div />').addClass('asset_image');
            d.append($('<img />').attr('src','/img/assets/'+itm.path));
            d.data('img','/img/assets/'+itm.path);
            d.hide();
            $('.results').append(d);
            d.show('slow');
          });

          var tool = $('<div />').addClass('btn-group').addClass('text-center');
          $('.results-meta').append(tool);
          for(var i = 0; i < pge.total; i++) {
            var btn = $('<a />').addClass('btn').addClass('btn-default').text(i+1);
            if((i+1) == pge.current_page) {
              btn.addClass('active');
            }
            tool.append(btn);
            btn.data('page',i+1);
          }
        });
      }

      return this;
  }
}( jQuery ));
