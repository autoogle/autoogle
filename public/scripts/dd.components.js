(function( $ ) {

  $.fn.componentEditor = function(action,data) {
    var el = $(this);

    if(typeof(action) == 'undefined' || action == '') {
      if(typeof(data) != 'undefined') {
        el.onSave = data.onSave;
      }
      else {
        this.onSave = function(data) {};
      }

      el.on('click','.save',function(e) {
        e.preventDefault();

        var res = {};
        $('input,select,textarea',el).each(function() {
          var name = $(this).attr('name');
          if(name.indexOf("[]") > -1) {
            var name = name.replace('[]','');
            if(typeof(res[name]) != 'undefined') {
              res[name].push({name : $(this).val()});
            }
            else {
              res[name] = new Array();
              res[name].push({name : $(this).val()});
            }
          }
          else {
            res[name] = $(this).val();
          }
        });
        console.log(res);
        el.onSave(res,function() {
            clearInput();
          });
      });
    }
    else if(action == 'bind') {
      clearInput();
      $('.components',el).empty();

      if(data == '') return;
      var binding = JSON.parse(data.binding);

      $.each(binding,function(i,itm) {
        if(itm.max == 1) {
          var ci = createInput(itm,false,0);
          //console.log(ci);
          if(ci != null) {
            $('.components',el).append(ci);
          }
        }
        else {
          if(itm.max == 0) {
            var ci = createInput(itm,true,$('.components input[name="'+itm.field+'[]"]').length);
            //console.log(ci);
            if(ci != null) {
              $('.components',el).append(ci);
            }

            var a = $('<a />').addClass('btn').addClass('btn-primary').text('Add');
            $('.components',el).append(a);
            $('.components',el).append($('<hr />'));
            a.click(function(e) {
              var ci = createInput(itm,true,$('.components input[name="'+itm.field+'[]"]').length);
              //console.log(ci);
              if(ci != null) {
                ci.insertBefore($(this));
              }
            });
          }
          else {
            for(var l = 0; i < itm.max; i++) {
              var ci = createInput(itm,true);
              //console.log(ci);
              if(ci != null) {
                $('.components',el).append(ci);
              }
            }
          }
        }
      });

      if($('textarea').length > 0)
        CKEDITOR.replace( $('textarea')[0] );
    }
    else if(action == 'setdata') {
      var t = JSON.parse(data);
      for (var key in t) {
        if( Object.prototype.toString.call( t[key] ) === '[object Array]' ) {
          for(var e = 0; e < t[key].length; e++) {
            if(e == 0) {
              $('input[name="'+key+'[]"]').val(t[key][e].name);
            }
            else {
              var ci = $('<div />').addClass('form-group');
              ci.append($('input[name="'+key+'[]"]').clone());
              //console.log(ci);
              if(ci != null) {
                ci.insertAfter($('input[name="'+key+'[]"]'),el);
                $('input',ci).val(t[key][e].name);
              }
            }
          }
        }
        else {
          if($('input[name="'+key+'"]').length > 0)
            $('input[name='+key+']').val(t[key]);
          else
            $('textarea[name='+key+']').val(t[key]);
        }
      }
    }

    function clearInput() {
      for(name in CKEDITOR.instances)
      {
          CKEDITOR.instances[name].destroy()
      }
      $('.components').html("");
    }

    function createInput(d,multi,idx) {
      var lbl = $('<label for="'+d.field+'" />').text(capitalize(typeof(d.display) != 'undefined' ? d.display : d.field));
      var inp = null;
      var handled = false;

      if(d.type == "text") {
        inp = $('<input type="text" class="form-control" name="'+d.field+(multi ? '[]' : '')+'" />');
        handled = true;
      }
      else if(d.type == "image") {
        var tpl = '<div class="input-group"><input type="text" class="form-control" name="'
          +d.field+(multi ? '[]' : '')+'" placeholder="Image"><span class="input-group-btn">'
        +'<button class="btn btn-default" type="button">Choose</button></span></div>';
        inp = $(tpl);

        inp.click(function(e) {
          e.preventDefault();
          assets.assetManager('show',{"callback":function(data) {
            $('input',inp).val(data);
          }});
        });

        handled = true;
      }
      else if(d.type == "dropdown") {
        var cbo = $('<select />').addClass('form-control').attr('name',d.field+(multi ? '[]' : ''));
        $.each(d.data,function(i,itm) {
          var opt = $('<option />').text(itm.label).attr('value',itm.id);
          cbo.append(opt);
        });
        inp = cbo;
        handled = true;
      }
      else if(d.type == "area") {
        var tpl = "<textarea class=\"form-control\" name='"+d.field+(multi ? '[]' : '')+"' />";

        inp = $(tpl);
        handled = true;
      }

      if(!handled) return null;

      var t = $('<div />').addClass('form-group');

      if(!multi || (multi && idx == 0)) t.append(lbl);
      t.append(inp);
      return t;
    }
    return this;
 };

 var oldVal = $.fn.val;
  $.fn.val = function(value) {
      if(CKEDITOR.instances[$(this).attr('name')] != null) {
          if(typeof(value) != 'undefined')
            CKEDITOR.instances[$(this).attr('name')].setData(value);
          else
            return CKEDITOR.instances[$(this).attr('name')].getData();
      }
      else {
        return oldVal.apply(this, arguments);
      }
  };
}( jQuery ));
