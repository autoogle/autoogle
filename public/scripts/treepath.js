/**
 * Treepath takes an array of pathnames and gives you back
 * an object representation of that path hierarchy.
 */
var treepath = (function() {
  function buildTree(tree, pages) {
    var lastDir = 'root';
    var dirPath = '';

    parts.forEach(function(part) {
      var name = part.trim();

      // In case we have a single `/`
      if (!name || !!name.match(/^\/$/)) {
        return;
      }

      // It's a directory
      if (name.indexOf('.') === -1) {
        lastDir = name;
        dirPath += lastDir + '/';

        if (!tree[name]) {
          tree[name] = {
            path: dirPath,
            files: []
          };
        }
      } else {
        // It's a file
        tree[lastDir].files.push(name);
      }
    });
  }

  return function init(pages) {
    if (!pages || !Array.isArray(pages)) {
      throw new TypeError('Expected paths to be an array of strings but received: ' + (typeof pages));
    }

    var tree = {
      root: {
        path: '',
        pages: []
      }
    };

    pages.forEach(function(pages) {
      buildTree(tree, pages);
    });

    return tree;
  };
}());
