var gulp = require('gulp'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	less = require('gulp-less'),
	cssmin = require('gulp-cssmin'),
	sourcemaps = require('gulp-sourcemaps'),
	stripCssComments = require('gulp-strip-css-comments');

var js_files = [];

gulp.task('uglify', function () {
	return gulp.src(js_files)
		.pipe(uglify()).pipe(concat('ac.min.js'))
		.pipe(gulp.dest('js'));
});

gulp.task('js', function () {
	return gulp.src(js_files)
		.pipe(concat('ac.min.js'))
		.pipe(gulp.dest('js'));
});

gulp.task('less', function() {
	gulp.src('css/ac.less')
		.pipe(less({compress: true}))
		.pipe(stripCssComments())
		.pipe(cssmin())
		.pipe(concat('ac.min.css'))
		.pipe(gulp.dest('css'));
	gulp.src('css/admin.less')
		.pipe(less({compress: true}))
		.pipe(stripCssComments())
		.pipe(cssmin())
		.pipe(concat('ac-admin.min.css'))
		.pipe(gulp.dest('css'));
});

gulp.task('less-dev', function() {
	gulp.src('css/ac.less')
		.pipe(sourcemaps.init())
		.pipe(less({compress: false}))
		.pipe(concat('ac.min.css'))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('css'));
	gulp.src('css/admin.less')
		.pipe(sourcemaps.init())
		.pipe(less({compress: false}))
		.pipe(concat('admin.min.css'))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('css'));
});

gulp.task('watch', function () {
	gulp.watch(['css/*.less','vendor/dd_tpl/components/*.less'], ['less-dev']);
});