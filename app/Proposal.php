<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Lang;

class Proposal extends Model {
	function messages() {
		return $this->hasMany('App\Message');
	}

	function request() {
		return $this->belongsTo('App\UserRequest');
	}

	function getRRP() {
		return $this->base_rrp + $this->extra_rrp;
	}

	function getOffer() {
		return $this->base_offer + $this->extra_offer;
	}

	function getSaving() {
		$res =  $this->getOffer() - $this->getRRP();
		if($res < 0) {
			return abs($res);
		}
		else {
			return $res;
		}
	}

	function dealer() {
		return $this->belongsTo('\App\Dealer');
	}

	function agent() {
		return $this->belongsTo('\App\User','posted_by');
	}

	public function getLocalizedPrice($price,$decimal) {
	  if($price == null) return null;

	  if($decimal) {
	  	if(Lang::getLocale() == 'en') $price = number_format($price,2,'.',',');
	  	else $price = number_format($price,2,',',' ');
	  }
	  else {
	  	if(Lang::getLocale() == 'en') $price = number_format($price,0,'.',',');
	  	else $price = number_format($price,0,',',' ');
	  }

	  return Lang::get('messages.currency',['amount' => $price]);
	}
}
