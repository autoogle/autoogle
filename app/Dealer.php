<?php namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Dealer extends Model {
	protected $geofields = array('location');

	public function setLocationAttribute($value) {
        $this->attributes['location'] = DB::raw("POINT(".$value[0].",".$value[1].")");
    }

    public function getLocationAttribute($value){
		if($value == NULL) return null;
        $loc =  substr($value, 6);
        $loc = preg_replace('/[ ,]+/', ',', $loc, 1);

        return explode(',',substr($loc,0,-1));
    }

    public function newQuery($excludeDeleted = true)
    {
        $raw='';
        foreach($this->geofields as $column){
            $raw .= ' astext('.$column.') as '.$column.' ';
        }

        return parent::newQuery($excludeDeleted)->addSelect('*',DB::raw($raw));
    }

	public function requests() {
		return $this->belongsToMany('App\UserRequest','request_shown','dealer_id','request_id');
	}

	public function makes() {
		return $this->belongsToMany('App\Make','dealer_makes','dealer_id','make_id');
	}
}
