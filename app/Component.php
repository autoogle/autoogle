<?php namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use View;

class Component extends Model {
	protected $appends = array('tpl');

	public function getTplAttribute() {
		return View::make($this->layout)->render();
  }
}
