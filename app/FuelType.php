<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class FuelType extends Model {
	protected $table = 'fueltypes';
	
	function getLocalisedName() {
		$ln = Lang::getLocale();
		$g = 'fuel_type_'.$ln;
		
		if(isset($this->$g)) return $this->$g;
		else return $this->fuel_type;
	}
}