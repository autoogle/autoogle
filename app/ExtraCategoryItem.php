<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class ExtraCategoryItem extends Model {
	protected $table = 'extra_category_items';
	
	function getLocalisedName() {
		$ln = Lang::getLocale();
		$g = 'name_'.$ln;
		
		if(isset($this->$g)) return $this->$g;
		else return $this->name;
	}
	
	public function trim() {
	  return $this->belongsToMany('App\Trim')->withPivot('price');
	}
	
	public function model() {
	  return $this->belongsTo('App\CarModel');
	}
}