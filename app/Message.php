<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class Message extends Model {

	function fromUser() {
		return $this->belongsTo('App\User','from_id');
	}

	function toUser() {
		return $this->belongsTo('App\User','to_id');
	}
}
