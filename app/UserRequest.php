<?php namespace App;

use \Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserRequest extends Model {
	protected $table = 'requests';
	protected $geofields = array('location');


    public function setLocationAttribute($value) {
        $this->attributes['location'] = DB::raw("POINT(".$value[0].",".$value[1].")");
    }

    public function getLocationAttribute($value){

        $loc =  substr($value, 6);
        $loc = preg_replace('/[ ,]+/', ',', $loc, 1);
				$loc = substr($loc,0,-1);

        return explode(',',$loc);
    }

    public function newQuery($excludeDeleted = true)
    {
        $raw='';
        foreach($this->geofields as $column){
            $raw .= ' astext('.$column.') as '.$column.' ';
        }

        return parent::newQuery($excludeDeleted)->addSelect('*',DB::raw($raw));
    }

	function getDistanceTo($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Mi') {
	     $theta = $longitude1 - $longitude2;
	     $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
	     $distance = acos($distance);
	     $distance = rad2deg($distance);
	     $distance = $distance * 60 * 1.1515; switch($unit) {
	          case 'Mi': break; case 'Km' : $distance = $distance * 1.609344;
	     }
	     return (round($distance,2));
	}

	public function engine() {
		return $this->belongsTo('App\Engine');
	}

	public function trim() {
		return $this->belongsTo('App\Trim');
	}

	public function proposals() {
		return $this->hasMany('App\Proposal','request_id');
	}
}
