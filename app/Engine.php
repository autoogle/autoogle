<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Gearbox;
use Lang;

class Engine extends Model {
	protected $table = 'engines';
	
	public function gearbox() {
	  return $this->belongsTo('App\Gearbox');
	}
	
	public function fueltype() {
	  return $this->belongsTo('App\FuelType');
	}
	
	public function model() {
	  return $this->belongsTo('App\CarModel');
	}
	
	public function trim() {
	  return $this->belongsToMany('App\Trim')->withPivot('price');
	}
	
	public function getMinPrice() {
	  $lowest = null;
	  
	  foreach($this->trim as $t) {
	  	if($lowest == null) {
	  		 $lowest = $t->pivot->price;
	  	}
	  	else if($t->pivot->price < $lowest) {
	  		$lowest = $t->pivot->price;
	  	}
	  }
	  
	  return $lowest;
	}
	
	public function getMaxPrice() {
	  $highest = null;
	  
	  foreach($this->trim as $t) {
	  	if($highest == null) {
	  		 $highest = $t->pivot->price;
	  	}
	  	else if($t->pivot->price > $highest) {
	  		$highest = $t->pivot->price;
	  	}
	  }
	  
	  return $highest;
	}
}