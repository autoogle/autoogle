<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class ExtraCategory extends Model {
	protected $table = 'extra_categories';
	
	public function items() {
	  return $this->hasMany('App\ExtraCategoryItem','category_id');
	}
	
	function getLocalisedName() {
		$ln = Lang::getLocale();
		$g = 'name_'.$ln;
		
		if(isset($this->$g)) return $this->$g;
		else return $this->name;
	}
}