<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Template extends Model {

	function components() {
		return $this->belongsToMany('App\Component','template_parts')->withPivot('settings','parent_id','parent_meta','id');
	}
}
