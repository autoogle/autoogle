<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	const ADMIN =  0xF000; //61440
	const DEALER = 0x0F00; //3840

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	function hasRole($r) {
		return $r & $this->role;
	}

	public function dealer() {
		return $this->belongsTo('App\Dealer');
	}

	public function getInitials() {
		if($this->name == null || trim($this->name) == '') return 'u';

		$n = $this->name;
		$s = explode(' ',$n);

		if(sizeof($s) == 2) return $s[0][0].$s[1][0];
		else $s[0][0];
	}
}
