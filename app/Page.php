<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model {
	use SoftDeletes;

	public function template() {
		return $this->belongsTo('App\Template');
	}

	public function pagedata() {
		return $this->hasMany('App\PageData');
	}
}
