<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class Gearbox extends Model {
	protected $table = 'gearboxes';
	
	function getLocalisedName() {
		$ln = Lang::getLocale();
		$g = 'gearbox_type_'.$ln;
		
		if(isset($this->$g)) return $this->$g;
		else return $this->gearbox_type;
	}
}