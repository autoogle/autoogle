<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class Trim extends Model {
	function getLocalisedDescription() {
		$ln = Lang::getLocale();
		$g = 'description_'.$ln;
		
		if(isset($this->$g)) return $this->$g;
		else return $this->description;
	}
	
	public function getLocalizedPrice($price,$decimal) {
	  if($price == null) return null;

	  if($decimal) {
	  	if(Lang::getLocale() == 'en') $price = number_format($price,2,'.',',');
	  	else $price = number_format($price,2,',',' ');
	  }
	  else {
	  	if(Lang::getLocale() == 'en') $price = number_format($price,0,'.',',');
	  	else $price = number_format($price,0,',',' ');
	  }
	  
	  return Lang::get('messages.currency',['amount' => $price]);
	}
}