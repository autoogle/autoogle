<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class OfferUpdated extends Event {

	use SerializesModels;

	public $user_id;
	public $offer_id;
	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($user_id,$offer_id) {
		$this->user_id = $user_id;
		$this->offer_id = $offer_id;
	}

}
