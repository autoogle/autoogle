<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class NewRequest extends Event {

	use SerializesModels;

	public $user_id;
	public $request_id;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($user_id, $request_id) {
		$this->user_id = $user_id;
		$this->request_id = $request_id;
	}
}
