<?php namespace App\Handlers\Events;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use DB;

class BaseHandler implements ShouldBeQueued {

	use InteractsWithQueue;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct() {
	}

	public function log($name, $data) {
    DB::table('eventlog')->insert(array('name' => $name, 'data' => $data));
  }

}
