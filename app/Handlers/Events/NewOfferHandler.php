<?php namespace App\Handlers\Events;

use App\Events\NewOffer;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Mail;
use Lang;

use App\User;
use App\UserRequest;

class NewOfferHandler extends BaseHandler {

	//use InteractsWithQueue;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct() {
	}

	/**
	 * Handle the event.
	 *
	 * @param  UserCreated  $event
	 * @return void
	 */
	public function handle(NewOffer $event) {
		$this->log('NewOffer','UserID:'.$event->user_id.', RequestID:'.$event->request_id.', OfferID:'.$event->offer_id);

		if($event->user_id != null) {
			$u = User::find($event->user_id);
			$r = UserRequest::with('engine','trim','engine.model','engine.model.make')->find($event->request_id);

			Mail::send('emails.user.new_offer_'.Lang::getLocale(), ['user' => $u, 'req' => $r, 'offer_id' => $event->offer_id], function($message) use ($u) {
			    $message->to($u->email)->subject(Lang::get('email.new_offer_subject'));
			});
		}
	}

}
