<?php namespace App\Handlers\Events;

use App\Events\UserReplied;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Mail;
use Lang;

use App\User;

class UserRepliedHandler extends BaseHandler {

	//use InteractsWithQueue;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct() {
	}

	/**
	 * Handle the event.
	 *
	 * @param  UserCreated  $event
	 * @return void
	 */
	public function handle(UserReplied $event) {
		$this->log('UserReplied','UserID:'.$event->user_id.', OfferID:'.$event->offer_id);

		if($event->user_id != null) {
			$u = User::find($event->user_id);

			Mail::send('emails.dealer.user_replied_'.Lang::getLocale(), ['user' => $u,
				'offer_id' => $event->offer_id], function($message) use ($u) {
			    $message->to($u->email)->subject(Lang::get('email.user_replied_subject'));
			});
		}
	}

}
