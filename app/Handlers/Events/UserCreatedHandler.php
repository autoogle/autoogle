<?php namespace App\Handlers\Events;

use App\Events\UserCreated;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Mail;
use Lang;

use App\User;

class UserCreatedHandler extends BaseHandler {

	//use InteractsWithQueue;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct() {
	}

	/**
	 * Handle the event.
	 *
	 * @param  UserCreated  $event
	 * @return void
	 */
	public function handle(UserCreated $event) {
		$this->log('UserCreated','UserID:'.$event->user_id);

		if($event->user_id != null) {
			$u = User::find($event->user_id);

			Mail::send('emails.user.confirm_'.Lang::getLocale(), ['user' => $u], function($message) use ($u) {
			    $message->to($u->email)->subject(Lang::get('email.confirm_subject'));
			});
		}
	}

}
