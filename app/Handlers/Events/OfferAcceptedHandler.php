<?php namespace App\Handlers\Events;

use App\Events\OfferAccepted;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Mail;
use Lang;

use App\User;

class OfferAcceptedHandler extends BaseHandler {

	//use InteractsWithQueue;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct() {
	}

	/**
	 * Handle the event.
	 *
	 * @param  UserCreated  $event
	 * @return void
	 */
	public function handle(OfferAccepted $event) {
		$this->log('OfferAccepted','UserID:'.$event->user_id.', OfferID:'.$event->offer_id);

		if($event->user_id != null) {
			$u = User::find($event->user_id);

			/* Dealer out
			Mail::send('emails.dealer.offer_accepted_'.Lang::getLocale(), ['user' => $u, 'offer_id' => $event->offer_id], function($message) use ($u) {
			    $message->to($u->email)->subject(Lang::get('email.offer_accepted_subject'));
			});*/
		}
	}

}
