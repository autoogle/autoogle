<?php namespace App\Handlers\Events;

use App\Events\NewRequest;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Mail;
use Lang;

use App\User;
use App\UserRequest;

class NewRequestHandler extends BaseHandler {

	//use InteractsWithQueue;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct() {
	}

	/**
	 * Handle the event.
	 *
	 * @param  UserCreated  $event
	 * @return void
	 */
	public function handle(NewRequest $event) {
		$this->log('NewRequest','UserID:'.$event->user_id.', RequestID:'.$event->request_id);

		if($event->user_id != null) {
			$u = User::find($event->user_id);
			$r = UserRequest::with('engine','trim','engine.model','engine.model.make')->find($event->request_id);

			Mail::send('emails.dealer.new_request_'.Lang::getLocale(), ['user' => $u, 'req' => $r], function($message) use ($u) {
			    $message->to($u->email)->subject(Lang::get('email.new_request_subject'));
			});
		}
	}

}
