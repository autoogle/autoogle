<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class UserAuthController extends Controller {

	public function login(Request $req) {
    $pc = $req->input('pc');

    $u = User::where('passcode',$pc)->first();
    if($u != null) {
      Auth::login($u);
      return redirect('user/dashboard');
    }
    else {
      return redirect('/');
    }
	}
}
