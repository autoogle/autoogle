<?php namespace App\Http\Controllers;

use App\Page;
use App\Component;
use App\JsonTemplate\JsonTemplateModule;
use NotFoundHttpException;
use Lang;

class PageController extends Controller {

	public function index($path) {
		$path = str_replace(Lang::get('routing.car-reviews'),'car-reviews',$path);

		$p = Page::where('path',$path)->with('template')->first();
		if($p == null) abort(404);
		$components = $this->getPageParts($p->id,$p->template->id);
		return view($p->template->base)->with('title',$p->title)->with('components',$components);
	}

	public function getPageParts($page,$template) {
		$components = $this->getComponents($page,$template,null);

		$sb = '';
		foreach($components as $set) {
			foreach($set as $c) {
				$sb .= $c;
			}
		}

		$sb = preg_replace_callback('/\$[A-z]+\$/m',function($match) {
			return '';
		},$sb);

		return $sb;
	}

	public function getComponents($page,$template,$parent) {
		$query = Component::join('template_parts','components.id','=','template_parts.component_id')
			->join('page_data','page_data.template_part_id','=','template_parts.id')
			->where('page_data.page_id',$page)
			->select('components.*','page_data.template_part_id','page_data.data','template_parts.parent_meta');

		if($parent != NULL) {
			$query = $query->where('template_parts.parent_id',$parent);
		}
		else {
			$query = $query->whereNull('template_parts.parent_id');
		}

		$results = $query->orderby('template_parts.sequence')->get();

		$sb = array();
		foreach($results as $r) {
			$children = $this->getComponents($page,$template,$r->template_part_id);
			$m = ($r->parent_meta != NULL ? $r->parent_meta : 'c'.$r->template_part_id);
			if($children != '') {
				$res = $this->render($r,$children);
				if(array_key_exists($m,$sb)) {
					array_push($sb[$m],$res);
				}
				else {
					$sb[$m] = [$res];
				}
			}
			else {
				$res = $this->render($r,null);
				if(array_key_exists($m,$sb)) {
					array_push($sb[$m],$res);
				}
				else {
					$sb[$m] = [$res];
				}
			}
		}
		//echo var_dump($sb);
		return $sb;
	}

	public function render($component,$groups) {
		$view = view($component->layout);
		$proc = JsonTemplateModule::expand($view,$component->data);

		foreach($groups as $key => $children) {
			if($children != null && sizeof($children)) {
				$sb = '';
				foreach($children as $c) {
					$sb .= $c;
				}
			
				if(preg_match_all('/\$('.$key.')\$/i',$view,$matches,PREG_SET_ORDER)) {
					foreach($matches as $match) {
						$proc = str_replace($match[0],$sb,$proc);
					}
				}
			}
		}

		return $proc;
	}
	
	function groupArray($id,$data) {
		$result = array();
		foreach ($arr as $data) {
		  $id = $data['id'];
		  if (isset($result[$id])) {
		     $result[$id][] = $data;
		  } else {
		     $result[$id] = array($data);
		  }
		}
		return $result;
	}
}
