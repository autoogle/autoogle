<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Session;
use Lang;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

	public function __construct() {
		\Carbon\Carbon::setLocale(Lang::getLocale());
	
		if(Session::has('requests')) {
				view()->share('requests',Session::get('requests'));
		}
	}
}
