<?php namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\UserRequest;
use App\Proposal;
use App\Message;
use Session;
use DB;
use App\Events\UserReplied;


class UserDashboardController extends Controller {

	public function index($id = null) {
		$user = Auth::user();
		$requests = UserRequest::where('user_id',$user->id)
			->with('proposals','proposals.dealer','proposals.agent','proposals.messages',
				'engine','trim','engine.model','engine.model.make')
			->orderBy('created_at','desc')
			->get();

		Session::put('requests',$requests);

		$curReq = null;
		if($id == null) {
			if(sizeof($requests) > 0) {
					$curReq = $requests->get(0);
			}
		}
		else {
			$curReq = UserRequest::where('user_id',$user->id)
				->where('id',$id)
				->with('proposals','proposals.dealer','proposals.agent','proposals.messages',
					'engine','trim','engine.model','engine.model.make')
				->orderBy('created_at','desc')
				->first();
		}

		return view('user.dashboard')->with('req',$curReq);
  }

	public function offerDetails($id) {
		$prop = Proposal::with('dealer','agent','messages','messages.fromUser','messages.toUser','request',
		'request.engine','request.trim','request.engine.model','request.engine.model.make')->find($id);
		return view('user.offer-details')->with('offer',$prop);
	}

	public function sendMessage(Request $req) {
		$msg = $req->input('message');
		$agent = $req->input('agent_id');
		$proposal = $req->input('proposal_id');

		$m = new Message();
		$m->message = $msg;
		$m->proposal_id = $proposal;
		$m->to_id = $agent;
		$m->from_id = Auth::id();
		$m->save();

		$p = Proposal::find($proposal);
		$p->status = 1;
		$p->save();

		\Event::fire(new UserReplied($agent,$proposal));

		$m = Message::with('fromUser','toUser')->find($m->id);
		return view('elements.user-message')->with('m',$m);
	}

	public function acceptOffer($offer_id) {
		$p = Proposal::find($offer_id);
		$p->status = 10;
		$p->save();

		$aff = Proposal::where('request_id',$p->request_id)->where('id','<>',$p->id)
			->update(array('status' => 2));

		\Event::fire(new OfferAccepted($p->posted_by,$offer_id));

		//Not sure what to do here
		return redirect('user/dashboard');
	}
}
