<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Lang;
use App\UserRequest;
use Session;

class UserAuthController extends Controller {

	public function login(Request $req) {
    $pc = $req->input('pc');

    $u = User::where('passcode',$pc)->first();
    if($u != null) {
      Auth::login($u);
      return redirect(Lang::get('routing.user').'/'.Lang::get('routing.user-dashboard'));
    }
    else {
      return redirect('/');
    }
	}

	public function loginByPass($pc,$params = null) {
		$u = User::where('passcode','=',$pc)->first();
    if($u != null) {
      Auth::login($u);

			$requests = UserRequest::where('user_id',$u->id)
				->with('proposals','proposals.dealer','proposals.agent','proposals.messages',
					'engine','trim','engine.model','engine.model.make')
				->orderBy('created_at','desc')
				->get();

			Session::put('requests',$requests);

			if($params == null) {
					return redirect(Lang::get('routing.user').'/'.Lang::get('routing.user-dashboard'));
			}
      else {
				return redirect(Lang::get('routing.user').'/'.$params);
			}
    }
    else {
      return redirect('/');
    }
	}

	public function confirm() {
		$u = Auth::user();
		$u->confirmed = 1;
		$u->save();

		return view('wizard.thankyou_'.Lang::getLocale());
	}
}
