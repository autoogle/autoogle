<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;
use DB;
use App\UserRequest;
use \App\Events\NewRequest;

class UserMatchingController extends Controller {
  public function processMatching() {
    $results = DB::select('select r.id as req_id,d.id as dealer_id,  X(d.location) as lat, Y(d.location) as lng,
X(r.location) as l, Y(r.location),
(3959 *
        acos(
          cos( radians(X(d.location)) )
        * cos( radians( X(r.location) ) )
        * cos( radians( Y(r.location) ) - radians(Y(d.location)) )
        + sin( radians(X(d.location)) )
        * sin( radians( X(r.location) ) ) ) ) as distance


FROM `dealers` d
INNER JOIN `requests` r on
(3959 *
        acos(
          cos( radians(X(d.location)) )
        * cos( radians( X(r.location) ) )
        * cos( radians( Y(r.location) ) - radians(Y(d.location)) )
        + sin( radians(X(d.location)) )
        * sin( radians( X(r.location) ) ) ) ) < 150
INNER JOIN `users` u on r.user_id = u.id
WHERE r.new = 1 and u.confirmed = 1 ORDER BY r.id,distance');

    DB::table('eventlog')->insert(array('name' => 'matching', 'data' => sizeof($results)));

    if($results != null && sizeof($results) > 0) {
      foreach($results as $r) {
        DB::table('request_shown')->insert(array('dealer_id' => $r->dealer_id,'request_id' => $r->req_id,'updated_at' => DB::raw('NOW()'),'created_at' => DB::raw('NOW()')));

        $ur = UserRequest::find($r->req_id);
        $ur->new = null;
        $ur->save();

        //event
        $users = User::where('dealer_id',$r->dealer_id)->get();
        foreach($users as $u) {
          \Event::fire(new NewRequest($u->id,$r->req_id));
        }
      }
    }
  }
}
