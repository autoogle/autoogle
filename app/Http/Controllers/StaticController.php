<?php namespace App\Http\Controllers;

use Lang;

class StaticController extends Controller {

	public function showFAQ() {
		return view('static.faq_'.Lang::getLocale());
	}

	public function showTNC() {
		return view('static.t_and_c_'.Lang::getLocale());
	}

	public function showPrivacy() {
		return view('static.privacy_'.Lang::getLocale());
	}
}
