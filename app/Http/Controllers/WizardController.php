<?php namespace App\Http\Controllers;

use App\Make;
use App\CarModel;
use App\Engine;
use App\ExtraCategory;
use App\UserRequest;
use App\Location;
use App\User;
use \Illuminate\Support\Facades\Session;
use \Illuminate\Support\Facades\Input;
use \Illuminate\Support\Facades\Redirect;
use \Illuminate\Support\Facades\Hash;
use \Illuminate\Support\Facades\Lang;
use \Illuminate\Http\Request;

use \App\Events\UserCreated;

class WizardController extends Controller {

	public function index() {
		$makes = Make::orderBy('name')->get();

		return view('wizard.make')->with('makes',$makes);
	}

	public function showModels($id = null) {
		if($id != NULL) {
			Session::set('make',$id);
		}
		else if(Session::has('make',$id)) {
			$id = Session::get('make');
		}
		else {
			return Redirect::to(Lang::get('routing.select-make'));
		}

		$models = CarModel::active()->where('make_id',$id)->orderBy('name')->get();
		return view('wizard.model')->with('models',$models);
	}

	public function showEngines($id = null) {
		if($id != NULL) {
			Session::set('model',$id);
		}
		else if(Session::has('model',$id)) {
			$id = Session::get('model');
		}
		else {
			return Redirect::to(Lang::get('routing.select-model'));
		}

		$engines = Engine::with('gearbox','fueltype','trim')->where('model_id',$id)->orderBy('name')->orderBy('horsepower')->get();
		//return $engines;
		return view('wizard.engine')->with('engines',$engines);
	}

	public function showTrims($id = null) {
		if($id != NULL) {
			Session::set('engine',$id);
		}
		else if(Session::has('engine',$id)) {
			$id = Session::get('engine');
		}
		else {
			return Redirect::to(Lang::get('routing.select-engine'));
		}

		$engine = Engine::with('trim')->where('id',$id)->first();

		//if(sizeof($engine->trim) == 1) {
		//	return Redirect::to('finish');
		//}

		return view('wizard.trims')->with('trims',$engine->trim);
	}

	/*
	public function showExtras($id = null,$tid = null) {
		if($id != NULL) {
			Session::set('engine',$id);
		}
		else if(Session::has('engine',$id)) {
			$id = Session::get('engine');
		}
		else {
			return Redirect::to('select-engine');
		}

		if($tid != NULL) {
			Session::set('trim',$tid);
		}
		else if(Session::has('trim',$tid)) {
			$tid = Session::get('trim');
		}
		else {
			return Redirect::to('select-engine');
		}

		$engine = Engine::with(array('gearbox','fueltype','trim' => function($query) use ($tid) {
			$query->where('trim_id',$tid);
		}))->where('id',$id)->first();
		$categories = ExtraCategory::with(array('items' => function($query) use ($engine) {
			$query->where('model_id',Session::get('model'));
		},
		'items.trim' => function($query) use ($tid) {
			$query->where('trim_id',$tid);
		}))->get();
		//return $categories;
		return view('wizard.extras')->with('category',$categories)->with('engine',$engine);
	}*/

	public function showFinish($id = null) {
		if($id != NULL) {
			Session::set('trim',$id);
		}
		else if(Session::has('trim',$id)) {
			$id = Session::get('trim');
		}
		else {
			return Redirect::to(Lang::get('routing.select-trim'));
		}

		return view('wizard.finish');
	}

	public function saveData(Request $req) {
		$u = User::where('email',$req->input('email'))->first();

		$newUser = false;
		if($u == null) {
			$u = new User();
			$u->email = $req->input('email');
			$u->passcode = str_replace('/','',Hash::make(str_random(15)));
			$u->save();

			$newUser = true;
			\Event::fire(new UserCreated($u->id));
		}

		$r = new UserRequest();
		$r->user_id = $u->id;
		$r->engine_id = Session::get('engine');
		$r->trim_id = Session::get('trim');
		$r->new = 1;

		$loc = $this->getLocation($req->input('postcode'));
		$r->setLocationAttribute($loc);
		$r->save();

		if($newUser) return view('wizard.checkmail_'.Lang::getLocale());
		else return view('wizard.thankyou_'.Lang::getLocale());
	}

	private function getLocation($town) {
		$l = Location::where('town',$town)->where('country',Lang::get('messages.country'))->first();
		if($l == null) {
			$loc = $this->googleLoc($town,Lang::get('messages.country'));

			$l = new Location();
			$l->town = $town;
			$l->country = Lang::get('messages.country');
			$l->setLocationAttribute($loc);
			$l->save();
		}
		return $l->location;
	}

	private function googleLoc($town,$country) {
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
		$url .= $town.' '.$country.'&key=AIzaSyBls8o4hSYQCaC6Ijrz2lNCJXIDs0MnaX8';
		$url = str_replace(' ','+',$url);
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CAINFO, getcwd() . "/ca-bundle.crt");

		$server_output = curl_exec ($ch);

		if ($errno = curl_errno($ch)) {
			echo 'Error '.$errno;
		}

		curl_close ($ch);

		$data = json_decode($server_output);
		return [$data->results[0]->geometry->location->lat,$data->results[0]->geometry->location->lng];
	}
}
