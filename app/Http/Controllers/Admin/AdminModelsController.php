<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Make;
use App\CarModel;
use App\ModelType;
use Illuminate\Http\Request;

class AdminModelsController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index() {
		$models = CarModel::all();
		$makes = Make::lists('name','id');
		$types = ModelType::lists('name','id');
		return view('admin/models')->with('models',$models)->with('makes',$makes)->with('types',$types)->with('model',new CarModel());
	}
	
	public function editModel($id) {
		$models = CarModel::all();
		$makes = Make::lists('name','id');
		$types = ModelType::lists('name','id');
		return view('admin/models')->with('models',$models)->with('makes',$makes)->with('types',$types)->with('model',CarModel::find($id));
	}
	
	public function updateModel(Request $req,$id = null) {
		
		$this->validate($req, [
			'make_id' => 'required|not_in:0',
        	'name' => 'required',
    	]);
		
		if($id != null) {
			$m = CarModel::find($id);
			$m->name = $req->input('name');
			$m->make_id = $req->input('make_id');
			$m->model_type_id = $req->input('model_type_id');
			$m->save();
		
			return redirect('admin/data/models')->with('msg', 'Model '.$id.' has beed updated');
		}
		else {
			$m = new CarModel();
			$m->name = $req->input('name');
			$m->make_id = $req->input('make_id');
			$m->model_type_id = $req->input('model_type_id');
			$m->inactive = 1;
			$m->save();
			
			return redirect('admin/data/models')->with('msg','Model has been created');
		}
	}
	
	public function deleteModel($id) {
		$m = CarModel::find($id);
		$m->delete();
		
		return redirect('admin/data/models')->with('msg','Model has been deleted');
	}
	
	public function toggleActive($id) {
		$m = CarModel::find($id);
		$m->inactive = ($m->inactive == 1 ? 0 : 1);
		$m->save();
		
		return redirect('admin/data/models')->with('msg','Model '.$id
			.' has been '.($m->inactive == 1 ? 'Deactived' : 'Published' ));
	}
}
