<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ModelType;
use Illuminate\Http\Request;

class AdminModelTypesController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index() {
		$types = ModelType::all();
		return view('admin/model-types')->with('types',$types)->with('type',new ModelType());
	}
	
	public function editModelType($id) {
		$types = ModelType::all();
		return view('admin/model-types')->with('types',$types)->with('type',ModelType::find($id));
	}
	
	public function updateModelType(Request $req,$id = null) {
		
		$this->validate($req, [
        	'name' => 'required',
    	]);
		
		if($id != null) {
			$m = ModelType::find($id);
			$m->name = $req->input('name');
			$m->name_sk = $req->input('name_sk');
			$m->save();
		
			return redirect('admin/data/model-types')->with('msg', 'Model Type '.$id.' has beed updated');
		}
		else {
			$m = new ModelType();
			$m->name = $req->input('name');
			$m->name_sk = $req->input('name_sk');
			$m->save();
			
			return redirect('admin/data/model-types')->with('msg','Model Type has been created');
		}
	}
	
	public function deleteModelType($id) {
		$m = ModelType::find($id);
		$m->delete();
		
		return redirect('admin/data/model-types')->with('msg','Model Type has been deleted');
	}
}
