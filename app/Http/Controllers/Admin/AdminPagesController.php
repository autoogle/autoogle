<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Page;
use App\Template;
use App\PageData;
use App\UploadedImage;
use Illuminate\Http\Request;
use View;
use DB;
use Hash;

class AdminPagesController extends Controller {

	public function index() {
		$templates = Template::lists('name','id');
		$paged = Page::all();
		return view('admin.pages.pages')->with('pages',$paged)->with('templates',$templates);
	}

	public function create(Request $req) {
		$tid = $req->input('tid');
		$tmp = Template::with(array('components' => function($query) {
				$query->orderBy('sequence');
		}))->find($tid);
		//return $tmp;
		foreach ($tmp->components as $i => $c) {
			//echo $c->layout;
			$c->tpl = view($c->layout);
		}

		return view('admin.pages.create')->with('template',$tmp);
	}

	public function edit($page_id) {
		$p = Page::with('pagedata')->find($page_id);
		if($p != null) {
			$tmp = Template::with(array('components' => function($query) {
					$query->orderBy('sequence');
			}))->find($p->template_id);
			//return $tmp;
			foreach ($tmp->components as $i => $c) {
				//echo $c->layout;
				$c->tpl = view($c->layout);
			}

			return view('admin.pages.create')->with('template',$tmp)->with('page',$p);
		}
		else return "Page ".$page_id." does not exist";
	}

	public function delete($page_id) {
		$p = Page::find($page_id);
		if($p != null) {
			$p->delete();

			return redirect('admin/pages')->with('msg','Page '.$page_id.' has been deleted');
		}
	}

	function saveComponent(Request $req) {
		$tpid = $req->input('template_id');
		$title = $req->input('title');
		$path = $req->input('path');
		$data = $req->input('component_data');
		$page_id = $req->input('page');

		$page = null;
		$res = DB::table('template_parts')->where('id',$tpid)->first();
		
		if($page_id != NULL && $page_id != '') {
			$page = Page::find($page_id);
		}
		else {
			$page = new Page();
			$page->template_id = $res->template_id;
		}
		
		if($res->parent_meta != null) {
			$this->ensureParent($page_id,$res->parent_id);
		}

		$page->title = $title;
		$page->path = $path;
		$page->save();

		$pd = PageData::where('page_id',$page->id)->where('template_part_id',$tpid)->first();
		if($pd == null) {
			$pd = new PageData();
			$pd->page_id = $page->id;
			$pd->template_part_id = $tpid;
		}

		$pd->data = json_encode($data);
		$pd->save();

		return $pd;
	}

	function ensureParent($pageID,$parent_id) {
		$res = DB::table('template_parts')->where('id',$parent_id)->first();

		$pg = PageData::where('page_id',$pageID)->where('template_part_id',$res->id)->first();
		if($pg == NULL) {
			$pg = new PageData();
			$pg->page_id = $pageID;
			$pg->template_part_id = $res->id;
			$pg->save();
		}

		if($res->parent_id != null) {
			$this->ensureParent($pageID,$res->parent_id);
		}
	}

	function upload(Request $req) {
			$this->validate($req, ['file' => 'required|image']);

			if($req->hasFile('file')) {
				$fileName = str_replace('/', '_', Hash::make($this->generateRandomString(6))).'.'.$req->file('file')->guessExtension();
				$req->file('file')->move(public_path().'/img/assets', $fileName);

				$img = new UploadedImage();
				$img->path = $fileName;
				$img->save();

				return $img;
			}
			return "FAILED";
	}

	function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
	}

	function getAssets() {
		$images = UploadedImage::orderBy('created_at','desc')->paginate(20);
		return $images;
	}
}
