<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Make;
use App\Trim;
use App\Engine;
use App\CarModel;
use App\FuelType;
use App\Gearbox;
use Illuminate\Http\Request;

class AdminEnginesController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index() {
		$makes = Make::lists('name','id');
		$gears = Gearbox::lists('gearbox_type','id');
		$fuels = FuelType::lists('fuel_type','id');
		return view('admin/engines')->with('makes',$makes)
			->with('gears',$gears)->with('fuels',$fuels)
			->with('engine',new Engine());
	}
	
	public function editEngine($id) {
		$makes = Make::lists('name','id');
		$engine = Engine::with('model','model.make','trim')->where('id',$id)->first();
		$gears = Gearbox::lists('gearbox_type','id');
		$fuels = FuelType::lists('fuel_type','id');
		return view('admin/engines')->with('makes',$makes)
			->with('gears',$gears)->with('fuels',$fuels)
			->with('engine',$engine);
	}
	
	public function updateEngine(Request $req,$id = null) {
		
		$this->validate($req, [
			'model_id' => 'required|not_in:0',
        	'name' => 'required',
			'gearbox_id' => 'required|not_in:0',
			'fueltype_id' => 'required|not_in:0',
			'horsepower' => 'required',
			'consumption' => 'required',
			'no_of_gears' => 'required',
			'doors' => 'required'
    	]);
		
		if($id != null) {
			$m = Engine::find($id);
			$m->name = $req->input('name');
			$m->model_id = $req->input('model_id');
			$m->gearbox_id = $req->input('gearbox_id');
			$m->fueltype_id = $req->input('fueltype_id');
			$m->horsepower = $req->input('horsepower');
			$m->consumption = $req->input('consumption');
			$m->no_of_gears = $req->input('no_of_gears');
			$m->doors = $req->input('doors');
			$m->save();
		
		  	$trims = $this->getTrimData($req->input('id'),$req->input('price'));
			$m->trim()->sync($trims);
			
			return redirect('admin/data/engines')->with('msg', 'Engine '.$id.' has beed updated');
		}
		else {
			$m = new Engine();
			$m->name = $req->input('name');
			$m->model_id = $req->input('model_id');
			$m->gearbox_id = $req->input('gearbox_id');
			$m->fueltype_id = $req->input('fueltype_id');
			$m->horsepower = $req->input('horsepower');
			$m->consumption = $req->input('consumption');
			$m->no_of_gears = $req->input('no_of_gears');
			$m->doors = $req->input('doors');
			$m->save();
			
			$trims = $this->getTrimData($req->input('id'),$req->input('price'));
			$m->trim()->sync($trims);
			
			return redirect('admin/data/engines')->with('msg','Engine has been created');
		}
	}
	
	public function deleteEngine($id) {
		$e = Engine::find($id);
		$e->delete();
		
		return redirect('admin/data/trims')->with('msg','Engine has been deleted');
	}
	
	function getTrimData($ids, $prices) {
		$res = array();
		
		$i = 0;
		for($i = 0; $i < sizeof($ids); $i++) {
			$id = $ids[$i];
			$price = $prices[$i];
			
			if($price != NULL && $price != '') {
				$res[$id] = ['price'=>$price];
			}
		}
		
		return $res;
	}
}
