<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\CarModel;
use App\Trim;
use Illuminate\Http\Request;

class AdminTrimsController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index() {
		$trims = Trim::all();
		$models = CarModel::lists('name','id');
		return view('admin/trims')->with('trims',$trims)->with('models',$models)->with('trim',new Trim());
	}
	
	public function editTrim($id) {
		$trims = Trim::all();
		$models = CarModel::lists('name','id');
		return view('admin/trims')->with('trims',$trims)->with('models',$models)->with('trim',Trim::find($id));
	}
	
	public function updateTrim(Request $req,$id = null) {
		
		$this->validate($req, [
			'model_id' => 'required|not_in:0',
        	'name' => 'required',
    	]);
		
		if($id != null) {
			$m = Trim::find($id);
			$m->name = $req->input('name');
			$m->description = $req->input('description');
			$m->description_sk = $req->input('description_sk');
			$m->model_id = $req->input('model_id');
			$m->save();
		
			return redirect('admin/data/trims')->with('msg', 'Trim '.$id.' has beed updated');
		}
		else {
			$m = new Trim();
			$m->name = $req->input('name');
			$m->description = $req->input('description');
			$m->description_sk = $req->input('description_sk');
			$m->model_id = $req->input('model_id');
			$m->save();
			
			return redirect('admin/data/trims')->with('msg','Trim has been created');
		}
	}
	
	public function deleteTrim($id) {
		$m = Trim::find($id);
		$m->delete();
		
		return redirect('admin/data/trims')->with('msg','Trim has been deleted');
	}
}
