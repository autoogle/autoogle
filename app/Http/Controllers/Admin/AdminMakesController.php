<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Make;
use Illuminate\Http\Request;

class AdminMakesController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index() {
		$makes = Make::all();
		return view('admin/makes')->with('makes',$makes)->with('make',new Make());
	}
	
	public function editMake($id) {
		$makes = Make::all();
		return view('admin/makes')->with('makes',$makes)->with('make',Make::find($id));
	}
	
	public function updateMake(Request $req,$id = null) {
		
		$this->validate($req, [
        	'name' => 'required',
    	]);
		
		if($id != null) {
			$m = Make::find($id);
			$m->name = $req->input('name');
			$m->save();
		
			return redirect('admin/data/makes')->with('msg', 'Make '.$id.' has beed updated');
		}
		else {
			$m = new Make();
			$m->name = $req->input('name');
			$m->save();
			
			return redirect('admin/data/makes')->with('msg','Make has been created');
		}
	}
	
	public function deleteMake($id) {
		$m = Make::find($id);
		$m->delete();
		
		return redirect('admin/data/makes')->with('msg','Make has been deleted');
	}
}
