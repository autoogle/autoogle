<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ExtraCategory;
use App\Trim;
use App\Engine;
use App\CarModel;
use App\FuelType;
use App\Gearbox;
use Illuminate\Http\Request;

class AdminCategoriesController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index() {
		$cats = ExtraCategory::all();
		return view('admin/categories')->with('categories',$cats)->with('category', new ExtraCategory());
	}
	
	public function editCategory($id) {
		$cats = ExtraCategory::all();
		$category = ExtraCategory::find($id);
		return view('admin/categories')->with('categories',$cats)->with('category', $category);
	}
	
	public function updateCategory(Request $req,$id = null) {
		
		$this->validate($req, [
        	'name' => 'required'
    	]);
		
		if($id != null) {
			$c = ExtraCategory::find($id);
			$c->name = $req->input('name');
			$c->name_sk = $req->input('name_sk');
			$c->save();
			
			return redirect('admin/data/extra-categories')->with('msg', 'Category '.$id.' has beed updated');
		}
		else {
			$c = new ExtraCategory();
			$c->name = $req->input('name');
			$c->name_sk = $req->input('name_sk');
			$c->save();
			
			return redirect('admin/data/extra-categories')->with('msg','Category has been created');
		}
	}
	
	public function deleteCategory($id) {
		$c = ExtraCategory::find($id);
		$c->delete();
		
		return redirect('admin/data/extra-categories')->with('msg','Make has been deleted');
	}
}
