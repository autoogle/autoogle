<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\CarModel;
use App\Engine;
use App\Trim;
use App\ExtraCategoryItem;
use Illuminate\Http\Request;

class AdminAsyncController extends Controller {
	
	public function getModels($model) {
		$models = CarModel::where('make_id',$model)->get();
		return $models;
	}
	
	public function getEngines($engine) {
		$engines = Engine::with('gearbox','fueltype')->where('model_id',$engine)->get();
		return view('admin/async/engines')->with('engines',$engines);
	}
	
	public function getTrims($model) {
		return Trim::where('model_id',$model)->get();
	}
	
	public function getExtras($cat,$model) {
		$extras = ExtraCategoryItem::where('model_id',$model)->where('category_id',$cat)->get();
		return view('admin/async/extras')->with('extras',$extras);
	}
}
