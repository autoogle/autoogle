<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Make;
use App\ExtraCategory;
use App\ExtraCategoryItem;
use App\Trim;
use App\Engine;
use App\CarModel;
use App\Gearbox;
use Illuminate\Http\Request;

class AdminExtrasController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index() {
		$makes = Make::lists('name','id');
		$categories = ExtraCategory::lists('name','id');
		return view('admin/extras')->with('makes',$makes)
			->with('categories',$categories)
			->with('extra',new ExtraCategoryItem());
	}
	
	public function editExtra($id) {
		$extra = ExtraCategoryItem::with('model','model.make','trim')->where('id',$id)->first();
		$makes = Make::lists('name','id');
		$categories = ExtraCategory::lists('name','id');
		return view('admin/extras')->with('makes',$makes)
			->with('categories',$categories)
			->with('extra',$extra);
	}
	
	public function updateExtra(Request $req,$id = null) {
		
		$this->validate($req, [
			'model_id' => 'required|not_in:0',
        	'name' => 'required',
			'category_id' => 'required|not_in:0',
			'name_sk' => 'required',
    	]);
		
		if($id != null) {
			$m = ExtraCategoryItem::find($id);
			$m->name = $req->input('name');
			$m->name_sk = $req->input('name_sk');
			$m->model_id = $req->input('model_id');
			$m->category_id = $req->input('category_id');
			$m->save();
		
		  	$trims = $this->getTrimData($req->input('id'),$req->input('price'));
			$m->trim()->sync($trims);
			
			return redirect('admin/data/extras')->with('msg', 'Engine '.$id.' has beed updated');
		}
		else {
			$m = new ExtraCategoryItem();
			$m->name = $req->input('name');
			$m->name_sk = $req->input('name_sk');
			$m->model_id = $req->input('model_id');
			$m->category_id = $req->input('category_id');
			$m->save();
			
			$trims = $this->getTrimData($req->input('id'),$req->input('price'));
			$m->trim()->sync($trims);
			
			return redirect('admin/data/extras')->with('msg','Engine has been created');
		}
	}
	
	function getTrimData($ids, $prices) {
		$res = array();
		
		$i = 0;
		for($i = 0; $i < sizeof($ids); $i++) {
			$id = $ids[$i];
			$price = $prices[$i];
			
			if($price != NULL && $price != '') {
				$res[$id] = ['price'=>$price];
			}
		}
		
		return $res;
	}
}
