<?php namespace App\Http\Controllers\Admin\Dealer;

use App\Http\Controllers\Controller;
use App\Dealer;
use App\UserRequest;
use Illuminate\Http\Request;
use Auth;

class DealerRequestsController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index() {
		$dealer = Auth::user()->dealer;
		$dealers = Dealer::with(array('requests' => function($query) use($dealer) {
			$query->leftJoin('proposals',function($join) use($dealer) {
					$join->on('requests.id','=','proposals.request_id')
					->where('proposals.dealer_id','=',$dealer->id);
			})
			->whereNull('proposals.id')
			->select('requests.*');
		}
		,'requests.engine','requests.trim','requests.engine.model','requests.engine.model.make'))->where('id',$dealer->id)
		->first();
		return view('admin.dealer.requests')->with('dealers',$dealers);
	}
}
