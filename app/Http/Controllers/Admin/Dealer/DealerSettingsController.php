<?php namespace App\Http\Controllers\Admin\Dealer;

use App\Http\Controllers\Controller;
use App\Dealer;
use App\Location;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Hash;
use Lang;

class DealerSettingsController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index() {
		$dealer = Auth::user()->dealer;

		return view('admin.dealer.settings')->with('dealer',$dealer);
	}

	public function updateDealer(Request $req) {
		$dealer = Auth::user()->dealer;

		$name = $req->input('name');
		$location = $req->input('town');
		$about = $req->input('about');

		// getting all of the post data
	    $file = $req->file('image');
		// setting up rules
		$rules = array('image' => 'mimes:jpeg,bmp,png','name' => 'required', 'town' => 'required'); //mimes:jpeg,bmp,png and for max size max:10000

		// doing the validation, passing post data, rules and the messages
		$validator = Validator::make($req->all(), $rules);

		if ($validator->fails()) {
			// send back to the page with the input data and errors
			return redirect('admin/dealer/settings')->withInput()->withErrors($validator);
		}
		else {
			$dealer->name = $name;
			$dealer->town = $location;
			$dealer->location = $this->getLocation($location);
			$dealer->about = $about;

			if($file != null) {
				// checking file is valid.
				if ($file->isValid()) {
					$destinationPath = 'img/dealer_assets'; // upload path
					$extension = $file->getClientOriginalExtension(); // getting image extension
					$fileName = rand(11111,99999).'.'.$extension; // renaming image
					$file->move($destinationPath, $fileName); // uploading file to given path
					// sending back with message

					$dealer->logo = $fileName;
					$dealer->save();

					return redirect('admin/dealer/settings')->with('msg', 'Upload successfully');
				}
				else {
					// sending back with error message.
					Session::flash('errors', 'uploaded file is not valid');
					return redirect('admin/dealer/settings');
				}
			}
			else {
				$dealer->save();
				return redirect('admin/dealer/settings')->with('msg','Settings updated');
			}
		}
	}

	private function getLocation($town) {
		$l = Location::where('town',$town)->where('country',Lang::get('messages.country'))->first();
		if($l == null) {
			$loc = $this->googleLoc($town,Lang::get('messages.country'));

			$l = new Location();
			$l->town = $town;
			$l->country = Lang::get('messages.country');
			$l->setLocationAttribute($loc);
			$l->save();
		}
		return $l->location;
	}

	private function googleLoc($town,$country) {
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
		$url .= $town.' '.$country.'&key=AIzaSyBls8o4hSYQCaC6Ijrz2lNCJXIDs0MnaX8';
		$url = str_replace(' ','+',$url);
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CAINFO, getcwd() . "/ca-bundle.crt");

		$server_output = curl_exec ($ch);

		if ($errno = curl_errno($ch)) {
			echo 'Error '.$errno;
		}

		curl_close ($ch);

		$data = json_decode($server_output);
		return [$data->results[0]->geometry->location->lat,$data->results[0]->geometry->location->lng];
	}
}
