<?php namespace App\Http\Controllers\Admin\Dealer;

use App\Http\Controllers\Controller;
use App\Dealer;
use App\Proposal;
use App\UserRequest;
use App\Message;
use Illuminate\Http\Request;
use Auth;
use App\Events\NewOffer;
use App\Events\OfferUpdated;

class DealerProposalsController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index() {
		$dealer = Auth::user()->dealer;
		$proposals = Proposal::with('messages','request','request.engine',
			'request.engine.model','request.engine.trim','request.engine.model.make')
			->where('dealer_id',$dealer->id)->where('status','<>',99)->get();
		return view('admin.dealer.proposals')->with('proposals',$proposals);
	}

	public function createProposal($rid) {
		$req = UserRequest::with('engine','engine.model','engine.trim','engine.model.make')->find($rid);
		return view('admin.dealer.proposal')->with('req',$req)->with('proposal',null);
	}

	public function editProposal($id) {
		$proposal = Proposal::with(array('messages' => function($query) {
			$query->orderby('created_at','desc');
		}),'request',
			'request.engine','request.engine.model','request.engine.trim',
			'request.engine.model.make')->find($id);
		return view('admin.dealer.proposal')->with('req',$proposal->request)->with('proposal',$proposal);
	}

	public function saveProposal(Request $req) {
		$id = $req->input('id');
		$user = $req->input('user_id');

		$isnew = false;
		$p = null;
		if($id == null) {
			$p = new Proposal();
			$isnew = true;
		}
		else {
			$p = Proposal::find($id);
		}

		$p->base_rrp = $req->input('base_rrp');
		$p->base_offer = $req->input('base_offer');
		$p->extra_rrp = $req->input('extra_rrp');
		$p->extra_offer = $req->input('extra_offer');
		$p->status = 0;
		$p->dealer_note = $req->input('dealer_note');
		$p->dealer_id = Auth::user()->dealer_id;
		$p->request_id = $req->input('request_id');
		$p->posted_by = Auth::user()->id;
		$p->save();

		$msg = $req->input('msg');
		if($msg != NULL) {
			$m = new Message();
			$m->proposal_id = $p->id;
			$m->to_id = $user;
			$m->from_id = Auth::user()->id;
			$m->message = $msg;
			$m->save();
		}

		if($isnew) {
			\Event::fire(new NewOffer($user,$id,$p->id));
		}
		else {
			\Event::fire(new OfferUpdated($user,$p->id));
		}

		return redirect('admin/dealer/proposals');
	}

	public function updateStatus($id,$status) {
		if($id != null) {
			$m = Proposal::find($id);
			if($m != null) {
				$m->status = $status != null ? $status : 0;
				$m->save();
			}
		}

		return redirect('admin/dealer/proposals');
	}
}
