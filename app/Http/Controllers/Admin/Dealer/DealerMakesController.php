<?php namespace App\Http\Controllers\Admin\Dealer;

use App\Http\Controllers\Controller;
use App\Dealer;
use App\Make;
use Illuminate\Http\Request;
use Auth;

class DealerMakesController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index() {
		$dealer = Auth::user()->dealer;
		$mks = $dealer->makes->all();
		$dealer->makes = $dealer->makes->keyBy('id');
		
		$makes = Make::all();
		return view('admin.dealer.makes')->with('dealer',$dealer)->with('makes',$makes);
	}
	
	public function saveMakes(Request $req) {
		$makes = $req->input('makes');
		
		$dealer = Auth::user()->dealer;
		$dealer->makes()->sync($makes);
		
		return redirect('admin/dealer/makes')->with('msg', 'Make selection been updated');
	}
}
