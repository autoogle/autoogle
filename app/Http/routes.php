<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get(Lang::get('routing.select-make'), 'WizardController@index');
Route::get(Lang::get('routing.select-model').'/{id?}', 'WizardController@showModels');
Route::get(Lang::get('routing.select-engine').'/{id?}', 'WizardController@showEngines');
Route::get(Lang::get('routing.select-trim').'/{id?}', 'WizardController@showTrims');
//Route::get('select-extras/{id?}/{tid?}', 'WizardController@showExtras');
Route::any(Lang::get('routing.finish').'/{id?}','WizardController@showFinish');

Route::post(Lang::get('routing.thank-you'),'WizardController@saveData');

Route::get(Lang::get('routing.faq'), 'StaticController@showFAQ');
Route::get(Lang::get('routing.tnc'), 'StaticController@showTNC');
Route::get(Lang::get('routing.privacy'), 'StaticController@showPrivacy');

Route::get('process_matching','UserMatchingController@processMatching');


Route::group(['middleware' => ['auth'],
				'namespace' => 'User',
				'prefix' => Lang::get('routing.user')], function() {
		Route::get(Lang::get('routing.user-dashboard').'/{id?}','UserDashboardController@index');
		Route::get(Lang::get('routing.user-offer').'/{id}','UserDashboardController@offerDetails');
		Route::post('message/send','UserDashboardController@sendMessage');

		Route::get('offer/accept/{id}','UserDashboardController@acceptOffer');

		Route::get(Lang::get('routing.user-confirm'),'UserAuthController@confirm');
});

Route::pattern('params', '.*');
Route::group(['namespace' => 'User'], function() {
	Route::get('user/login','UserAuthController@login');
	Route::get( Lang::get('routing.user').'/{passcode}/{params}','UserAuthController@loginByPass');
});

Route::group(['middleware' => ['auth'],
				'namespace' => 'Admin',
				'prefix' => 'admin'], function() {

	Route::group(['middleware' => ['role'],'rr' => 61440],function() {
		Route::get('/','AdminController@index');
	});

	Route::group(['middleware' => ['role'],'rr' => 3840],function() {
		Route::get('/','AdminController@index');
	});

  //Can Pass anything to middleware this way (5.1 allows direct pass)
  Route::group(['prefix' => 'data','middleware' => ['role'],'rr' => 61440],function() {

		Route::get('makes','AdminMakesController@index');
		Route::get('makes/edit/{id}','AdminMakesController@editMake');
		Route::post('makes/update/{id?}',
			['as' => 'make.update','uses' => 'AdminMakesController@updateMake']);
		Route::get('makes/delete/{id}','AdminMakesController@deleteMake');

		Route::get('models','AdminModelsController@index');
		Route::get('models/edit/{id}','AdminModelsController@editModel');
		Route::post('models/update/{id?}',
			['as' => 'model.update','uses' => 'AdminModelsController@updateModel']);
		Route::get('models/delete/{id}','AdminModelsController@deleteModel');
		Route::get('models/toggle/{id}','AdminModelsController@toggleActive');

		Route::get('model-types','AdminModelTypesController@index');
		Route::get('model-types/edit/{id}','AdminModelTypesController@editModelType');
		Route::post('model-types/update/{id?}',
			['as' => 'types.update','uses' => 'AdminModelTypesController@updateModelType']);
		Route::get('model-types/delete/{id}','AdminModelTypesController@deleteModelType');

		Route::get('trims','AdminTrimsController@index');
		Route::get('trims/edit/{id}','AdminTrimsController@editTrim');
		Route::post('trims/update/{id?}',
			['as' => 'trim.update','uses' => 'AdminTrimsController@updateTrim']);
		Route::get('trims/delete/{id}','AdminTrimsController@deleteTrim');

		Route::get('engines','AdminEnginesController@index');
		Route::get('engines/edit/{id}','AdminEnginesController@editEngine');
		Route::post('engines/update/{id?}',
			['as' => 'engine.update','uses' => 'AdminEnginesController@updateEngine']);
		Route::get('engines/delete/{id}','AdminEnginesController@deleteEngine');

		Route::get('extra-categories','AdminCategoriesController@index');
		Route::get('extra-categories/edit/{id}','AdminCategoriesController@editCategory');
		Route::post('extra-categories/update/{id?}',
			['as' => 'categories.update','uses' => 'AdminCategoriesController@updateCategory']);
		Route::get('extra-categories/delete/{id}','AdminCategoriesController@deleteCategory');

		Route::get('extras','AdminExtrasController@index');
		Route::get('extras/edit/{id}','AdminExtrasController@editExtra');
		Route::post('extras/update/{id?}',
			['as' => 'extras.update','uses' => 'AdminExtrasController@updateExtra']);
		Route::get('extras/delete/{id}','AdminExtrasController@deleteExtra');

		/* Utils */
		Route::get('async/models/{id}','AdminAsyncController@getModels');
		Route::get('async/engines/{id}','AdminAsyncController@getEngines');
		Route::get('async/trims/{id}','AdminAsyncController@getTrims');
		Route::get('async/extras/{cID}/{mID}','AdminAsyncController@getExtras');

  });

  Route::group(['prefix' => 'dealer','namespace' => 'Dealer','middleware' => ['role'],'rr' => 3840],function() {
		Route::get('requests','DealerRequestsController@index');
		Route::get('proposals','DealerProposalsController@index');


		Route::get('makes','DealerMakesController@index');
		Route::post('makes/save','DealerMakesController@saveMakes');

		Route::get('create-proposal/{rid}','DealerProposalsController@createProposal');
		Route::get('proposal/edit/{id}','DealerProposalsController@editProposal');
		Route::post('proposal/update',array('as' => 'proposal.update','uses' => 'DealerProposalsController@saveProposal'));
		Route::get('proposal/status-update/{id}/{status}','DealerProposalsController@updateStatus');
		Route::get('ignore-proposal','DealerProposalsController@ignoreProposal');

		Route::get('settings','DealerSettingsController@index');
		Route::post('settings/update','DealerSettingsController@updateDealer');
  });

  Route::group(['prefix' => 'pages',
		'middleware' => ['role'],'rr' => 61440],function() {
			Route::get('/','AdminPagesController@index');
			Route::post('create','AdminPagesController@create');
			Route::get('edit/{page_id}','AdminPagesController@edit');
			Route::get('delete/{page_id}','AdminPagesController@delete');

			Route::post('save','AdminPagesController@saveComponent');
			Route::post('upload','AdminPagesController@upload');
			Route::get('assets','AdminPagesController@getAssets');
  });
});

Route::any('{path}', 'PageController@index')
->where('path',Lang::get('routing.car-reviews').'/(.*)?');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('test',function() {
	return view('emails.user.confirm_sk',array('user' => Auth::user()));
});
