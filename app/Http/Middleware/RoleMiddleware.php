<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware {

    public function handle($request, Closure $next) {
		$action = $request->route()->getAction();
        if (!$request->user()->hasRole($action['rr'])) {
            return redirect('/');
        }

        return $next($request);
    }

}