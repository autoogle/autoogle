<?php namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;


use App\Handlers\Events\OfferUpdatedHandler;
use App\Handlers\Events\NewRequestHandler;
use App\Handlers\Events\OfferAcceptedHandler;

class EventServiceProvider extends ServiceProvider {

	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		'event.name' => [
				'EventListener',
		],

		'App\Events\UserCreated' => [
			'App\Handlers\Events\UserCreatedHandler',
		],

		'App\Events\NewOffer' => [
			'App\Handlers\Events\NewOfferHandler',
		],

		'App\Events\OfferUpdated' => [
			'App\Handlers\Events\OfferUpdatedHandler',
		],

		'App\Events\NewRequest' => [
			'App\Handlers\Events\NewRequestHandler',
		],

		'App\Events\UserReplied' => [
			'App\Handlers\Events\UserRepliedHandler',
		],

		'App\Events\OfferAccepted' => [
			'App\Handlers\Events\OfferAcceptedHandler',
		],
	];

	/**
	 * Register any other events for your application.
	 *
	 * @param  \Illuminate\Contracts\Events\Dispatcher  $events
	 * @return void
	 */
	public function boot(DispatcherContract $events)
	{
		parent::boot($events);
	}

}
