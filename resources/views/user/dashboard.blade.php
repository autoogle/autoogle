@extends('template')

<?php Carbon\Carbon::setLocale(Lang::getLocale()); ?>

@section('content')
<div class="header-wrap">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="text-center">{{ Lang::get('users.your-offers') }}</h1>
      </div>
    </div>
  </div>
</div>

<div class="c-wrap">
  <div class="container">
    <div class="row">

      <div class="col-md-9">
        <br>
        @if($req != null)
          @if(sizeof($req->proposals) > 0)
          @foreach($req->proposals as $p)
            <div class="offer" data-href="{{ URL::to(Lang::get('routing.user').'/'.Lang::get('routing.user-offer').'/'.$p->id) }}">
              <div class="highlight">{{ Lang::get('users.best-offer') }}</div>
              <div class="row">
                <div class="col-md-6">
                  <div class="row">
                    <div class="col-xs-6 col-md-5 text-center">
                      @if($p->agent->img == null)
                        <img src="http://placehold.it/75/98d06e/fff&text={{ $p->agent->getInitials() }}" alt="User Avatar" class="img-circle avatar-img" />
                      @endif
                    </div>
                    <div class="col-xs-6 col-md-7">
                      <span class="txt callout">{{ $p->agent->name }}</span>
                      <span class="txt">{{ $p->dealer->name }}</span>
                      <div class="buttons">
                        <a href="{{ URL::to(Lang::get('routing.user').'/'.Lang::get('routing.user-offer').'/'.$p->id.'#messages') }}" class="btn btn-primary"><i class="glyphicon glyphicon-envelope"></i>
                          @if($p->messages->count() > 0)
                            <span class="badge">{{ $p->messages->count() }}</span>
                          @endif
                        </a>
                        <a href="tel:{{ $p->dealer->telephone }}" class="btn btn-primary"><i class="glyphicon glyphicon-earphone"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-6 col-md-3">

                  <span class="txt callout"><i class="glyphicon glyphicon-map-marker"></i>{{ $req->getDistanceTo(doubleval($req->location[0]),doubleval($req->location[1]),
                      doubleval($p->dealer->location[0]),doubleval($p->dealer->location[1])) }} {{ Lang::get('users.miles-away') }}</span>
                  <span class="txt"><i class="glyphicon glyphicon-ok"></i> {{ Lang::get('users.free-delivery') }}</span>
                  <span class="txt"><i class="glyphicon glyphicon-ok"></i> {{ Lang::get('users.full-tank') }}</span>
                </div>
                <div class="col-xs-6 col-md-3">
                  <span class="txt price">{{ $p->getLocalizedPrice($p->getOffer(),false) }}</span>
                  <span class="txt saving">{{ $p->getLocalizedPrice($p->getSaving(),false) }}</span>
                </div>
              </div>
            </div>
          @endforeach
          @else
            <p class="text-center">{{ Lang::get('users.no-offers') }}</p>
          @endif
        @else
          <p class="text-center">{{ Lang::get('users.no-requests') }}</p>
        @endif
      </div>

      <div class="col-md-3" style="padding-top:70px;">
		<div class="panel panel-primary">
		  <div class="panel-heading">{{ Lang::get('users.request-info') }}</div>
		  <div class="panel-body text-center">
			<span class="txt price">{{ $req->engine->model->make->name }} {{ $req->engine->model->name }}</span>
			<span class="txt saving">{{ $req->trim->name }}</span>
			<span class="txt rrp">{{ $req->engine->name }}</span>
			<br />

			<span class="txt rrp">{{ Lang::get('users.created') }}<br/>{{ $req->created_at->diffForHumans() }}</span>
		  </div>
		</div>

		<div class="panel panel-default">
		  <div class="panel-body text-center">
			<p>{{ Lang::get('users.create-password-desc') }}</p>
			<a href="#" class="btn btn-default">{{ Lang::get('users.create-password-now') }}</a>
		  </div>
		</div>

		<div class="panel panel-primary">
		  <div class="panel-heading">{{ Lang::get('users.new-request') }}</div>
		  <div class="panel-body text-center">
			<p>{{ Lang::get('users.new-request-desc') }}</p>
			<a href="#" class="btn btn-primary" href="{{ URL::to( Lang::get('routing.select-make')) }}">{{ Lang::get('messages.wizard_btn_select_make') }}</a>
		  </div>
		</div>

      </div>

    </div>
  </div><!-- /.container -->
</div>
@stop

@section('script')
<script>
  $(function() {
    var offers = $('.offers-row').hide();

    $('.request-row').click(function(e) {
      e.preventDefault();

      var dataFor = $(this).attr('data-for');
      var idFor = $(dataFor);

      idFor.slideToggle(400, function() {
        if(idFor.is(':visible')) {
          $('.dropdown-request i',idFor).removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
        else {
          $('.dropdown-request i',idFor).removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        }
      });
    });

    $('.offer').click(function(e) {
      window.location = $(this).data('href');
    })
  });
</script>
@endsection
