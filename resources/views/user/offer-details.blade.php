@extends('template')

@section('content')
<div class="container">
  <div class="row">
  <div class="details-header" id="tabs">
    <ul>
      <li class="active"><a href="#">{{ Lang::get('users.details') }}</a></li>
      <li><a href="#">{{ Lang::get('users.dealer') }}</a></li>
      <li><a href="#">{{ Lang::get('users.messages') }}</a></li>
    </ul>
  </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="slides slide_1" id="tabs_details">

        <div class="slide-wrap">

          @include('user.offer-details-details')

          @include('user.offer-details-dealer')

          @include('user.offer-details-messages')

        </div>

      </div>
      <br>
      <div class="accept-wrap">
        <div class="row">
          <div class="col-md-4 col-md-offset-4">
              <a href="{{ URL::to('user/offer/accept/'.$offer->id) }}" class="btn btn-primary btn-block">{{ Lang::get('users.accept-offer') }}</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBls8o4hSYQCaC6Ijrz2lNCJXIDs0MnaX8&callback=initMap&signed_in=true"></script>
<script>
  $(function() {
    $('#tabs a').click(function(e) {
      e.preventDefault();
      $('#tabs li').removeClass('active');
      $(this).parent().addClass('active');

      $('#tabs_details').removeClass (function (index, css) {
          return (css.match (/\bslide_\S+/g) || []).join(' ');
      });
      $('#tabs_details').addClass('slide_'+($(this).parent().index()+1));
    });

    $('#msgForm').submit(function(e) {
      e.preventDefault();

      if($('#btn-input').val() != '') {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
        {
            url : formURL,
            type: "POST",
            data : postData,
            success:function(data, textStatus, jqXHR) {
                var d = $(data).hide();
                $('.chat').append(d);
                d.slideDown('slow');
                $('#btn-input').val('');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
        e.preventDefault(); //STOP default action
      }
    });
  });

  var map;
  function initMap() {
    var myLatLng = {lat: {{ $offer->dealer->location[0] }}, lng: {{ $offer->dealer->location[1] }}};

    map = new google.maps.Map(document.getElementById('map'), {
      center: myLatLng,
      zoom: 10
    });

    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      title: '{{ $offer->dealer->name }}'
    });
  }
</script>
@endsection
