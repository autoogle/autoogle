<div class="slide">
  <div class="row">
    <div class="col-md-6">
      <!-- Logo here -->
      <span class="txt price">{{ $offer->dealer->name }}</span>
      <span class="txt">{!! $offer->dealer->about !!}</span>
      <br>
      <div class="row">
        <div class="col-xs-3 col-sm-2">
          @if($offer->agent->img == null)
            <img src="http://placehold.it/75/98d06e/fff&text={{ $offer->agent->getInitials() }}" alt="User Avatar" class="img-circle avatar-img" />
          @endif
        </div>
        <div class="col-xs-7 col-sm-10">
          <span class="txt callout">{{ $offer->agent->name }}</span>
          <span class="txt">{{ $offer->dealer->name }}</span>
          <div class="buttons">
            <a href="tel:{{ $offer->dealer->telephone }}" class="btn btn-primary"><i class="glyphicon glyphicon-earphone"></i></a>
          </div>
          <br>
        </div>
      </div>

    </div>
    <div class="col-md-6">
      <div id="map" style="min-height:400px;"></div>
    </div>
  </div>
</div>
