<div class="slide">
  <div class="row">
    <div class="col-md-12">

        <ul class="chat">
          @if(sizeof($offer->messages) > 0)
            @each('elements.user-message', $offer->messages, 'm')
          @else
            <p class="text-center">{{ Lang::get('users.no-messages') }}</p>
          @endif
        </ul>
        <div class="panel-footer">
          <form id="msgForm" action="{{ URL::to(Lang::get('routing.user').'/message/send') }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="agent_id" value="{{ $offer->posted_by }}">
            <input type="hidden" name="proposal_id" value="{{ $offer->id }}">
          <div class="input-group">
              <input id="btn-input" type="text" class="form-control" name="message" placeholder="Type your message here..." />
              <span class="input-group-btn">
                  <button class="btn btn-primary" id="btn-chat">
                      {{ Lang::get('users.send') }}</button>
              </span>
          </div>
        </div>

        </form>
    </div>
  </div>
</div>
