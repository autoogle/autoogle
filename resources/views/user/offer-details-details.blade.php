<div class="slide">
  <div class="row">
    <div class="col-md-6 text-center">
      <h2>{{ Lang::get('users.your-offer') }}</h2>
      <div class="row">
        <div class="col-md-6">
          <span class="txt price">{{ $offer->getLocalizedPrice($offer->getOffer(),false) }}</span>
          <span class="txt saving">{{ $offer->getLocalizedPrice($offer->getSaving(),false) }}</span>
          <span class="txt rrp "><strike>{{ Lang::get('users.rrp') }} {{ $offer->getLocalizedPrice($offer->getRRP(),false) }}</strike></span>
          <br>
          <span class="txt callout">{{ Lang::get('users.finance-option') }}</span>
          @if($offer->finance != NULL)
            <span class="txt saving">{{ $offer->finance }}</span>
          @else
            <span class="txt">{{ Lang::get('users.finance-not-available') }}</span>
          @endif
        </div>

        <div class="col-md-6">
          <span class="txt"><i class="glyphicon glyphicon-ok"></i> {{ Lang::get('users.free-delivery') }}</span>
          <span class="txt"><i class="glyphicon glyphicon-ok"></i> {{ Lang::get('users.full-tank') }}</span>
        </div>
      </div>
    </div>
    <div class="col-md-6 text-center">
      <h2>{{ Lang::get('users.your-car') }}</h2>
      <span class="txt price">{{ $offer->request->engine->model->make->name }} {{ $offer->request->engine->model->name }}</span>
      <span class="txt saving">{{ $offer->request->trim->name }}</span>
      <span class="txt rrp">{{ $offer->request->engine->name }}</span>
    </div>
  </div>
</div>
