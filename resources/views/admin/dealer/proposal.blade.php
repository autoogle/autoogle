@extends('admin.dashboard')

@section('title')
{{ Lang::get('dealers.proposal') }}
@stop

@section('content')
{!! Form::model($proposal, array('route' => array('proposal.update','method' => 'POST',
	isset($proposal) ? $proposal->id : null))) !!}
<div class="row">
	<div class="col-md-7">
		<div class="row">
			<div class="col-md-6 text-center">
				<strong>{{ $req->engine->model->make->name }} {{ $req->engine->model->name }}</strong><br/>
								{{ $req->engine->name }} {{ $req->trim->name }}
			</div>
			<div class="col-md-6 text-center">
				<strong>{{ Lang::get('dealers.created') }}</strong><br/>
				{{ $req->created_at->diffForHumans() }}
				{!! Form::hidden('id',isset($proposal) ? $proposal->id : null) !!}
				{!! Form::hidden('request_id',$req->id) !!}
				{!! Form::hidden('user_id',$req->user_id) !!}
			</div>
		</div>

		<div class="row" style="margin-top:25px;">
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('base_rrp',Lang::get('dealers.base-rrp')) !!}

					@if(isset($proposal))
						{!! Form::text('base_rrp',
						null
					,array('class' => 'form-control','readonly' => 'readonly')) !!}
					@else
					{!! Form::text('base_rrp',
						$req->engine->trim->keyBy('id')->get($req->trim_id)->pivot->price
					,array('class' => 'form-control','readonly' => 'readonly')) !!}
					@endif
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('base_offer',Lang::get('dealers.base-offer')) !!}

					@if(isset($proposal))
					{!! Form::text('base_offer',null,
					array('class' => 'form-control')) !!}
					@else
					{!! Form::text('base_offer',$req->engine->trim->keyBy('id')->get($req->trim_id)->pivot->price,
					array('class' => 'form-control')) !!}
					@endif
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('extra_rrp',Lang::get('dealers.extra-rrp')) !!}

					@if(isset($proposal))
					{!! Form::text('extra_rrp',null,array('class' => 'form-control')) !!}
					@else
					{!! Form::text('extra_rrp',0,array('class' => 'form-control')) !!}
					@endif
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('extra_offer',Lang::get('dealers.extra-offer')) !!}

					@if(isset($proposal))
					{!! Form::text('extra_offer',null,array('class' => 'form-control')) !!}
					@else
					{!! Form::text('extra_offer',0,array('class' => 'form-control')) !!}
					@endif
				</div>
			</div>
		</div>

		<div class="row" style="border-top:1px #eee solid;">
			<div class="col-md-12"><strong>{{ Lang::get('dealers.total') }}</strong></div>
		</div>
		<div class="row">
			<div class="col-md-6" >
				<span id="trrp"></span>
			</div>
			<div class="col-md-6">
				<span id="toff"></span> (<strong id="saving"></strong>)
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<br/>
				<div class="form-group">
					{!! Form::label('msg', Lang::get('dealers.message') ) !!}
					{!! Form::textarea('msg',null,array('class' => 'form-control','placeholder' => Lang::get('dealers.new-message'))) !!}
				</div>

				@if(isset($proposal))
					<hr/>
					@if(sizeof($proposal->messages) > 0)

						@foreach($proposal->messages as $msg)
						<div class="media">
						  <div class="media-body">
							<h4 class="media-heading">{{ $msg->from_id == Auth::user()->id ? Lang::get('dealers.you') : Lang::get('dealers.user') }}
								<small class="pull-right">{{ $msg->created_at->diffForHumans() }}</small></h4>
							{!! $msg->message !!}
						  </div>
						</div>
						<hr/>
						@endforeach
					@endif
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">

			</div>
		</div>
	</div>
	<div class="col-md-5">
		<div class="panel panel-primary">
		  <div class="panel-heading">{{ Lang::get('dealers.status') }}</div>
		  <div class="panel-body text-center">
			@if(isset($p))

			@else
				<span class="text-primary">{{ Lang::get('dealers.new') }}</span>
			@endif
		  </div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading"><span class="glyphicon glyphicon-book"></span> {{ Lang::get('dealers.notes') }}</div>
			<div class="panel-body">
				{!! Form::textarea('dealer_note',null,array('class' => 'form-control')) !!}
			</div>
			<div class="panel-footer">
				{{ Lang::get('dealers.user-will-not') }}
			</div>
		</div>
		<button class="btn btn-primary btn-block">{{ Lang::get('dealers.makes-save') }}</button>
	</div>
</div>
{!! Form::close() !!}
@stop

@section('script')
<script type="text/javascript">
	$(function() {
		$('#base_rrp,#base_offer,#extra_rrp,#extra_offer').change(function() {
			var brrp = parseInt($('#base_rrp').val());
			var boff = parseInt($('#base_offer').val());

			var errp = parseInt($('#extra_rrp').val());
			var eoff = parseInt($('#extra_offer').val());

			$('#trrp').html(brrp+errp);
			$('#toff').html(boff+eoff);

			var s = (brrp+errp) - (boff+eoff);
			if(s == 0) {
				$('#saving').removeClass('text-danger');
				$('#saving').removeClass('text-success');
				$('#saving').html(s);
			}
			else if(s > 0) {
				$('#saving').removeClass('text-danger');
				$('#saving').addClass('text-success').html('+'+s);
			}
			else {
				$('#saving').removeClass('text-sucess');
				$('#saving').addClass('text-danger').html(s);
			}
		}).change();
	});
</script>
@stop
