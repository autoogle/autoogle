@extends('admin.dashboard')

@section('title')
{{ Lang::get('dealers.requests') }}
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<tr>
					<th>{{ Lang::get('dealers.request-req') }}</th>
					<th>{{ Lang::get('dealers.request-date') }}</th>
					<th>{{ Lang::get('dealers.request-action') }}</th>
				</tr>
			</thead>
			<tbody>
				@if(sizeof($dealers->requests) > 0)
					@foreach($dealers->requests as $r)
						<tr>
							<td><strong>{{ $r->engine->model->make->name }} {{ $r->engine->model->name }}</strong>
								<br />{{ $r->engine->name }} {{ $r->trim->name }}</td>
							<td>{{ $r->created_at->diffForHumans() }}</td>
							<td>
								<a href="{{ URL::to('admin/dealer/create-proposal/'.$r->id) }}" class="btn btn-primary">{{ Lang::get('dealers.request-make-offer') }}</a>
								<a href="{{ URL::to('admin/dealer/ignore-request/'.$r->id) }}" class="btn btn-default">{{ Lang::get('dealers.request-ignore') }}</a>
							</td>
						</tr>
					@endforeach
				@else
					<tr>
						<td colspan="4">{{ Lang::get('dealers.request-pending') }}</td>
					</tr>
				@endif
			</tbody>
		</table>
	</div>
</div>
@stop

@section('script')

@stop