@extends('admin.dashboard')

@section('title')
{{ Lang::get('dealers.settings') }}
@stop

@section('content')
{!! Form::model($dealer,array('url' => 'admin/dealer/settings/update','method' => 'post', 'files' => true)) !!}
<div class="row">
	<div class="col-md-7">
		<div class="form-group">
		{!! Form::label(Lang::get('dealers.settings-name')) !!}
		{!! Form::text('name',null,array('class' => 'form-control')) !!}
		</div>
		<div class="form-group">
		{!! Form::label(Lang::get('dealers.settings-town')) !!}
		{!! Form::text('town',null,array('class' => 'form-control')) !!}
		</div>
		<div class="form-group">
			{!! Form::label(Lang::get('dealers.settings-about')) !!}
			{!! Form::textarea('about',null,array('class' => 'form-control')) !!}
		</div>
	</div>
</div>
<div class="row" style="margin-top:25px;">
	<div class="col-md-7">
		<div class="form-group">
			{!! Form::label('logo',Lang::get('dealers.settings-logo')) !!}
			<div class="input-group">
				<input type="text" class="form-control" readonly>
				<span class="input-group-btn">
					<span class="btn btn-primary btn-file">
					{{ Lang::get('dealers.settings-browse') }}&hellip; <input type="file" name="image" id="logo">
					</span>
				</span>
			</div>
		</div>
		<button class="btn btn-primary">{{ Lang::get('dealers.makes-save') }}</button>
	</div>
	<div class="col-md-5 text-center">
		@if($dealer->logo != null)
		<img src="{!! asset('img/dealer_assets/'.$dealer->logo) !!}" class="img-responsive img-thumbnail" alt=""/>
		@else
			<div class="well">
			<p>{{ Lang::get('dealers.settings-no-image-set') }}</p>
			</div>
		@endif
	</div>
</div>
{!! Form::close() !!}
@stop

@section('script')
<script type="text/javascript">
$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' {{ Lang::get('dealers.file-select') }}' : label;

        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }

    });
});
</script>
@stop
