@extends('admin.dashboard')

@section('title')
{{ Lang::get('dealers.makes') }}
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<p>{{ Lang::get('dealers.makes-select') }}</p>
	</div>
	
	<div class="col-md-12">
		{!! Form::open(array('url' => URL::to('admin/dealer/makes/save'))) !!}
		
		@if(sizeof($makes) > 0)
			@foreach($makes as $m)
				<div class="checkbox">
					<label>
						<input type="checkbox" name="makes[]" value="{{ $m->id }}" 
							{{ $dealer->makes != NULL && $dealer->makes->get($m->id) ? 'checked' : '' }} > {{ $m->name }}
					</label>
				</div>
			@endforeach
		@endif
		
		<button class="btn btn-primary">{{ Lang::get('dealers.makes-save') }}</button>
		
		{!! Form::close() !!}
	</div>
</div>
@stop

@section('script')

@stop