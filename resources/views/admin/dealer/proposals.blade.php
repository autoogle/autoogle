@extends('admin.dashboard')

@section('title')
{{ Lang::get('dealers.proposals') }}
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>{{ Lang::get('dealers.proposal-requested') }}</th>
					<th>{{ Lang::get('dealers.proposal-offer-created') }}</th>
					<th>{{ Lang::get('dealers.proposal-current-offer') }}</th>
					<th>{{ Lang::get('dealers.proposal-status') }}</th>
				</tr>
			</thead>
			<tbody>
				@if(sizeof($proposals) > 0)
					@foreach($proposals as $p)
					<tr>
						<td><strong>{{ $p->request->engine->model->make->name }} {{ $p->request->engine->model->name }}</strong>
								{{ $p->request->engine->name }} {{ $p->request->trim->name }}</td>
						<td>{{ $p->created_at->diffForHumans() }}</td>
						<td>
							{{ $p->base_offer+$p->extra_offer }} 
							({{ ($p->extra_rrp+$p->base_rrp) - ($p->base_offer+$p->extra_offer) }})
						</td>
						<td>
							@if($p->status == 0)
								<span class="label label-success">{{ Lang::get('dealers.proposal-offer-sent') }}</span>
							@elseif($p->status == 1)
								<span class="label label-warning">{{ Lang::get('dealers.proposal-new-message') }}</span>
							@elseif($p->status == 2)
								<span class="label label-danger">{{ Lang::get('dealers.proposal-rejected') }}</span>
							@elseif($p->status == 10)
								<span class="label label-success">{{ Lang::get('dealers.proposal-accepted') }}</span>
							@endif
						</td>
						<td>
							<a href="{{ URL::to('admin/dealer/proposal/edit/'.$p->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i></a>
							<a href="{{ URL::to('admin/dealer/proposal/status-update/'.$p->id.'/99') }}" class="btn btn-success"><i class="glyphicon glyphicon-euro"></i></a>
						</td>
					</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
</div>
@stop

@section('script')

@stop