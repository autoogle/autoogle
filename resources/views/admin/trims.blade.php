@extends('admin.dashboard')

@section('title')
Trims
@stop

@section('content')
<div class="row">
	<div class="col-md-6">
    	{!! Form::model($trim, array('route' => array('trim.update', $trim->id))) !!}
        	<div class="form-group">
				{!! Form::label('model_id', 'Model') !!}
                {!! Form::select('model_id', ['Please Select']+$models,null,array('class' => 'form-control')) !!}
			</div>
        	<div class="form-group">
				{!! Form::label('name', 'Name') !!}
                {!! Form::text('name',NULL,array('class'=>'form-control')) !!}
			</div>
			<div class="form-group">
				{!! Form::label('name', 'Description') !!}
                {!! Form::textarea('description',NULL,array('class'=>'form-control','id' => 'description')) !!}
			</div>
			<div class="form-group">
				{!! Form::label('name', 'Description SK') !!}
                {!! Form::textarea('description_sk',NULL,array('class'=>'form-control','id' => 'description_sk')) !!}
			</div>
            <p align="right">
            <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button>
            </p>
		{!! Form::close() !!}
    </div>
	<div class="col-md-6">
		<table id="trimsTbl" class="table table-striped">
        	<thead>
				<tr>
			    	<th>#</th>
					<th>Name</th>
					<th>Action</th>
				</tr>
            </thead>
		    @if(sizeof($trims) > 0)
		    	@foreach($trims as $t)
		    	<tr data-model="{{ $t->model_id }}">
		    		<td>{{ $t->id }}</td>
		    		<td>{{ $t->name }}</td>
		    		<td>
                    <a href="{{ URL::to('admin/data/trims/edit/'.$t->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;
                    <a href="{{ URL::to('admin/data/trims/delete/'.$t->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-remove"></i></a></td>
		    	</tr>
		        @endforeach
		    @endif
		</table>
    </div>
</div>
@stop

@section('script')
<script type="text/javascript">
	$(function() {
		$('#model_id').change(function() {
			var val = $(this).val();
			$('#trimsTbl tr').show();
			if(val != null && val != 0) {
				$('#trimsTbl tr').each(function() {
					if($(this).data('model') != null && $(this).data('model') != val) {
						$(this).hide();
					}
				});
			}
		});
		$('#model_id').change();
		
		CKEDITOR.replace( 'description' );
		CKEDITOR.replace( 'description_sk' );
	});
</script>
@stop