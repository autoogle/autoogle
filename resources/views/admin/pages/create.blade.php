@extends('admin.dashboard')

@section('title')
Create Page from "{{ $template->name }}"
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="title">Title</label>
      <input type="text" class="form-control" value="{{ isset($page) ? $page->title : '' }}" name="page_title" id="page_title" />
    </div>
    <div class="form-group">
      <label for="path">Path</label>
      <input type="text" name="page_path" value="{{ isset($page) ? $page->path : '' }}" class="form-control" id="page_path" />
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-8">
      <div id="display">

      </div>
  </div>
  <div class="col-md-4">
    <div class="component_editor">
      <p>Editor</p>
      <form id="editor" action="" {{ isset($page) ? 'data-page_id = '.$page->id : '' }} >
        <div class="components">

        </div>
        <br />
        <a href="#" class="btn btn-primary save">Save</a>
      </form>
    </div>
  </div>
</div>
@stop

@section('script')
<!-- Modal -->
<div class="modal fade" id="assets" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="width:900px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Image Manager</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6">
          <!--  <form action="{{ URL::to('admin/assets/search') }}" method="POST" class="form-inline">
              <div class="form-group">
                <label for="search">Search</label>
                <input type="text" class="form-control" name="search" class="search" />
              </div>
            </form>-->
          </div>
        </div>
        <div class="row">
          <hr>
          <div class="col-md-12 results" style="height:300px;">

          </div>
        </div>
        <div class="row">
          <div class="col-md-12 results-meta">

          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <hr>
            {!! Form::open(array('url'=> URL::to('admin/pages/upload'),'method' => 'POST','files' => true,'class' => 'dropzone')) !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Select</button>
      </div>
    </div>
  </div>
</div>

<script src="{{  asset('scripts/dd.mask.js') }}" charset="utf-8"></script>
<script src="{{  asset('scripts/dd.components.js') }}" charset="utf-8"></script>
<script src="{{  asset('scripts/dropzone.js') }}" charset="utf-8"></script>
<script src="{{  asset('scripts/dd.assetmanager.js') }}" charset="utf-8"></script>
<script src="{{  asset('scripts/json-template.js') }}" charset="utf-8"></script>
<script>
var tpl = {!! $template !!};
var components = new Object();

var assets = $('#assets').assetManager(void 0,{"url":"{{ URL::to('admin/pages/assets') }}"});

$(function() {
  mask.show();
  loadTemplate();
  mask.hide();


  $('#editor').componentEditor(void 0,{
    onSave: function(data,cb) {
      var title = $('#page_title').val();
      var path = $('#page_path').val();
      var tid =  $('#editor').data('tid');
      var page_id = $('#editor').data('page_id');

      if(typeof(title) == 'undefined') {
        alert("Title cannot be empty");
      }
      else if(typeof(path) == 'undefined') {
        alert("Path cannot be empty");
      }
      else {
        //save
        $.postJSON("{{ URL::to('admin/pages/save') }}", {
          "_token":$('meta[name="csrf-token"]').attr('content'),
          "template_id":tid,
          "title":title,
          "path":path,
          "component_data":data,
          "page":page_id
        }, function(data) {
          $('#editor').data('page_id',data.page_id);
          updatePageData(data);
        }, null);
        cb();
      }
    }
  });

  $('#display').on('click','.tools',function(e) {
    e.preventDefault();

    var t = $(this);
    console.log(components[t.data('cid')]);
    $('#editor').data('tid',t.data('tpid'));
    $('#editor').componentEditor("bind",components[t.data('cid')]);

    if(t.parent().has('.populated')) {

      for(var e = 0; e < page_data.length; e++) {
        if(page_data[e].template_part_id == t.data('tpid')) {
          $('#editor').componentEditor("setdata",page_data[e].data);
          break;
        }
      }
    }
  })
});

var page_data = {!! isset($page) && $page != null ? $page->pagedata : '[]' !!};
function updatePageData(data) {
  var found = false;

  for(var i = 0; i < page_data.length; i++) {
    if(page_data[i].id == data.id) {
      page_data[i] = data;
      found = true;
      break;
    }
  }

  if(!found) {
    page_data.push(data);
    $('#pgdid'+data.id).addClass('populated');
  }
}

function constructBinder(component) {
  $('#editor').componentEditor("bind",component);
}

function loadTemplate() {
  var dis = $('#display');
  console.log(tpl.components);
  $.each(tpl.components, function(itm,i) {
    components[i.id] = i;
    if(i.pivot.parent_id == null) {
      var s = createShim(i);
      addChildren(tpl.components,i.pivot.id,s);
      dis.append(s);
    }
  });

  dis[0].outerHTML = dis[0].outerHTML.replace(/\$[A-z]+\$/gm,'<div class="component blank"></div>');
}

function reEscape(s) {
    return s.replace(/([.*+?^$|(){}\[\]])/mg, "\\$1");
}

function replaceAll(find, replace, str) {
  return str.replace(new RegExp(find, 'g'), replace);
}

function addChildren(comps,id,el) {
  var res = [];

  $.each(comps,function(i,itm) {
    components[itm.id] = itm;

    if(itm.pivot.parent_id == id) {
      var s = createShim(itm);
      addChildren(comps,itm.pivot.id,s);
      
      if(typeof(itm.pivot.parent_meta) != 'undefined' && itm.pivot.parent_meta != null) {
	      var idx;
	      if(res.hasOwnProperty(itm.pivot.parent_meta)) {
	      	res[itm.pivot.parent_meta].push(s);
	      }
	      else {
	      	res[itm.pivot.parent_meta] = [s];
	      }
      }
    }
  });
  console.log(res);
  console.log(res.length + " sets");
  
  for(key in res) {
  	var value = res[key];
  	$('div',el).filter(function() {
    	console.log(key);
        return $.trim($(this).text()) === "$"+key+"$";
      }).each(function() {
        $(this).html(function(idx,html) {
            
            var sb = "";
            $.each(value,function(i,val) {
            	sb += $(val)[0].outerHTML;
            });
            console.log(sb);
            return html.replace('\$'+key+'\$',sb);
        });
      });
  }
}

function createShim(i) {
  if(i.name.startsWith("Grid")) {
    return $(i.tpl).removeClass('container');
  }
  else {
    var el = $('<div />');
    var inner = $('<div />').text(i.name);

    var tools = $('<div class="tools"><a href="" class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i></a></div>');
    tools.attr('data-tpid',i.pivot.id);
    tools.attr('data-cid',i.pivot.component_id);

    inner.append(tools);

    el.addClass('component').append(inner);

    for(var e = 0; e < page_data.length; e++) {
      if(page_data[e].template_part_id == i.pivot.id) {
        el.addClass('populated');
        break;
      }
    }

    return el;
  }
}


var capitalize = function (input) {
  if(input == null) return null;
  return input.replace(/^./, function (match) {
    return match.toUpperCase();
  });
};

$.postJSON = function(url, data, success, args) {
  args = $.extend({
    url: url,
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    async: true,
    success: success
  }, args);
  return $.ajax(args);
};
</script>
@stop
