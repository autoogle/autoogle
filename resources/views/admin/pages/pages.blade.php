@extends('admin.dashboard')

@section('title')
Pages
@stop

@section('content')
<div class="row">
	<div class="col-md-2 col-md-offset-10">
		<p><a href="#" data-toggle="modal" data-target="#createTemplate" class="btn btn-primary">Create</a></p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>#</th>
					<th>Title</th>
					<th>Path</th>
				</tr>
			</thead>
			<tbody>
				@if(sizeof($pages) > 0)
					@foreach($pages as $p)
					<tr>
						<td>{{ $p->id }}</td>
						<td>{{ $p->title }}</td>
						<td>{{ $p->path }}</td>
						<td><a href="{{ URL::to(
							str_replace('car-reviews',
								Lang::get('routing.car-reviews'),
								$p->path)
							) }}" target="_blank" class="btn btn-default">View</a>
						<a href="{{ URL::to('admin/pages/edit/'.$p->id) }}" class="btn btn-primary">Edit</a>
						<a href="{{ URL::to('admin/pages/delete/'.$p->id) }}" class="btn btn-danger">Delete</a></td>
					</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
</div>
@stop

@section('script')
<!-- Modal -->
<div class="modal fade" id="createTemplate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
			{!! Form::open(array('url' => URL::to('admin/pages/create'))) !!}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">New Page</h4>
      </div>
      <div class="modal-body">
        <p>
        	To create a new page select the template.
        </p>
				<div class="form-group">
					{!! Form::select('tid', ['0' => 'Please Select']+$templates,'0',array('class'=>'form-control')) !!}
				</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Create Page</button>
      </div>
			{!! Form::close() !!}
    </div>
  </div>
</div>
@stop
