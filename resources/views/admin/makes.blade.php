@extends('admin.dashboard')

@section('title')
Makes
@stop

@section('content')
<div class="row">
	<div class="col-md-6">
    	{!! Form::model($make, array('route' => array('make.update', $make->id))) !!}
        	<div class="form-group">
				{!! Form::label('name', 'Name') !!}
                {!! Form::text('name',NULL,array('class'=>'form-control')) !!}
			</div>
            <p align="right">
            <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button>
            </p>
		{!! Form::close() !!}
    </div>
	<div class="col-md-6">
		<table class="table table-striped">
        	<thead>
				<tr>
			    	<th>#</th>
					<th>Name</th>
					<th>Action</th>
				</tr>
            </thead>
		    @if(sizeof($makes) > 0)
		    	@foreach($makes as $m)
		    	<tr>
		    		<td>{{ $m->id }}</td>
		    		<td>{{ $m->name }}</td>
		    		<td><a href="{{ URL::to('admin/data/makes/edit/'.$m->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;<a href="{{ URL::to('admin/data/makes/delete/'.$m->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-remove"></i></a></td>
		    	</tr>
		        @endforeach
		    @endif
		</table>
    </div>
</div>
@stop