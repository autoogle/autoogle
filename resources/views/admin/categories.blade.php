@extends('admin.dashboard')

@section('title')
Extra Categories
@stop

@section('content')
<div class="row">
	<div class="col-md-6">
    	{!! Form::model($category, array('route' => array('categories.update', $category->id))) !!}
        	<div class="form-group">
				{!! Form::label('name', 'Name') !!}
                {!! Form::text('name',NULL,array('class'=>'form-control')) !!}
			</div>
			<div class="form-group">
				{!! Form::label('name_sk', 'Name SK') !!}
                {!! Form::text('name_sk',NULL,array('class'=>'form-control')) !!}
			</div>
            <p align="right">
            <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button>
            </p>
		{!! Form::close() !!}
    </div>
	<div class="col-md-6">
		<table class="table table-striped">
        	<thead>
				<tr>
			    	<th>#</th>
					<th>Name</th>
					<th>Action</th>
				</tr>
            </thead>
		    @if(sizeof($categories) > 0)
		    	@foreach($categories as $c)
		    	<tr>
		    		<td>{{ $c->id }}</td>
		    		<td>{{ $c->name }}</td>
		    		<td><a href="{{ URL::to('admin/data/extra-categories/edit/'.$c->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;<a href="{{ URL::to('admin/data/extra-categories/delete/'.$c->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-remove"></i></a></td>
		    	</tr>
		        @endforeach
		    @endif
		</table>
    </div>
</div>
@stop