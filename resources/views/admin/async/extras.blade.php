@if(sizeof($extras) > 0)
	@foreach($extras as $e)
    	<tr>
    		<td>{{ $e->id }}</td>
    		<td>{{ $e->name }}</td>
			<td>{{ $e->name_sk }}</td>
            <td>
            	<a href="{{ URL::to('admin/data/extras/edit/'.$e->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;
                <a href="{{ URL::to('admin/data/extras/delete/'.$e->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
    	</tr>
    @endforeach 
@endif