@if(sizeof($engines) > 0)
	@foreach($engines as $e)
    	<tr>
    		<td>{{ $e->id }}</td>
    		<td>{{ $e->name }}</td>
    		<td>{{ $e->horsepower }}</td>
    		<td>{{ $e->consumption }}</td>
    		<td>{{ $e->gearbox->getLocalisedName() }}</td>
    		<td>{{ $e->no_of_gears }}</td>
    		<td>{{ $e->fueltype->getLocalisedName() }}</td>
    		<td>{{ $e->doors }}</td>
            <td>
            	<a href="{{ URL::to('admin/data/engines/edit/'.$e->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;
                <a href="{{ URL::to('admin/data/engines/delete/'.$e->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
    	</tr>
    @endforeach 
@endif