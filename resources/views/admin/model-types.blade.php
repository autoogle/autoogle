@extends('admin.dashboard')

@section('title')
Model Types
@stop

@section('content')
<div class="row">
	<div class="col-md-6">
    	{!! Form::model($type, array('route' => array('types.update', $type->id))) !!}
        	<div class="form-group">
				{!! Form::label('name', 'Name') !!}
                {!! Form::text('name',NULL,array('class'=>'form-control')) !!}
			</div>
			<div class="form-group">
				{!! Form::label('name_sk', 'Name SK') !!}
                {!! Form::text('name_sk',NULL,array('class'=>'form-control')) !!}
			</div>
            <p align="right">
            <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button>
            </p>
		{!! Form::close() !!}
    </div>
	<div class="col-md-6">
		<table class="table table-striped">
        	<thead>
				<tr>
			    	<th>#</th>
					<th>Name</th>
					<th>Name SK</th>
					<th>Action</th>
				</tr>
            </thead>
		    @if(sizeof($types) > 0)
		    	@foreach($types as $t)
		    	<tr>
		    		<td>{{ $t->id }}</td>
		    		<td>{{ $t->name }}</td>
					<td>{{ $t->name }}</td>
		    		<td><a href="{{ URL::to('admin/data/model-types/edit/'.$t->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;<a href="{{ URL::to('admin/data/model-types/delete/'.$t->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-remove"></i></a></td>
		    	</tr>
		        @endforeach
		    @endif
		</table>
    </div>
</div>
@stop