<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">
<title>@yield('title') | {{ Lang::get('dealers.title') }}</title>

<link href="{{ URL::to('css/admin.min.css') }}" rel="stylesheet">
</head><body>
<div >
	<nav class="navbar navbar-inverse navbar-fixed-top">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
	      <a class="navbar-brand" href="#">AC Admin</a> </div>
	    <div id="navbar" class="navbar-collapse collapse">
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="#">{{ Auth::user()->name }}</a></li>
	        <li><a href="{{ URL::to('auth/logout') }}">{{ Lang::get('dealers.log-out') }}</a></li>
	      </ul>
	    </div>
	  </div>
	</nav>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-3 col-md-2 sidebar navbar-sidebar navbar-inverse">
      <ul class="nav navbar-nav  side-nav">
        <li class="{{ Request::is('admin') ? 'active' : '' }}"> <a href="{{ URL::to('admin') }}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a> </li>

		<!-- Data Manager Menu [ADMIN] -->
		@if(Auth::user()->hasRole(\App\User::ADMIN))
		<li class="{{ Request::is('admin/data*') ? 'active' : '' }}"> <a href="javascript:;" data-toggle="collapse" data-target="#dm"><i class="fa fa-fw fa-arrows-v"></i> Data Manager <i class="fa fa-fw fa-caret-down"></i></a>
          <ul id="dm" class="{{ Request::is('admin/data*') ? '' : 'collaspe' }}">
            <li class="{{ Request::is('admin/data/makes') ? 'active' : '' }}">
            	<a href="{{ URL::to('admin/data/makes') }}">Makes</a>
            </li>
            <li class="{{ Request::is('admin/data/models') ? 'active' : '' }}">
            	<a href="{{ URL::to('admin/data/models') }}">Models</a>
            </li>
			<li class="{{ Request::is('admin/data/model-types') ? 'active' : '' }}">
            	<a href="{{ URL::to('admin/data/model-types') }}">Model Types</a>
            </li>
            <li class="{{ Request::is('admin/data/trims') ? 'active' : '' }}">
            	<a href="{{ URL::to('admin/data/trims') }}">Trims</a>
            </li>
            <li class="{{ Request::is('admin/data/engines') ? 'active' : '' }}">
            	<a href="{{ URL::to('admin/data/engines') }}">Engines</a>
            </li>
            <li class="{{ Request::is('admin/data/extra-categories') ? 'active' : '' }}">
            	<a href="{{ URL::to('admin/data/extra-categories') }}">Extras Categories</a>
            </li>
            <li class="{{ Request::is('admin/data/extras') ? 'active' : '' }}">
            	<a href="{{ URL::to('admin/data/extras') }}">Extras</a>
            </li>
          </ul>
        </li>
		@endif
		<!-- ./Data Manager Menu [ADMIN] -->

		<!-- Dealer Menu [DEALER] -->
		@if(Auth::user()->hasRole(\App\User::DEALER))
		<li class="{{ Request::is('admin/dealer*') ? 'active' : '' }}"> <a href="javascript:;" data-toggle="collapse" data-target="#dm"><i class="fa fa-fw fa-arrows-v"></i> {{ Lang::get('dealers.dealers-dash') }} <i class="fa fa-fw fa-caret-down"></i></a>
			<ul id="dm" class="{{ Request::is('admin/dealer*') ? '' : 'collaspe' }}">
				<li class="{{ Request::is('admin/dealer/requests') ? 'active' : '' }}">
					<a href="{{ URL::to('admin/dealer/requests') }}">{{ Lang::get('dealers.requests') }}</a>
				</li>
				<li class="{{ Request::is('admin/dealer/proposals') ? 'active' : '' }}">
					<a href="{{ URL::to('admin/dealer/proposals') }}">{{ Lang::get('dealers.proposals') }}</a>
				</li>
				<li class="{{ Request::is('admin/dealer/makes') ? 'active' : '' }}">
					<a href="{{ URL::to('admin/dealer/makes') }}">{{ Lang::get('dealers.makes') }}</a>
				</li>
				<li class="{{ Request::is('admin/dealer/settings') ? 'active' : '' }}">
					<a href="{{ URL::to('admin/dealer/settings') }}">{{ Lang::get('dealers.settings') }}</a>
				</li>
			</ul>
    </li>
		@endif
		<!-- ./Dealer Menu [DEALER] -->

		<!-- Pages Menu [ADMIN] -->
		@if(Auth::user()->hasRole(\App\User::ADMIN))
		<li class="{{ Request::is('admin/pages*') ? 'active' : '' }}"> <a href="javascript:;" data-toggle="collapse" data-target="#pg"><i class="fa fa-fw fa-arrows-v"></i> {{ Lang::get('dealers.dealers-dash') }} <i class="fa fa-fw fa-caret-down"></i></a>
			<ul id="pg" class="{{ Request::is('admin/pages*') ? '' : 'collaspe' }}">
				<li class="{{ Request::is('admin/pages') ? 'active' : '' }}">
					<a href="{{ URL::to('admin/pages') }}">{{ Lang::get('pages.pages') }}</a>
				</li>
			</ul>
    </li>
		@endif
		<!-- ./Pages Menu [ADMIN] -->
      </ul>
    </div>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		<a href="#" class="sidebar_toggle btn btn-primary">></a>
      <h1 class="page-header">@yield('title')</h1>

      @if(Session::has('msg'))
      	<div class="alert alert-success" role="alert"><strong>{{ Lang::get('dealers.save-success') }}</strong> {{ Session::get('msg') }}</div>
      @endif
      @if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	  @endif

      @yield('content')
    </div>
  </div>
</div>
<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="//cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>
<script type="text/javascript">
$(function() {
	$('.sidebar_toggle').click(function() {
		$('.sidebar').toggleClass('active');
	});
});
</script>
@yield('script')
</body>
</html>
