@extends('admin.dashboard')

@section('title')
Extras
@stop

@section('content')
{!! Form::model($extra, array('route' => array('extras.update', $extra->id))) !!}
<div class="row">
	<div class="col-md-12">
        	<div class="row">
	        	<div class="col-md-3">
	            	<div class="form-group">
						{!! Form::label('make_id', 'Make') !!}
	                	{!! Form::select('make_id', ['Please Select']+$makes,
                        	( isset($extra) && $extra->model != null ? $extra->model->make->id : null ),
                          array('class' => 'form-control')) !!}
					</div>
	            </div>
	        	<div class="col-md-3">
	            	<div class="form-group">
						{!! Form::label('model_id', 'Model') !!}
	                	{!! Form::select('model_id', ['Please Select'],
                        	null,
                        	array('class' => 'form-control', 'data-val' => 
                            	( isset($extra) ? $extra->model_id : null ))) !!}
					</div>
	            </div>
				<div class="col-md-3">
	            	<div class="form-group">
						{!! Form::label('category_id', 'Category') !!}
	                	{!! Form::select('category_id', ['Please Select']+$categories,
                        	( isset($extra) ? $extra->category_id : null ),
                          array('class' => 'form-control')) !!}
					</div>
	            </div>
            </div>
    </div>
    <div class="col-md-6">
      	<div class="form-group">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name', null, array('class' => 'form-control')) !!}
        </div>
		<div class="form-group">
            {!! Form::label('name_sk', 'Name SK') !!}
            {!! Form::text('name_sk', null, array('class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-md-6">
    	<h2>Trims</h2>
        <table id="trims" class="table table-bordered">
        	<thead>
            	<tr>
            		<td>Name</td>
            		<td>Price</td>
            	</tr>
            </thead>
            <tbody>
            	
                
            </tbody>
        </table>
    </div>
    <div class="col-md-12" align="center">
    	<button class="btn btn-primary">Save</button>
    </div>
    <div class="col-md-12">
    	<hr />
    	<table id="extras" class="table table-striped">
    		<thead>
            	<tr>
            		<th>ID</th>
            		<th>Name</th>
            		<th>Name SK</th>
                    <th>Action</th>
            	</tr>
            </thead>
            <tbody>
              
            </tbody>
    	</table>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('script')
<script type="text/javascript">
	var eTrims = {!! isset($extra) ? $extra->trim : '' !!};
</script>
<script type="text/javascript">
        var eID = {{ isset($extra) && $extra->id != NULL ? $extra->id : 'null' }};
      
	$(function() {
		$('#make_id').change(function() {
			var val = val = $(this).val();
			if(val == null || val == 0) {
				val = $(this).data('val');
			}
			$('#model_id').html('<option value="0">Please Select</option>')
			$.getJSON("{{ URL::to('admin/data/async/models/') }}/"+val,function(data) {
				$.each(data,function(i,itm) {
					var t = $('<option />', {value: itm.id,html: itm.name});
					$('#model_id').append(t);
				});
				
				if($('#model_id').data('val') != null) {
					$('#model_id').val($('#model_id').data('val'));
					$('#model_id').change();
				}
			});
		});
		$('#make_id').change();
		
		$('#model_id,#category_id').change(function() {
			getExtras();
		});
		
		$('#model_id').change(function() {
			var val = $(this).val();
			if(val == null || val == '')
				val = $(this).data('val');

			if(val != null && val != 0) {
				$.ajax("{{ URL::to('admin/data/async/trims/') }}/"+val)
					.done(function(data) {
					$('#trims>tbody').empty();
					console.log(data);
					$.each(data,function(i) {
						var tr = $('<tr/>');
						
						var p = findExtra(this.id,eID);
						
						var td = $('<td/>').text(this.name);
						td.append($('<input type="hidden" name="id[]" />').val(this.id));
						tr.append(td);
						
						var tl = $('<td/>');
						
						var input = $('<input type="number" name="price[]" class="form-control" />');
						tl.append(input);
						tr.append(tl);
						
						console.log(input.prop('outerHTML'));
						
						$('#trims>tbody').append(tr);
						if(p != -1) {
							console.log(p.pivot.price);
							input.val(p.pivot.price);
						}
					});
					
				});
			}
		});
	});
	
	function findExtra(id,cat) {
		var r = $.grep(eTrims,function(e){return e.id == id && e.pivot.extra_category_item_id == cat; });
		if(r.length == 1) {
			console.log(r[0]);
			return r[0];
		}
		else return -1;
	}
	
	function getExtras() {
		var mID = $('#model_id').val();
		var cID = $('#category_id').val();
		
		if(mID == null || mID == '')
			mID = $('#model_id').data('val');
		
		if(cID == null || cID == '')
			cID = $('#category_id').data('val');
		
		$('#extras>tbody>tr').remove();
		if((mID != null && mID != 0) && (cID != null && cID != 0)) {
			$.ajax({
				url:"{{ URL::to('admin/data/async/extras/') }}/"+cID+"/"+mID
			}).done(function(data) {
				$("#extras>tbody").append(data);
				
				$('#model_id').val(mID);
				$('#category_id').val(cID);
			});
		}
	}
</script>
@stop