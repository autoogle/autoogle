@extends('admin.dashboard')

@section('title')
Engines
@stop

@section('content')
{!! Form::model($engine, array('route' => array('engine.update', $engine->id))) !!}
<div class="row">
	<div class="col-md-12">
        	<div class="row">
	        	<div class="col-md-3">
	            	<div class="form-group">
						{!! Form::label('make_id', 'Make') !!}
	                	{!! Form::select('make_id', ['Please Select']+$makes,
                        	( isset($engine) && $engine->model != null ? $engine->model->make->id : null ),
                          array('class' => 'form-control')) !!}
					</div>
	            </div>
	        	<div class="col-md-3">
	            	<div class="form-group">
						{!! Form::label('model_id', 'Model') !!}
	                	{!! Form::select('model_id', ['Please Select'],
                        	null,
                        	array('class' => 'form-control', 'data-val' => 
                            	( isset($engine) && $engine->model != null ? $engine->model->id : null ))) !!}
					</div>
	            </div>
            </div>
    </div>
    <div class="col-md-6">
      	<div class="form-group">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name', null, array('class' => 'form-control')) !!}
        </div>
        <div class="form-group">
            {!! Form::label('gearbox_id', 'Gearbox') !!}
            {!! Form::select('gearbox_id', ['Please Select']+$gears,null,
            	array('class' => 'form-control')) !!}
        </div>
        <div class="form-group">
            {!! Form::label('fueltype_id', 'Fuel') !!}
            {!! Form::select('fueltype_id', ['Please Select']+$fuels,null,
            	array('class' => 'form-control')) !!}
        </div>
        <div class="row">
        	<div class="col-md-6">
            	<div class="form-group">
		            {!! Form::label('horsepower', 'Horsepower') !!}
		            {!! Form::number('horsepower', null, array('class' => 'form-control','step' => 'any')) !!}
		        </div>
            </div>
        	<div class="col-md-6">
            	<div class="form-group">
		            {!! Form::label('consumption', 'Consumption') !!}
		            {!! Form::text('consumption', null, array('class' => 'form-control')) !!}
		        </div>
            </div>
            <div class="col-md-6">
            	<div class="form-group">
		            {!! Form::label('no_of_gears', 'No. Gears') !!}
		            {!! Form::number('no_of_gears', null, array('class' => 'form-control','step' => 'any')) !!}
		        </div>
            </div>
            <div class="col-md-6">
            	<div class="form-group">
		            {!! Form::label('doors', 'Doors') !!}
		            {!! Form::number('doors', null, array('class' => 'form-control','step' => 'any')) !!}
		        </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
    	<h2>Trims</h2>
        <table id="trims" class="table table-bordered">
        	<thead>
            	<tr>
            		<td>Name</td>
            		<td>Price</td>
            	</tr>
            </thead>
            <tbody>
            	
                
            </tbody>
        </table>
    </div>
    <div class="col-md-12" align="center">
    	<button class="btn btn-primary">Save</button>
    </div>
    <div class="col-md-12">
    	<hr />
    	<table id="engines" class="table table-striped">
    		<thead>
            	<tr>
            		<th>ID</th>
            		<th>Name</th>
            		<th>Horsepower</th>
            		<th>Consumption</th>
            		<th>Gearbox</th>
            		<th>No. of Gears</th>
            		<th>Fueltype</th>
            		<th>Doors</th>
                    <th>Action</th>
            	</tr>
            </thead>
            <tbody>
              
            </tbody>
    	</table>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('script')
<script type="text/javascript">
	var eTrims = {!! isset($engine) ? $engine->trim : '' !!};
</script>
<script type="text/javascript">
        var eID = {{ isset($engine) && $engine->id != NULL ? $engine->id : 'null' }};
      
	$(function() {
		$('#make_id').change(function() {
			var val = val = $(this).val();
			if(val == null || val == 0) {
				val = $(this).data('val');
			}
			$('#model_id').html('<option value="0">Please Select</option>')
			$.getJSON("{{ URL::to('admin/data/async/models/') }}/"+val,function(data) {
				$.each(data,function(i,itm) {
					var t = $('<option />', {value: itm.id,html: itm.name});
					$('#model_id').append(t);
				});
				
				if($('#model_id').data('val') != null) {
					$('#model_id').val($('#model_id').data('val'));
					$('#model_id').change();
				}
			});
		});
		$('#make_id').change();
		
		$('#model_id').change(function() {
			var val = $(this).val();
			if(val == null || val == '')
				val = $(this).data('val');
			$('#engines>tbody>tr').remove();
			if(val != null && val != 0) {
				$.ajax({
					url:"{{ URL::to('admin/data/async/engines/') }}/"+val
				}).done(function(data) {
					$("#engines>tbody").append(data);
					
					$('#model_id').val(val);
				});
				
				$.ajax("{{ URL::to('admin/data/async/trims/') }}/"+val)
					.done(function(data) {
					$('#trims>tbody').empty();
					$.each(data,function(i) {
						var tr = $('<tr/>');
						
						var p = findEngine(this.id,eID);
						
						var td = $('<td/>').text(this.name);
						td.append($('<input type="hidden" name="id[]" />').val(this.id));
						tr.append(td);
						
						var tl = $('<td/>');
						
						var input = $('<input type="number" name="price[]" class="form-control" />');
						tl.append(input);
						tr.append(tl);
						
						console.log(input.prop('outerHTML'));
						
						$('#trims>tbody').append(tr);
						if(p != -1) {
							console.log(p.pivot.price);
							input.val(p.pivot.price);
						}
					});
					
				});
			}
		});
	});
	
	function findEngine(id,eng) {
		var r = $.grep(eTrims,function(e){return e.id == id && e.pivot.engine_id == eng; });
		if(r.length == 1) {
			console.log(r[0]);
			return r[0];
		}
		else return -1;
	}
</script>
@stop