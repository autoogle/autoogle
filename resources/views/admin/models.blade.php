@extends('admin.dashboard')

@section('title')
Models
@stop

@section('content')
<div class="row">
	<div class="col-md-6">
    	{!! Form::model($model, array('route' => array('model.update', $model->id))) !!}
        	<div class="form-group">
				{!! Form::label('make_id', 'Make') !!}
                {!! Form::select('make_id', ['Please Select']+$makes,null,array('class' => 'form-control')) !!}
			</div>
        	<div class="form-group">
				{!! Form::label('name', 'Name') !!}
                {!! Form::text('name',NULL,array('class'=>'form-control')) !!}
			</div>
			<div class="form-group">
				{!! Form::label('model_type_id', 'Type') !!}
                {!! Form::select('model_type_id', ['Please Select']+$types,null,array('class' => 'form-control')) !!}
			</div>
            <p align="right">
            <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button>
            </p>
		{!! Form::close() !!}
    </div>
	<div class="col-md-6">
		<table id="modelsTbl" class="table table-striped">
        	<thead>
				<tr>
			    	<th>#</th>
					<th>Name</th>
					<th>Action</th>
				</tr>
            </thead>
		    @if(sizeof($models) > 0)
		    	@foreach($models as $m)
		    	<tr data-make="{{ $m->make_id }}" class="{{ $m->inactive ? 'warning' : '' }} s">
		    		<td>{{ $m->id }}</td>
		    		<td>{{ $m->name }}</td>
		    		<td>
                    <a href="{{ URL::to('admin/data/models/edit/'.$m->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;
                    <a href="{{ URL::to('admin/data/models/toggle/'.$m->id) }}" class="btn btn-primary"><i class="glyphicon {{ $m->inactive ? 'glyphicon glyphicon-eye-open' : 'glyphicon glyphicon-eye-close' }}"></i></a>&nbsp;
                    <a href="{{ URL::to('admin/data/models/delete/'.$m->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-remove"></i></a></td>
		    	</tr>
		        @endforeach
		    @endif
            <tfoot>
            	<tr>
                	<td colspan="3">* Yellow rows are inactive and not published</td>
                </tr>
            </tfoot>
		</table>
    </div>
</div>
@stop

@section('script')
<script type="text/javascript">
	$(function() {
		$('#make_id').change(function() {
			var val = $(this).val();
			$('#modelsTbl tr').show();
			if(val != null && val != 0) {
				$('#modelsTbl tr').each(function() {
					if($(this).data('make') != null && $(this).data('make') != val) {
						$(this).hide();
					}
				});
			}
		});
		$('#make_id').change();
	});
</script>
@stop