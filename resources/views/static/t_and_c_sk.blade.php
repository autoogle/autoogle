@extends('template')

@section('content')
<div class="container" style="padding-top:25px;">
<h1>Podmienky používania</h1>
<h3>Úvod</h3>
<p>Tieto podmienky používania sa vzťahujú pre užívateľov Autoogle web stránok a popisujú ako môžete používať web stránky Autoogle. Autoogle ochrana osobných údajov popisuje ako používame dáta týkajúce sa vás a tvorí súčasť podmienok používania. Tieto podmienky ustanovujú legálnu dohodu medzi nami a vami, prosíme, prečítajte si všetko pozorne.</p>
<h3>1. O tejto webovej stránke a podmienky používania Autoogle.</h3>
<p>Autoogle.eu (ďalej len &quot;Web stránka&quot;), je online služba vlastnená a prevádzkovaná spoločnosťou Autoogle (&quot;my&quot;, &quot;nás&quot;,&quot;nami&quot; alebo &quot;naše&quot;), ktorá umožňuje jednotlivcom ľahko porovnať cenové ponuky na nové autá.Tieto podmienky používania platia pre všetkých, ktorí používajú Web stránku a tvoria právnu dohodu medzi vami a Autoogle. Používaním Web stránky potvrdzujete, že ste pochopili a prijali (a ste schopní pochopiť a prijať), tieto podmienky používania a že súhlasíte, že sa nimi bude riadiť.</p>
<br />
<p>Nesmiete použiť Web stránku, ak:</p>
<ul>
  <li>
    <p>Nesúhlasíte s týmito podmienkami používania</p>
  </li>
  <li>
    <p>Ste mladší ako 18 rokov</p>
  </li>
  <li>
    <p>Konáte v mene spoločnosti či agentúry</p>
  </li>
</ul>
<br />
<p>Autoogle môže vykonať zmeny v týchto podmienkach používania z času na čas. Ak nie ste spokojní s akoukoľvek zmenou, musíte prestať používať Web stránku. Ak budete používať Web stránku naďalej, bude to znamenať, že súhlasíte s týmito zmenami v podmienkach používania. Môžeme vám oznámiť kľúčové zmeny, ale vy by ste mali z času na čas skontrolovať tieto podmienky používania, aby ste si boli vedomí prípadných zmien. V prípade potreby vám môže oznámiť zmeny e-mailom, alebo tým, že zverejníme oznámenie na Web stránke.</p>
<ul>
  <li>
    <h3>2. Ako formou sú ponuky na auto prijaté a akceptované na web stránke</h3>
  </li>
</ul>
<p>My autá nepredávame. Na našej webovej stránke umožňujeme autorizovaným dealerom (&ldquo;predajcom&rdquo;) dať vám cenovú ponuku na kúpu nového auta. Ak sa rozhodnete prijať ponuku a kúpiť auto od dealera, uzatvoríte zmluvu s konkrétnym dealerom. My nie sme súčasťou tej zmluvy. Ak akceptujete ponuku od dealera cez našu web stránku, nie ste zaviazaný auto kúpiť. </p>
<p>Ak vám dealer pošle cenovú ponuku na nové auto, dealer vám nie je zaviazaný predať auto za ponúknutú cenu, alebo predať auto vôbec.</p>
<br />
<p>Cenové ponuky sú prijaté a akceptované nasledovne:</p>
<ul>
  <li>
    <p>Vy si na našej web stránke nakonfigurujete nové auto podľa vašich predstáv a &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;požiadate dealerov o cenové ponuky.</p>
  </li>
  <li>
    <p>Auto dealer vám pošle cenovú ponuku, alebo ponuka je poslaná v ich mene. Ponuka sa zhoduje s vašou konfiguráciou.</p>
  </li>
  <li>
    <p>Dealerová ponuka môže obsahovať cenu, čas doručenia, ďalšie náklady a výbavu navyše. Všetko je súčasťou ponuky.</p>
  </li>
  <li>
    <p>Vy zvážite všetky prijaté ponuky a ak máte záujem niektorú ponuku prijať, kontaktujete priamo dealera.</p>
  </li>
  <li>
    <p>Vy a auto dealer môžete potom uzavrieť kúpnu zmluvu na nové auto, na základe podmienok ponuky ktorú ste akceptovali od dealera. To môže zahŕňať zaplatenie zálohy dealerovy, financovanie, alebo iných podmienok týkajúcich sa kúpnej zmluvy.</p>
  </li>
</ul>
<br />
<p>My nemôžeme zaručiť, že dostanete nejaké cenové ponuky, alebo kedy budú ponuky poslané dealerom. My môžeme ponuky moderovať, ale nie sme povinný to robiť.</p>
<p>My alebo auto dealer môžeme hocikedy ponuky upravovať, alebo odstraňovať. Ak sa rozhodnete kúpiť auto od dealera, tak uzatvárate dohodu len s dealerom, čo nezahrňuje web stránku v žiadnom zmysle. Musíte sa uistiť, že všetky informácie ktoré máme o vás, sú pravdivé, kompletné, nezavádzajúce a aktuálne. </p>
<h3>3. Práva na používanie a práva duševného vlastníctva</h3>
<p>Web stránka je k dispozícii iba pre vaše súkromné, nekomerčné, osobné použitie.To vám nedáva žiadne práva na vlastníctvo na Web stránke. Pokiaľ nie je uvedené inak, všetky autorské práva, dizajnové práva, práva k databázam, patentové práva, ochranné známky, obchodné práva a ďalšie práva duševného vlastníctva na Web stránke patria Autoogle, alebo Autoogle je ich oprávnená používať.</p>
<br />
<p>Všetky obchodné názvy a ochranné známky tretích strán sú majetkom ich príslušných vlastníkov a Autoogle neposkytuje žiadne záruky ani zastúpenie vo vzťahu k nim. Ak nie je uvedené inak, my nepodporujeme a nie sme spojení so žiadnym z majiteľov takýchto práv a preto my nemôžeme poskytnúť žiadnu licenciu na vykonávanie týchto práv. Autoogle a naše logo sú ochranné známky, ktoré patria nám. Nedávame žiadne povolenie na použitie týchto ochranných známok, a takéto použitie môže predstavovať porušenie našich práv.</p>
<h3>4. Obmedzenia a povinnosti</h3>
<p>Súhlasíte, že budete dodržiavať tieto podmienky používania a všetkých pravidlá vzťahujúce sa na používanie Web stránky.</p>
<br />
<p>Nebudete:</p>
<ul>
  <li>
    <p>Hackovať, upravovať, spätne dešifrovať alebo vytvárať odvodené diela na Web stránke alebo akejkoľvek jej časti.</p>
  </li>
  <li>
    <p>Získavať neoprávnený prístup k akejkoľvek neprístupnej časti Web stránky.</p>
  </li>
  <li>
    <p>Odstranovať, upravovať či zakrývať akékoľvek autorské práva, ochrannú známku alebo iné správy o vlastníctve na Web stránke.</p>
  </li>
  <li>
    <p>Vytvárať softvér, ktorý kopíruje alebo napodobňuje dáta alebo funkčnosť Web stránky</p>
  </li>
  <li>
    <p>Zbierať dáta z Web stránky prostredníctvom systematických alebo automatických prostriedkov.</p>
  </li>
  <li>
    <p>Používať svoj prístup k Web stránke pre zasielanie marketingovej komunikácie</p>
  </li>
  <li>
    <p>Používať akýkoľvek počítačový vírus alebo škodlivý kód akejkoľvek povahy v súvislosti s Web stránkou.</p>
  </li>
  <li>
    <p>Dávať k dispozícii akúkoľvek časť Web stránky tretej osobe, ktorá nesúhlasí s týmito podmienkami používania.</p>
  </li>
  <li>
    <p>Kopírovať alebo využívať akúkoľvek časť Web stránky alebo obsah, ktorý obsahuje.</p>
  </li>
  <li>
    <p>Používať Web stránku, alebo akúkoľvek jej časť nečestne, pre nezákonné alebo nemorálne účely.</p>
  </li>
  <li>
    <p>Pokúšať sa robiť niektorý z vyššie uvedených činov.</p>
  </li>
</ul>
<br />
<p>Bez ovplyvnenia akýchkoľvek našich iných práv alebo<a href="http://webslovnik.zoznam.sk/slovensko-anglicky/z%C3%A1konn%C3%A1-n%C3%A1prava"> zákonných náprav</a>, ak by sme mali obstojné podozrenie, že ste porušili niektorú z týchto podmienok používania môžeme pozastaviť alebo ukončiť váš prístup na Web stránku, alebo podniknúť akékoľvek iné opatrenia pokladané za potrebné pre obhájenie alebo podporovanie akýchkoľvek z našich práv alebo záujmov.</p>
<h3>5. Komunikačný obsah</h3>
<p>Web stránka umožňuje zobrazenie obsahu tretích strán (&quot;Užívateľský obsah&quot;) bez preskúmania alebo moderácie. Web stránka je preto iba sprostredkovateľom Užívateľského obsahu. Názory vyjadrené v akomkoľvek Užívateľskom obsahu sú názory jednotlivých autorov, a nie tie Autoogle, pokiaľ nebudeme špecifikovať inak. Odmietame všetku zodpovednosť s rešpektom k akýmkoľvek<a href="http://webslovnik.zoznam.sk/slovensko-anglicky/koment%C3%A1re"> komentár</a>om, názorom či pripomienkam vyjadrených v Užívateľskom obsahu. Používaním Web stránky beriete na vedomie, že nemáme žiadnu zodpovednosť, aby sme preverili všetok Užívateľský obsah, a že všetok Užívateľský obsah je k dispozícii na základe toho, že nie sme povinní kontrolovať alebo uzatvárať nad ním mienku. Avšak môžeme odstrániť Užívateľský obsah z akéhokoľvek dôvodu. Ak umiestnite akýkoľvek Užívateľský obsah na Web stránku, alebo inak publikujete či rozšírite Užívateľský obsah pomocou Web stránky, dávate nám nevýhradnú, neprenosnú, sub licencovanú, bez poplatkovú, neodvolateľnú a trvalú celosvetovú licenciu pre použitie toho Užívateľského obsahu pre akýkoľvek účel.</p>
<br />
<p>Akýkoľvek Užívateľský obsah, ktorý umiestnite na alebo inak publikujete pomocou Web Stránky musí:</p>
<ul>
  <li>
    <p>Nebyť<a href="http://webslovnik.zoznam.sk/slovensko-anglicky/ur%C3%A1%C5%BEliv%C3%BD"> urážlivý</a> alebo v zlom úmysle nepravdivý</p>
  </li>
  <li>
    <p>Neklamať, neurážať, neobťažovať, nebyť nevhodný, nemravný, sprostý, násilný, rasistický alebo fanatický</p>
  </li>
  <li>
    <p>Neporušiť žiadne práva inej osoby, vrátane akéhokoľvek práva duševného vlastníctva, práva dôvery alebo súkromia, alebo akéhokoľvek práva podľa predpisov o ochrane údajov</p>
  </li>
  <li>
    <p>Byť v súlade s platnými zákonmi, predpismi alebo súdnymi príkazmi; alebo</p>
  </li>
  <li>
    <p>Nebyť klamstvo, nepravdivý, nepresný alebo zavádzajúci.</p>
  </li>
</ul>
<br />
<p>Je to známe riziko používania internetu, že ľudia nie sú nutne, kto hovoria, že sú. Ľudia môžu poskytnúť informácie alebo sa správať spôsobom, ktorý je nespoľahlivý, zavádzajúci, nezákonný alebo ilegálny. Nemáme spôsob, ako povedať, či vyhlásenia ostatnými užívateľmi, sú pravdivé. Preto by ste mali vykonávať určitú mieru opatrnosti pri čítaní užívateľského obsahu na akejkoľvek web stránke. Užívaním Web stránky súhlasíte, že používate Web stránku na vlastné nebezpečenstvo.</p>
<h3>6. Užívateľské účty</a></h3>
<p>Je možné, že budete musieť predložiť osobné údaje a vybrať si názov účtu (&quot;Užívateľské meno&quot;), heslo (&quot;Heslo&quot;) a platnú e-mailovú adresu s cieľom vytvorenia účtu na Web stránke (&quot;Účet&quot;), alebo inak používať Web stránku. Ak áno, toto Užívateľské meno a Heslo vám bude pridelené ak úspešne požiadate o registráciu ako Zákazník. Len my sa môžeme rozhodnúť, či aplikácia budú úspešná. Vaše Užívateľské meno a Heslo sú osobné pre vás a môžete mať len jeden Účet. </p>
<br />
<p>Sme oprávnení predpokladať, že akékoľvek využitie vášho účtu je vykonávané vami. Nesiete výhradnú zodpovednosť za akékoľvek používanie Web stránky v rámci vášho Účtu alebo akékoľvek iné použitie vašeho Užívateľského mena a Hesla. Nezdieľajte vaše Heslo so žiadnou inou osobou alebo neumožnite inej osobe používať váš Účet. Nie sme zodpovední za akékoľvek nesprávne použitie vášho Hesla či Účtu alebo akéhokoľvek použitia vášho Hesla či Účtu inou osobou. Ak si myslíte, že iná osoba pozná vaše Heslo, alebo že vaše Heslo bolo použité inou osobou, musíte nás bezodkladne informovať.</p>
<h3>7. Naša zodpovednosť</h3>
<p>Autoogle poskytuje a udržiava Web stránku na &quot;tak ako je&quot; báze a je zodpovedná len za poskytovanie svojich služieb s náležitou schopnosťou a starostlivosťou. Externé stránky neboli nami overené a zhodnotené, teda prístup a používanie externých stránok je vykonávane na vaše vlastné riziko. &quot;Externé stránky&quot; zahŕňajú web stránky tretích strán a on-line služby, ktoré majú na Web stránke odkazy. Autoogle neposkytuje žiadnu záruku s ohľadom k prípadným ponukám alebo iným informáciám obsiahnutých na Web stránke a vylučuje akúkoľvek zodpovednosť za akékoľvek nesprávne alebo nepresné informácie alebo materiály, ktoré Web stránka obsahuje. Autoogle neposkytuje žiadnu inú záruku v súvislosti s Web stránkou a v maximálnom rozsahu povoleným zákonom.</p>
<br />
<p>Autoogle odmieta zodpovednosť za:</p>
<ul>
  <li>
    <p>Konanie alebo opomenutie akéhokoľvek predajcu alebo výrobcu.</p>
  </li>
  <li>
    <p>Straty či poškodenie akéhokoľvek druhu spôsobené akýmkoľvek spôsobom vrátane akejkoľvek priamej, nepriamej, zvláštnej, trestnej či vyplývajúcej straty, či táto strata vyplýva z niečoho, o čom Autoogle bola informovaná.</p>
  </li>
  <li>
    <p>Akékoľvek prerušenia alebo meškania v aktualizácii Web stránky.</p>
  </li>
  <li>
    <p>Porušenie inou osobou akéhokoľvek autorského práva alebo iných práv duševného vlastníctva tretej strany prostredníctvom akéhokoľvek používania Web stránky.</p>
  </li>
  <li>
    <p>Dostupnosť, kvalitu, obsah alebo charakter externých stránok.</p>
  </li>
  <li>
    <p>Akékoľvek transakcie uskutočnené na externých stránkach.</p>
  </li>
  <li>
    <p>Akékoľvek transakcie s maloobchodníkom tretej strany prebiehajúce na Web stránke</p>
  </li>
  <li>
    <p>Akúkoľvek sumu alebo druh straty alebo poškodenia spôsobené vírusmi alebo iným škodlivým softvérom, ktorý môže infikovať počítačové vybavenie užívateľa, softvér, dáta alebo iný majetok spôsobené akoukoľvek inou osobou ktoré pristupuje, používa alebo sťahuje Web stránku.</p>
  </li>
  <li>
    <p>Všetky vyhlásenia, záruky, podmienky a termíny a podmienky, ktoré by mali efekt pre toto oznámenie.</p>
  </li>
</ul>
<br />
<p>Autoogle nezaručuje, že prevádzka Web stránky bude neprerušená alebo bezchybná. Autoogle nenesie zodpovednosť v akejkoľvek čiastke za nesplnenie záväzkov vyplývajúcich z týchto podmienok použitia ak je táto chyba spôsobená vyskytnutím udalostí mimo jej primeranú kontrolu. S výnimkami ako sú uvedené vyššie neexistujú žiadne ďalšie záruky, podmienky alebo iné obchodné termíny a podmienky, výslovené alebo predpokladané, zákonné, alebo iné a všetky tieto termíny a podmienky sú týmto vylúčené v maximálnom rozsahu povolenom zákonom.</p>
<br />
<p>Súhlasíte, že nebudete používať Web stránku akýmkoľvek spôsobom, ktorý je:</p>
<ul>
  <li>
    <p>Nezákonný.</p>
  </li>
  <li>
    <p>Môže viesť k vzniku občianskoprávnej alebo trestnoprávnej zodpovednosti pre Autoogle.</p>
  </li>
  <li>
    <p>By mohol Autoogle vyvolať zlú povesť.</p>
  </li>
</ul>
<br />
<p>Tieto podmienky použitia podliehajú vašim zakonným a všeobecným právam spotrebiteľov a nebudú obmedzovať žiadne práva, ktoré môžete mať a ktoré nemôžu byť vylúčené na základe platných právnych predpisov. Tieto podmienky používania nebudú vylučovať a obmedzovať zodpovednosť Autoogle za smrť alebo poranenie osoby z jej nedbalosti ani žiadne podvodné konanie, vyhlásenia alebo chybné údaje.</p>
<h3>8. Odškodnenie</h3>
<p>Týmto ste nás odškodnili a zaväzujete sa, aby ste nás odškodňovali proti akýmkoľvek stratám, škodám, nákladom, záväzkom a výdavkom (okrem iného vrátane právnych výdavkov a všetky sumy zaplatené nami tretej strane vo vyúčtovaní nároku alebo sporu na radu našich právnych poradcov) vzniknutých alebo utrpených nami plynúce z akéhokoľvek porušenia vami akéhokoľvek ustanovenia týchto podmienok používania alebo vyplývajúce z akéhokoľvek tvrdenia, že ste porušili akékoľvek ustanovenia týchto podmienok používania.</p>
<h3>9. Všeobecné ustanovenia</h3>
<p>Tieto Podmienky používania spoločne s našimi zásadami ochrany súkromia predstavujú celú dohodu medzi vami a Autoogle týkajúce sa vášho používania Web stránky, s vylúčením akýchkoľvek iných termínov používania. Neschopnosť presadiť akékoľvek podmienky nepredstavuje zrieknutie sa týchto podmienok. Ak sa nájde akákoľvek časť týchto podmienok používania byť právne nevýmahatelná bude novelizovaná v minimálnom nevyhnutnom rozsahu potrebnom aby bola právne výmahatelná a ostatok podmienok používania ostane v plnej platnosti a účinnosti. Tieto Podmienky používania, a všetky spory týkajúce sa, alebo vznikajúce v spojitosti s týmito Podmienkami používania, sa budú riadiť a budú riešené v súlade so zákonmi Slovenskej republiky.</p>
<br />
<p>Autoogle bude mať právo postúpiť alebo inak previesť zmluvu ktorá je zahrnutá v týchto Podmienkach používania tým, že vám dá primerané oznámenie, ktoré môže zahŕňať oznámenie podané prostredníctvom Web stránky. Akékoľvek otázky, pripomienky alebo dotazy by mali byť smerované na Autoogle. Autoogle sa bude snažiť odpovedať do 48 hodín.</p>
<br />
<br />
</strong>
</div>
@endsection
