@extends('template')

@section('content')
<div class="container" style="padding-top:25px;">
  <h1>Ochrana osobných údajov</h1>
  <h3>Úvod</h3>
  <p>Nasledujúce Zásady Ochrany Osobných Údajov sa vzťahujú pre užívateľov Autoogle web stránok a popisujú ako používame informácie týkajúce sa vás. Toto sa vzťahuje na každé použitie vašich dát spoločnosťou Autoogle (&quot;my&quot;, &quot;nás&quot;,&quot;nami&quot; alebo &quot;naše&quot;). Ochrana vášho súkromia pri používaní internetových stránok je pre nás mimoriadne dôležitá. Ak máte nejaké výhrady voči našim zásadám o ochrane osobných údajov, prosím kontaktujte Autoogle.</p>
  <h3>1. O teto web stránke a tejto Ochrane osobných údajov</h3>
  <p>Autoogle.eu (ďalej len &quot;Webová stránka&quot;), je online služba vlastnená a prevádzkovaná Autoogle, ktorá umožňuje jednotlivcom jednoducho porovnávať ceny na nové autá.</p>
  <h3>2. Získavanie a spracovanie osobných údajov</h3>
  <p>Osobné údaje môžeme podľa § 11 a 14 zákona č. 122/2013 Z.z. o ochrane osobných údajov a o zmene a doplnení niektorých zákonov neobmedzene získať len vtedy, keď nám ich poskytnete vy sami – napr. v rámci registrácie pri vyplnení dotazníka, pri výhernej súťaži alebo pri požiadavke o zasielaní noviniek – a udelíte nám súhlas k ich spracovaniu. Ak budete pri využívaní ponúkaných služieb požiadaný o zadanie vašich osobných údajov, tak je vylúčené spojenie Vašich osobných údajov s údajmi z protokolu prístupu cez – hoci aj anonymnú - IP adresu s cieľom vytvorenia osobného profilu užívateľa.</p>
  <br />
  <p>Ak sa domnievate, že máme informácie o vás, ktoré nechcete, aby sme mali, alebo ktoré si myslíte, že sú nesprávne, kontaktujte nás prosím. Aj keď sa budeme snažiť najviac ako môžeme ochranovať informácie o vás, nemôžeme zaručiť bezpečnosť dát prenášaných na Webovej stránke. Každý prenos je na vaše vlastné riziko. Potom, čo sme dostali informácie o vás, budeme používať striktné postupy a bezpečnostné prvky, aby sme sa pokúsili zabrániť neoprávnenému prístupu k týmto informáciám. Informácie o vás môžu byť odoslané mimo Európskej Ekonomickej Oblasti, vrátane do krajín, ktorých pravidlá ochrany dát sú menej prísne ako tie, ktoré sú v Európskej Ekonomickej Oblasti.</p>
  <h3>3. Aké údaje zhromažďujeme a ako sa používajú?</h3>
  <br />
  <p>Môžeme vás požiadať o, alebo vy nám môžete podať, osobné aj neosobné informácie a údaje pri použití Webovej stránky. </p>
  <p>To by mohlo zahŕňať:</p>
  <ul>
    <li>
      <p>Vaše meno a adresa</p>
    </li>
    <li>
      <p>Vaše užívateľské meno a heslo</p>
    </li>
    <li>
      <p>Vaša emailová adresa</p>
    </li>
    <li>
      <p>Vaše telefónne číslo</p>
    </li>
    <li>
      <p>Akýkoľvek obsah, ktorý podáte na Webovú stránku (&quot;svoj obsah&quot;), ako sú komentáre, správy predajcom, hodnotenia a marketingové preferencie.</p>
    </li>
  </ul>
  <br />
  <p>Tieto údaje používame na nasledujúce účely:</p>
  <ul>
    <li>
      <p>Meno, adresa, užívateľské meno a heslo: zaregistrovať a spravovať váš užívateľský účet</p>
    </li>
    <li>
      <p>E-mailová adresa: kontaktovať vás ohľadom vášho používania Webovej stránky, s novinkami o nás, ponúknuť vám doplnkové služby.</p>
    </li>
    <li>
      <p>Telefónne číslo: kontaktovať vás ohľadom záležitostí, ktoré vyžadujú vašu naliehavú pozornosť, na overenie vašej identity alebo poskytovať ďalšie služby Autoogle s vaším súhlasom (napríklad oznámenia textovými správami); a</p>
    </li>
    <li>
      <p>Váš Obsah: spracovať a sprístupniť váš obsah na Webovú stránku a ponúkať vám (tovar, služby) v súlade s vašimi pokynmi.</p>
    </li>
  </ul>
  <br />
  <p>Informácie predložené na Webovej stránke môžu byť taktiež použité pre:</p>
  <ul>
    <li>
      <p>Povoliť náš súlad s platnými zákonmi a predpismi</p>
    </li>
    <li>
      <p>Povoliť vaše používanie Webovej stránky a súvisiace služby</p>
    </li>
    <li>
      <p>Poskytnúť vám aktuálne, efektívne a spoľahlivé služby</p>
    </li>
    <li>
      <p>Všeobecne operovať a zlepšiť Webové stránky.</p>
    </li>
  </ul>
  <br />
  <p>Keď sa dostanete do kontaktu s nami, môžeme uchovávať záznamy o tom, čo je komunikované. To má zabezpečiť, že máme záznam nášho rozhovoru pre neskoršie použitie a tak môžeme poskytovať služby pre vás efektívnejšie. Môžeme taktiež zaznamenať tieto komunikácie za účelom trénovania alebo riadenia nášho personálu.</p>
  <p>Môžeme získať citlivé informácie o vás, ale nikdy nebudeme zbierať vaše citlivé informácie bez vášho explicitného súhlasu.</p>
  <h3>4. Cookies a analýza využitia Webovej stránky</h3>
  <p>Naša Webová stránka používa &quot;cookies,&quot; a podobné technológie. Cookies sú malé počítačové súbory, ktoré môže táto Webová stránka uložiť vo vašom internetovom prehliadači, čo umožňuje internetovému prehliadaču a Webovéj stránke komunikovať medzi sebou navzájom. Iné podobné technológie môžu pracovať iným spôsobom, ale rovnako ako cookies, sa používajú na analýzu ako je webová stránka používaná.</p>
  <p>Cookies nám umožňujú zaznamenávať anonymné informácie o vás, vrátane: aký typ Internetového prehliadača používate na prístup k Webovej stránky, operačný systém vašeho počítača, Webové stránky ktoré ste navštívili pred návštevou Webovej stránky, oblasti Webovej stránky ktoré navštívite, ako dlho ste navštívili Webovú stránku a vašu IP adresu (ktorá môže poskytnúť informácie o vašej približnej zemepisnej polohe). Nepoužívame cookies na určenie vašej individuálnej identity.</p>
  <br />
  <p>Cookies používame pre:</p>
  <ul>
    <li>
      <p>Dozvedieť sa o tom, ako sa Webová stránka používa a aké typy jedincov používajú Webovú stránku.</p>
    </li>
    <li>
      <p>Rozoznať vás keď sa vrátite na Webovú stránku.</p>
    </li>
    <li>
      <p>Zaznamenať, ktoré iné webové stránky odkázali užívateľa na Webovú stránku.</p>
    </li>
    <li>
      <p>Optimizovať Webovú stránku a zlepšovať naše služby.</p>
    </li>
    <li>
      <p>Zjednodušiť vypĺňanie formulárov na Webovej stránke.</p>
    </li>
    <li>
      <p>Umožňiť Webovej stránke si zapamätať vaše preferencie.</p>
    </li>
    <li>
      <p>Povoliť úpravu aspektov Webovej stránky.</p>
    </li>
    <li>
      <p>Poskytovať relevantné reklamy.</p>
    </li>
  </ul>
  <br />
  <p>Niektoré cookies budú zmazané keď zavriete váš internetový prehliadač, iné zostanú vo vašom internetovom prehliadači kým nie sú zmazané, alebo uplynie ich platnosť.</p>
  <p>My budeme uchovávať informácie získané od vášho používania cookies po dobu až jedného roka. Tieto informácie môžu byť uložené spoločnosťou Google ako súčasť nášho používania Google Analytics. Ďalšie informácie o súboroch cookies nájdete na https://sk.wikipedia.org/wiki/HTTP_cookie. Ak si neželáte, aby sme používali cookies keď používate Webovú stránku, prosím upravte si nastavenie internetového prehliadača, aby cookies odmietal (súbor pomocníka vášho počítača by vám mal oznámiť, ako to urobiť). Ak cookies zakážete, niektoré časti Webovej stránky nemusia správne fungovať. Používaním Webovej stránky súhlasíte s našimi používaním cookies, ako bolo popísané vyššie.</p>
  <h3>5. Zdieľanie Dát</h3>
  <p>Môžeme zverejniť osobné informácie o vás ostatným členom našej skupiny spoločností alebo dôveryhodným tretím stranám (vo vnútri a mimo Európskej Ekonomickej Oblasti):</p>
  <ul>
    <li>
      <p>Na účely uvedené vyššie; a</p>
    </li>
    <li>
      <p>Aby sme mohli prijímať služby, ktoré nám pomáhajú prevádzkovať Webovú stránku.</p>
    </li>
  </ul>
  <br />
  <p>Vaše osobné údaje môžu byť tiež zverejnené:</p>
  <ul>
    <li>
      <p>Ak by sme kúpili alebo predali, alebo sa snažili kúpiť alebo predať, akékoľvek firmy alebo majetok</p>
    </li>
    <li>
      <p>S ohľadom na prevenciu podvodov alebo zníženie úverového rizika; a</p>
    </li>
    <li>
      <p>Ak sme povinní tak urobiť v súlade s právom, predpismi alebo súdnymi príkazmi; ak je to potrebné na vymáhanie niektorej z našich podmienok alebo akýchkoľvek iných dohôd s ktorými sme zmluvnou stranou; ak je to potrebné na ochranu našich práv, majetku alebo bezpečnosť našich zákazníkov alebo používateľov Webovej stránky.</p>
    </li>
  </ul>
  <h3>6. Marketing</h3>
  <p>Dáme vám na výber, či chcete aby sme vás my alebo iná spoločnosť kontaktovali ohľadom ponúk, udalostí, produktov a služieb, ktoré by vás mohli zaujímať.</p>
  <p>Môžeme vám posielať e-maily o uvádzaní na trh výrobkov alebo služieb, ktoré sú podobné alebo súvisiace s Webovou stránkou, alebo vás kontaktovať prostredníctvom e-mailu alebo textu s informáciami o Webovej stránke. Nebudeme vám posielať žiadne ďalšie marketingové e-maily alebo odovzdávať vaše kontaktné informácie tretím stranám pokiaľ nám nedáte súhlas, alebo ste nám ho už nedali.</p>
  <br />
  <p>Ak poskytnete informácie o vašom priateľovi v rámci postúpenia tohto priateľa k nám, pošleme tomuto priateľovi jeden e-mail o tejto Webowej stránke a uložíme ich osobné údaje v minimálnom nevyhnutnom rozsahu. Keď nám poskytnete informácie na svojho priateľa, potvrdzujete, že máte ich súhlas s týmto využívaním ich informácií. Oznámime vašemu priateľovi, že vy ste poskytli ich údaje keď sa s nimi spojíme.</p>
  <h3>7. Uchovávanie údajov</h3>
  <p>Budeme držať údaje o vás len tak dlho, ako je to nutné, s ohľadom na účel, pre ktorý boli údaje zozbierané.</p>
  <h3>8. Všeobecné ustanovenia</h3>
  <p>Ak budete nasledovať odkaz z Webovej stránky na akékoľvek webové stránky tretích strán, mali by ste si byť vedomí toho, že tieto webové stránky môžu mať svoje vlastné zásady ochrany osobných údajov. Neprijímame žiadnu zodpovednosť za tieto webové stránky. Prosím skontrolujte pravidlá a politiku akýchkoľvek web stránok tretích strán predtým než im poskytnete akékoľvek osobné údaje.</p>
  <br />
  <p>Môžeme vykonať zmeny týchto zásad ochrany súkromia v budúcnosti, ktoré budú zverejnené na tejto stránke. Mali by ste skontrolovať túto stránku z času na čas, aby ste si boli vedomí prípadných zmien. V prípade potreby vám môžme oznámiť zmeny e-mailom, alebo tým, že zverejníme oznámenie na Webovej stránke. Akékoľvek otázky, pripomienky alebo otázky by mali byť smerované k nám. Budeme sa snažiť reagovať do 48 hodín.</p>
  <br />

</div>
@endsection
