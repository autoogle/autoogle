@extends('template')

@section('content')
<div class="container" style="padding-top:25px;">
	<div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        V tejto časti nájdete otázky a odpovede, ako a prečo <strong>Autoogle</strong> funguje. Ak neviete nájsť odpoveď na vašu otázku, určite sa na nás obráďte, radi vám odpovieme.
    </div>

	<div class="panel-group" id="accordion">
        <!--<div class="faqHeader">General questions</div>-->

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Čo je Autoogle?</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p><strong>Autoogle</strong> je stránka pre porovnávanie nových aút. Je to jednoduchšia cesta ako dostať najlepšiu cenu na vaše nové auto.
					Nakonfigurujte si auto podľa vášho výberu a následne dostanete konkrétne cenové ponuky v
					priebehu 24 hodín.
					Dealery vám ponúknu ich najlepšiu cenu, exkluzívnu pre Autoogle zákazníkov.
					Vaše údaje nikdy nebudú poskytnuté dealerom, takže vás nikto nebude otravovať nechcenými
					telefonátmi.</p>

					<p>Keď dostanete ponuky, môžete si ich porovnať podľa ceny, vzdialenosti od vášho bydliska a
					recenzií na predajcu od iných kupujúcich.
					Ak sa vám nejaká ponuka páči, môžte kontaktovať vybraného dealera anonymne pomocou
					správ cez Autoogle, alebo priamo telefonicky.
					A najlepšie na tom je, že služby na našej stránke sú pre vás úplne zdarma, či už auto kúpite
					alebo nie.</p>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">Aky je rozdiel medzi použitím Autoogle a vašim miestnym dealerom?</a>
                </h4>
            </div>
            <div id="collapseTen" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Kúpa nového auta by mala byť príjemná a vzrušujúca skúsenosť, ale môže sa to zmeniť aj na
					otravnú a nepríjemnú záležitosť.
					My v Autoogle vieme, že mnohí kupujúci sú znechutení stratou času obchádzaním dealerov,
					zjednávaním lepšej ceny a nevedia, či vôbec dostali výhodnú ponuku.
					Keď pôjdete k miestnemu dealerovi, budete jednať s predajcom, ktorý je platený podľa toho,
					koľko zisku dostane od vás pri predaji auta.
					A práve tu je výhodné použiť Autoogle. My pracujeme priamo s manažérmi predaja, ktorí nám
					dajú vačšiu zľavu a my im na oplátku dáme viacej spokojných zákazníkov, čím sa zvýši ich
					predaj.</p>

					<p>
					My sme zaviazaný vybaviť čo najlepšiu cenu pre našich užívateľov a zároveň poskytovať
					jednoduchú a príjemnú skúsenosť pri kúpe nového auta.</p>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">Prečo sa musím registrovať, keď chcem vidieť ponuky od dealerov?</a>
                </h4>
            </div>
            <div id="collapseEleven" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Keď dostanete ponuku od dealera, bude vám zaslaný email. Keď sa zaregistrujete, môžete
					porovnávať a reagovať na ponuky cez Autoogle, komunikovať s dealermi zasielaním a
					prijímaním správ v anonymite. Tiež vás budeme informovať pri zmenách cien aút, o ktoré sa
					zaujímate.</p>
					<p>Dealeri vždy verejne neuverejňujú nadchádzajúce akcie, ale sú ochotní ponúknuť tieto akcie pre
					zaregistrovaného Autoogle zákazníka.</p>
                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a4">Existujú nejake skryté poplatky?</a>
                </h4>
            </div>
            <div id="a4" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Nikdy. Cena,  ktorú dostanete v ponuke,  je suma  ktorú zaplatíte a my vám nebudeme nič
					účtovať, či už auto kúpite, alebo nie.
					Každá ponuka zahrňuje DPH a záručné opravy.
					Všetky ponuky, ktoré dostanete cez Autoogle, sú na nové autá, ktoré budú objednané priamo u
					výrobcu, na základe vašej konfigurácie. Cena zahrňuje nové nezaregistrované auto vrátane DPH, s plnou výrobnou zárukou.
					Nie je rozdiel, či auto kúpite od vášho miestneho dealera, alebo vzdialeného dealera. Môžete
					rovnako využit ponuku na financovanie od výrobcu. Auto môžete servisovať v hociktorom
					autorizovanom servise.</p>
					<p>Nexistujú žiadne skryté poplatky a Autoogle vám nikdy nebude nič účtovať, aj keď auto kúpite.
					Jednoducho akú ponuku dostanete, toľko zaplatíte.</p>

                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a5">Je Autoogle predajca automobilov?</a>
                </h4>
            </div>
            <div id="a5" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Nie, to nie sme. Všetko čo robíme je, že vás prepojíme s dealerom, ale zároveň chránime vašu
					totožnosť. Za naše služby neplatíte a pokiaľ príjmete ponuku a kúpite auto, celá transakcia
					prebieha výhradne len medzi vami a dealerom. Teda auto kupujete priamo cez autorizovaného
					dealera.</p>

                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a6">Kto sú dealeri na Autoogle?</a>
                </h4>
            </div>
            <div id="a6" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Postupne budujeme veľkú sieť dôveryhodných predajcov po celom Slovensku. Všetci naši
					predajcovia sú importérom autorizovaný dealeri s predvádzacími priestormi.
					Pracujeme s konkrétnymi predajcami, ktorí sú v manažérskych pozíciách a preto vedia
					ponúknuť exkluzívne ceny s výraznými zľavami pre našich užívateľov. Rovnakú ponuku by ste
					nedostali od normálneho predajcu na predajni.</p>
					<p>My pracujeme len s takými predajcami, čo vedia pravideľne ponúknuť výrazné zľavy a kvalitný
					zakaznícky servis. Máme na to svoje metódy a vieme si overiť, či tomu tak naozaj je. To čo je
					dôležité vedieť  je, že pracujeme len s kvalitnými dealermi.</p>
					<p>Každého zakazníka žiadame, aby zanechal hodnotenie po kúpe auta, takže si môžete prečítať
					hodnotenia na dealerov od predchádzajúcich zákazníkov.</p>
                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a7">Prečo nie je môj miestny auto dealer na Autoogle?</a>
                </h4>
            </div>
            <div id="a7" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Pravdepodobne kvôli tomu, že nevedia poskytnúť takú úroveň zliav a služieb, akú od nich my
					očakávame. My chceme pracovať len s najlepšími dealermi, takými ktorí reagujú na požiadavky
					našich užívateľov a vedia ponúknuť výhodné ceny. Ak viete o niekom, kto by chcel s nami
					spolupracovať a zvýšiť svoj predaj, dajte im o nás vedieť!</p>
					<p>Podľa čoho sa rozhoduje od ktorých predajcov dostanem ponuku?
					My vždy zabezpečíme, aby ste dostali najlepšiu cenovú ponuku zo siete našich dealerov, bez
					ohľadu na vašu lokalitu. Jedna z ponúk bude tiež od vášho najbližšieho dealera.</p>
                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a75">Ako viem, že si dealer bude stáť za Autoogle ponukou?</a>
                </h4>
            </div>
            <div id="a75" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Je to veľmi zriedkavé, že dealer zmení danú ponuku. Môže sa to stať, ak sa výrobca náhle
						rozhodne zmeniť ceny aút. Cenníky sa približne menia každého štvrťroka a my sa vás o tom
						snažíme informovať pred tým ako zmena nastane!</p>
                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a8">Je dobré zvážiť aj kúpu auta od vzdialeného dealera?</a>
                </h4>
            </div>
            <div id="a8" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Kúpiť nové auto od predajcu, ktorý je ďalej od vášho bydliska, je rovnako bezpečné, ako kúpiť
					od miestneho predajcu. Váš záručný servis je platný všade a vy si môžete dávať auto servisovať v hociktorom autorizovanom servise v krajine. Lojálnosť vám neprinesie žiadne
					výhody.</p>
                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a9">Dostanem rovnaké podmienky od vášho dealera?</a>
                </h4>
            </div>
            <div id="a9" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Áno. Kedže auto kúpite priamo cez autorizovaného dealera, vaše nové auto bude mať plnú
					záruku. Vaše auto si môžete servisovať v hociktorom z autorizovaných dealerov na Slovensku,
					radi vás uvítajú, keďže dealeri najlepšie zarábajú na samotnom servise áut. Ak si zakúpite jeden
					zo servisných balíkov, ten si uplatníte tiež.</p>
                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a10">Kde si môžem dať servisovať moje auto?</a>
                </h4>
            </div>
            <div id="a10" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Vaše auto si možete dať servisovať v hociktorom autorizovanom servise.
					To znamená, že môžete použiť služby vášho miestneho autorizovaného dealera pre servis a aj
					záručné opravy. A kedže dealer na servise zarobí ešte viac, ako na samotnom predaji, určite
					vás privítajú.</p>
					<p>
					Nemusíte si robiť starosti, že vám neposkytnú také služby, ako keby ste auto kúpili priamo od
					nich. Ak ste si zakúpili aj servisný balíček, ten si môžete uplatniť tiež.
					</p>
                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a11">Môžem dostať rovnakú ponuku od miestneho dealera rovnakej značky?</a>
                </h4>
            </div>
            <div id="a11" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Ponuky na Autoogle dávajú väčšinou manažéri predaja, z konkrétnych predajní.
					To znamená, že je dosť pravdepodobné, že nedostanete ani zďaleka takú výhodnú ponuku.
					Dostal som skvelú ponuku na auto, ktoré chcem. Čo ďalej?
					Gratulujeme! Jednoducho si to auto zajednajte cez Autoogle dealera, ktorý vám tú ponuku
					zaslal. Buď ich priamo navštívte v ich predajni, alebo im zavolajte a dohodnite sa na dalších
					krokoch.</p>
					<p>Keď potvrdíte vašu objednávku, zložíte u dealera zálohu (výška záleží od ceny auta), čím si
					zabezpečíte vaše nové auto.</p>
					<p>Dealer vám pošle potvrdenie o vašej objednávke, a bude vás informavať,  kedy bude vaše auto
					pripravené na vyzdvihnutie.</p>
					<p>Zvyšok peňazí zaplatíte priamo dealerovi bankovým prevodom,  alebo inou vopred dohodnutou
					formou, pred vyzdvihnutím vášho nového auta. Ak sa rozhodnete pre financovanie na splátky,
					tak všetky dokumenty podpíšete tiež podľa dohody s dealerom.
					Potom už len zostáva si sadnúť a čakať na príchod vašeho nového nablískaného auta.
					</p>
                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a12">Prečo sú niektoré ponuky lepšie ako druhé?</a>
                </h4>
            </div>
            <div id="a12" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Zľavy,  ktoré dealeri ponúkajú, záležia väčšinou na tom, koľko áut chcú predať.
						Niektorí dealeri sa zamerajú na konkrétny model, na ktorý potom dávajú lepšie zľavy.
						Niektoré verzie modelov majú špeciálne edície, ktoré sú už dosť zlacnené výrobcom, takže
						dealeri už nemajú veľa priestoru na zľavu. A podobne, ak sa práve nejaký nový model dostal na
						trh, dealeri hneď nemusia poskytnúť výrazné zľavy.
					</p>
                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a13">Prečo sú moje ponuky podobné?</a>
                </h4>
            </div>
            <div id="a13" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Ponuky od dealerov môžu niekedy byť podobné, keďže dealeri, s ktorými spolupracujeme súhlasia s istými zľavami z predajnej ceny. Na základe toho vám jednoducho poskytujú
najlepšiu zľavu akú môžu.
					</p>
                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a14">Ako môžem získať kôtu na financovanie nového auta?</a>
                </h4>
            </div>
            <div id="a14" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Na zistenie kôty pre financovanie, jednoducho zavolajte, alebo pošlite odkaz dealerovi a uveďte
					o aký typ financovania máte zaujem. Na začiatku uveďte nasledujúce detaily - výška depozitu,
					koľko najzdíte ročne kilometrov a počet rokov, koľko chcete splácať.
					Na niektoré modely sa môžu vzťahovať ďalšie zľavy, v prípade ak využijete niektorú formu
					financovania.
					</p>
                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a15">Môj miestny dealer ponúka nízky úrok pri financovaní, alebo ročné poistenie zdarma.
Ponúkajú tieto výhody aj dealeri na Autoogle?</a>
                </h4>
            </div>
            <div id="a15" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Áno! Tieto ponuky sú vo všeobecnosti robené priamo výrobcom, nie miestnym dealerom, takže
					sú prístupné u každého dealera.</p>
					<p>
					Dealeri na Autoogle vám dajú tieto ponuky tiež a navyše vám ponúknu výraznú zľavu, takže sa
					ich určite oplatí kontaktovať.
					</p>
                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a16">Ako zistím, ktoré autá sú skladom na odobranie ihneď?</a>
                </h4>
            </div>
            <div id="a16" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Ak chcete kúpiť auto rýchlo, môžete kontaktovať našich dealerov, aby ste zistili aké autá majú
						skladom a či majú také auto, aké hľadáte.
						Skladové autá sa menia rýchlo, takže sa oplatí dealerov kontaktovať, aby ste vedeli, či majú
						auto aké chcete a akú zľavu vám vedia poskytnúť.<p>

						<p>Ako dostanem ponuku na iné auto?</p>

						<p>Jednoducho sa prihláste do Autoogle a na hlavnej stránke stlačte Vybrať Auto, čo vás dostane
						na auto configurátor. Alebo stačte Moje Ponuky na hornom menu stránky a vyberte +Nové.
						Môžete nakonfigurovať rovnaký model s iným motorom a výbavou, alebo si poskladať úplne iné
						auto. Prosíme vás, získajte ponuky na toľko áut, koľko potrebujete, aby ste sa vedeli dobre
						rozhodnúť.</p>

                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a17">Prečo nemáte v ponuke všetkých výrobcov automobilov?</a>
                </h4>
            </div>
            <div id="a17" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>My stále rozvíjame naše kontakty, aby sme pokryli všetkých importérov v krajine. Noví dealeri
sú pridávaný kontinuálne, kedže my neustále hľadáme tých najlepších dealerov pre spoluprácu.</p>

                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a18">Berú vaši dealeri staré auto ako protizálohu?</a>
                </h4>
            </div>
            <div id="a18" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Áno, vačšina našich dealerov prijímajú staré auto, ako pritihodnotu za nové. Kontaktujte vášho vybraného dealera, aby ste zistili, čo pre vás môže urobiť.
Nikedy je však lepšie predať auto iným spôsobom. </p>

                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a19">Ako dlho sú ponuky od dealerov platné?</a>
                </h4>
            </div>
            <div id="a19" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Dealeri si budú stáť za ponukou, ktorú vám dali, pokiaľ importér nezmenil cenník. To sa stáva
					približne každého štvrťroka.</p>
					<p>
					Ak ste sa vrátili na stránku po dlhšom čase, bude lepšie keď si prekonfigurujete vaše auto, aby
					ste zistili, či vám dealeri neponúknu lepšie zľavy. Po 4 mesiacoch budú vaše ponuky
					automaticky archivované a vy si budete musieť auto prekonfigurovať, aby ste dostali nové
					ponuky.</p>

                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a20">Čo keď som poslal odkaz dealerovi a nedostal som odpoveď?</a>
                </h4>
            </div>
            <div id="a20" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>To nás veľmi mrzí! My vieme, že dealeri majú veľa práce, ale požadujeme od nich aby
					dodržiavali určitý štandart. Ak dealer neodpovie do 48 hodín od vášho kontaktu, dajte nám
					vedieť, aby sme mohli zistiť dôvod a trocha ho pošťuchli.</p>
					<p>
					Chceme spolupracovať len s najlepšími dealermi, takže si veľmi cenníme, že nám dáte o tom
					vedieť. Zatelefonovať dealerovi priamo, môže byť efektívnejšie, ako poslanie odkazu, takže to
					môžete skúsiť tiež.</p>
                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a21">Ako Autoogle zarába peniaze?</a>
                </h4>
            </div>
            <div id="a21" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Účtujeme dealerom na základe množstva predaja aút, ktoré sme pre nich sprostretkovali. Vám nikdy neúčtuje nič, či už auto kúpite ,alebo nie!</p>
                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a22">Je v tom nejaký háčik?</a>
                </h4>
            </div>
            <div id="a22" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Nie, nie je v tom žiadny háčik? Radi pomáhame ľuďom kupovať nové autá bez zvyčajných
					naťahovačiek.
					Dealeri s ktorými spolupracujeme, vedia ponúknuť výhodné zľavy a my im na oplátku dodávame
					kvalitných zákazníkov. Cena ktorú vidíte, je cena ktorú zaplatíte.</p>
                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a23">Kto stojí za Autoogle?</a>
                </h4>
            </div>
            <div id="a23" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Sme malí, ale rastúci team zanietencov s vášňou pre vytvorenie najlepšej možnej cesty pre kúpu nového auta.</p>
                </div>
            </div>
        </div>

		<div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#a24">Ako vás môžem kontaktovať?</a>
                </h4>
            </div>
            <div id="a24" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Jednoducho! Pošlite nám email na <a href="mailto:info@autoogle.eu">info@autoogle.eu</a> a my vám radi odpovieme.</p>
                </div>
            </div>
        </div>
	</div>
</div>
@endsection

@section('script')
@endsection
