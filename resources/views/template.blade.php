<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="{{ asset('img/icon.png') }}">

    <title>{{ Lang::get('messages.home_page_title') }}</title>
    <link href="{{ URL::to('css/ac.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,100&subset=latin,latin-ext" rel="stylesheet" type="text/css">
  </head>

  <body>

    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">{{ Lang::get('messages.toggle_navigation') }}</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ URL::to('/') }}"><img src="{{ asset('img/logo_small.png') }}" alt="Autoogle" /></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="{{ URL::to( Lang::get('routing.select-make')) }}">{{ Lang::get('messages.wizard_btn_select_make') }}</a></li>
            <li><a href="#about">{{ Lang::get('messages.best_offers') }}</a></li>
            <!--<li><a href="#contact"></a></li>-->
          </ul>

          <ul class="nav navbar-nav navbar-right">
            @if(isset($requests) && Auth::check())
              @include('elements.user_menu')
            @else
              <li><a href="{{ URL::to('admin') }}">{{ Lang::get('messages.login') }}</a></li>
            @endif
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="wrap">
    @yield('content')
    </div>

    <div class="footer-wrap">
      @include('elements.footer')
    </div>

    <div class="foot-note-wrap">
    	<div class="container">
        	<div class="row">
        		<div class="col-md-4">
              <span class="txt">Follow us on</span> <span class="bdg fb"><i class="glyphicon glyphicon-heart"></i></span> <span class="bdg tw"><i class="glyphicon glyphicon-heart"></i></span>
            </div>
        		<div class="col-md-4 text-center">
              <span class="txt">{{ Lang::get('messages.copyright') }}</span>
            </div>
        		<div class="col-md-4">
                <span class="txt"><a href="{{ URL::to(Lang::get('routing.tnc')) }}">{{ Lang::get('messages.t_and_c') }}</a> <a href="{{ URL::to(Lang::get('routing.privacy')) }}">{{ Lang::get('messages.privacy') }}</a></span>
            </div>
        	</div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    @yield('script')
  </body>
</html>
