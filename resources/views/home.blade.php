@extends('template')

@section('content')
@include('elements.image-hero')

<!-- 4 Box Section -->
<div class="callout-wrap">
	<div class="container">
	  	<div class="row">
	        <div class="col-sm-6 col-md-3 callout">
	          <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
	          <h2>{{ Lang::get('messages.hme_callout_title_1') }}</h2>
	          <p>{{ Lang::get('messages.hme_callout_text_1') }}</p>
	        </div>
	      	<div class="col-sm-6 col-md-3 callout">
	        	<span class="glyphicon glyphicon-piggy-bank" aria-hidden="true"></span>
	          	<h2>{{ Lang::get('messages.hme_callout_title_2') }}</h2>
	          	<p>{{ Lang::get('messages.hme_callout_text_2') }}</p>
	        </div>
	      	<div class=" col-sm-6 col-md-3 callout">
				<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
	          	<h2>{{ Lang::get('messages.hme_callout_title_3') }}</h2>
	          	<p>{{ Lang::get('messages.hme_callout_text_3') }}</p>
	        </div>
	      	<div class=" col-sm-6 col-md-3 callout">
				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
	          	<h2>{{ Lang::get('messages.hme_callout_title_4') }}</h2>
	          <p>{{ Lang::get('messages.hme_callout_text_4') }}</p>
	        </div>
	    </div>
	</div>
</div>
<!-- ./4 Box Section -->

<!-- Steps Section -->
<div class="container section works" align="center">
	<h2 id="works">{{ Lang::get('messages.hme_btn_how_it_works') }}</h2>
	<br/>
	<div class="row">
		<div class="col-md-4">
			<i class="glyphicon glyphicon-wrench"></i>
			<h3>{{ Lang::get('messages.hme_hiw_title_1') }}</h3>
			<p>{{ Lang::get('messages.hme_hiw_desc_1') }}</p>
		</div>
		<div class="col-md-4">
			<i class="glyphicon glyphicon-th-list"></i>
			<h3>{{ Lang::get('messages.hme_hiw_title_2') }}</h3>
			<p>{{ Lang::get('messages.hme_hiw_desc_2') }}</p>
		</div>
		<div class="col-md-4">
			<i class="glyphicon glyphicon-road"></i>
			<h3>{{ Lang::get('messages.hme_hiw_title_3') }}</h3>
			<p>{{ Lang::get('messages.hme_hiw_desc_3') }}</p>
		</div>
	</div>
	<div style="margin-bottom:100px;"></div>
</div>
<!-- ./Steps Section -->

<!-- Text / Image Section -->
<div class="section section-image" style="">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h2>{{ Lang::get('messages.hme_section_2_title') }}</h2>
					@if(Lang::getLocale() == 'sk')
						@include('elements.hme_section_2_text_sk')
					@else
						@include('elements.hme_section_2_text_en')
					@endif
			</div>
			<div class="col-md-4">
				<!--<img src="http://placehold.it/350x350" class="img-responsive" alt="" />-->
			</div>
		</div>
	</div>
</div>
<!-- ./Text / Image Section -->



<!--
<div class="container section">
	<div class="row">
		<div class="col-md-12" align="center">
        	<h2 id="works">{{ Lang::get('messages.hme_works_title_part_how') }} <span class="highlight">Autoogle</span> {{ Lang::get('messages.hme_works_title_part_works') }}</h2>
        </div>
	</div>
    <div class="row">
    		<div class="col-md-8 col-md-offset-2">
            	<div class="embed-responsive embed-responsive-16by9">
	            	<iframe width="100%" height="100%" class="" src="https://www.youtube.com/embed/oojus0b68Gw" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
    </div>
</div>



<div class="reviews">
	<div class="container section">
    	<div class="row">
    		<div class="col-md-12" align="center">
            	<h2>{{ Lang::get('messages.hme_what_customers') }}</h2>
            </div>
    	</div>
		<div class="row">
			<div class="col-md-6">
	        	<div class="cloud">
                	
                	<div class="inner">
                    	<h4>Sam Smith</h4>
                        <p>Lorem ipsum dolor sit amet, munere propriae vis te. Posse probatus et usu. Et animal graecis copiosae eum, maiorum repudiandae nec ea. Ex diam quas mea, ex nam eruditi rationibus.</p>
                        <span class="fa fa-car"></span>
                    </div>
                </div>
                <div class="cloud">
                	
                	<div class="inner">
                    	<h4>Sam Smith</h4>
                        <p>Lorem ipsum dolor sit amet, munere propriae vis te. Posse probatus et usu. Et animal graecis copiosae eum, maiorum repudiandae nec ea. Ex diam quas mea, ex nam eruditi rationibus.</p>
                        <span class="fa fa-car"></span>
                    </div>
                </div>
	        </div>
			<div class="col-md-6">
	        	<div class="cloud">
                	
                	<div class="inner">
                    	<h4>Sam Smith</h4>
                        <p>Lorem ipsum dolor sit amet, munere propriae vis te. Posse probatus et usu. Et animal graecis copiosae eum, maiorum repudiandae nec ea. Ex diam quas mea, ex nam eruditi rationibus.</p>
                        <span class="fa fa-car"></span>
                    </div>
                </div>
                <div class="cloud">
                	
                	<div class="inner">
                    	<h4>Sam Smith</h4>
                        <p>Lorem ipsum dolor sit amet, munere propriae vis te. Posse probatus et usu. Et animal graecis copiosae eum, maiorum repudiandae nec ea. Ex diam quas mea, ex nam eruditi rationibus.</p>
                        <span class="fa fa-car"></span>
                    </div>
                </div>
	        </div>
		</div>
	</div>
</div>-->
@endsection

@section('script')
<script>
	$(function() {
		$('#wBtn').click(function(e) {
			e.preventDefault();
			 $('html, body').animate({
		        scrollTop: $('#works').offset().top-75
		    }, 2000);
		});
	});
</script>
@endsection
