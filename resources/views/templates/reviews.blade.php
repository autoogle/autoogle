@extends('template')

@section('title')
{{ $title }}
@endsection

@section('content')
{!! $components !!}
@endsection

@section('script')
<script type="text/javascript">
;(function($, win) {
  $.fn.inViewport = function(cb) {
     return this.each(function(i,el) {
       function visPx(){
         var elH = $(el).outerHeight(),
             H = $(win).height(),
             r = el.getBoundingClientRect(), t=r.top, b=r.bottom;
         return cb.call(el, Math.max(0, t>0? Math.min(elH, H-t) : (b<H?b:H)));  
       }
       visPx();
       $(win).on("resize scroll", visPx);
     });
  };
}(jQuery, window));

$(function() {
	$(".view-watch").inViewport(function(px) {
	  if(px){
		$(this).addClass('inview');
	  }else{
		$(this).removeClass('inview');
	  }
	});
});
</script>
@endsection
