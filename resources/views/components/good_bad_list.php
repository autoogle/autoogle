<div class="dd-wrap {style}">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        {.section good}
        <ul class="good">
          {.repeated section @}
            <li>{name}</li>
          {.alternates with}
          {.end}
        </ul>
        {.or}
          <p>Bugger all</p>
        {.end}
      </div>
      <div class="col-md-4">
        {.section bad}
        <ul  class="bad">
          {.repeated section @}
            <li>{name}</li>
          {.alternates with}
          {.end}
        </ul>
        {.or}
          <p>Bugger all</p>
        {.end}
      </div>
    </div>
  </div>
</div>
