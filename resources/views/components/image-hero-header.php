<div class="dd-image-hero-header" style="background-image:url({img});">
	<div class="caption caption-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>{title}</h1>
					<p>{subtitle}</p>
				</div>
			</div>
		</div>
	</div>
</div>