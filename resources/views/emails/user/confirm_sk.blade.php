@extends('emails.email')

@section('content')
<div class="block">
   <!-- Full + text -->
   <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="fullimage">
      <tbody>
         <tr>
            <td>
               <table bgcolor="#ffffff" width="580" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" modulebg="edit">
                  <tbody>
                     <tr>
                        <td width="100%" height="20"></td>
                     </tr>
                     <tr>
                        <td>
                           <table width="540" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner">
                              <tbody>
                                 <!-- title -->
                                 <tr>
                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #333333; text-align:left;line-height: 20px; text-transform:uppercase;" st-title="rightimage-title">
                                       Potvrďte prosím Váš Email
                                    </td>
                                 </tr>
                                 <!-- end of title -->
                                 <!-- Spacing -->
                                 <tr>
                                    <td width="100%" height="10"></td>
                                 </tr>
                                 <!-- Spacing -->
                                 <!-- content -->
                                 <tr>
                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #666666; text-align:left;line-height: 24px;" st-content="rightimage-paragraph">
                                      <p>Vitajte na Autoogle,</p>
                                      <p>My v Autoogle veríme, že kúpa nového auta by mala byť príjemná záležitosť. Takže sa vám snažíme dať čo najlepšiu skúsenosť, bez straty vášho času a neistoty pri získavaní lepšej cenovej ponuky.</p>
                                      <p><strong>Ako to všetko robíme?</strong></p>
                                      <ol>
                                        <li>My si vyberáme dealerov, ktorí sú ochotní dať dobrú ponuku a majú dobré zákaznícke služby</li>
                                        <li>Môžete si porovnať ceny dopredu, ktoré sú bez ďaľších skrytých poplatkov</li>
                                        <li>Keď sa rozhodnete prijať niektorú ponuku, jednoducho priamo kontaktujete dealera a auto dostanete za tú cenu, ktorá vám bola ponúknutá.</li>
                                      </ol>

                                    </td>
                                 </tr>
                                 <!-- end of content -->
                                 <!-- Spacing -->
                                 <tr>
                                    <td width="100%" height="10"></td>
                                 </tr>
                                 <!-- button -->
                                 <tr>
                                    <td>
                                       <table height="30" align="left" valign="middle" border="0" cellpadding="0" cellspacing="0" class="tablet-button" st-button="edit">
                                          <tbody>
                                             <tr>
                                                <td width="auto" align="center" valign="middle" height="30" style=" background-color:#98d06e; border-top-left-radius:4px; border-bottom-left-radius:4px;border-top-right-radius:4px; border-bottom-right-radius:4px; background-clip: padding-box;font-size:13px; font-family:Helvetica, arial, sans-serif; text-align:center;  color:#ffffff; font-weight: 300; padding-left:18px; padding-right:18px;">

                                                   <span style="color: #ffffff; font-weight: 300;">
                                                      <a style="color: #ffffff; text-align:center;text-decoration: none;" href="{{ URL::to(Lang::get('routing.user').'/'.$user->passcode.'/'.Lang::get('routing.user-confirm')) }}">Overte svoj účet</a>
                                                   </span>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </td>
                                 </tr>
                                 <!-- /button -->
                                 <!-- Spacing -->
                                 <tr>
                                    <td width="100%" height="20"></td>
                                 </tr>
                                 <!-- Spacing -->
                                 <tr>
                                   <td style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #666666; text-align:left;line-height: 24px;" st-content="rightimage-paragraph">
                                    <p>Ak máte nejaké otázky, tak sa na nás obráďte na poradna@autoogle.eu.</p>
                                    <p>Prajeme šťastnú kúpu auta.</p>
                                    <p style="font-style:italic">Spoločnosť Autoogle</p>
                                   </td>
                                 </tr>
                                 <!-- Spacing -->
                                 <tr>
                                    <td width="100%" height="20"></td>
                                 </tr>
                                 <!-- Spacing -->
                              </tbody>
                           </table>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </td>
         </tr>
      </tbody>
   </table>
</div>
@endsection
