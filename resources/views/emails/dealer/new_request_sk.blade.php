@extends('emails.email')

@section('content')
<div class="block">
   <!-- Full + text -->
   <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="fullimage">
      <tbody>
         <tr>
            <td>
               <table bgcolor="#ffffff" width="580" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" modulebg="edit">
                  <tbody>
                     <tr>
                        <td width="100%" height="20"></td>
                     </tr>
                     <tr>
                        <td>
                           <table width="540" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner">
                              <tbody>
                                 <!-- title -->
                                 <tr>
                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #333333; text-align:left;line-height: 20px;text-transform:uppercase;" st-title="rightimage-title">
                                       nová žiadosť
                                    </td>
                                 </tr>
                                 <!-- end of title -->
                                 <!-- Spacing -->
                                 <tr>
                                    <td width="100%" height="10"></td>
                                 </tr>
                                 <!-- Spacing -->
                                 <!-- content -->
                                 <tr>
                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #666666; text-align:left;line-height: 24px;" st-content="rightimage-paragraph">
                                       <p>Zdravíme vás, {{ $user->name }}</p>

                                        <p>Náš užívateľ vám poslal novú žiadosť o cenovú ponuku na nové auto. Kliknite na tlačítko, aby ste videli, o aký model a výbavu sa jedná. Zadajte najlepšiu cenovú ponuku akú momentálne môžete dať. Pamätajte, že druhý dealeri vás môžu kedykoľvek prebiť lepšou cenou. Môžete ponúknuť aj výbavu navyše.</p>
                                    </td>
                                 </tr>
                                 <!-- end of content -->
                                 <!-- Spacing -->
                                 <tr>
                                    <td width="100%" height="10"></td>
                                 </tr>
                                 <!-- button -->
                                 <tr>
                                    <td>
                                       <table height="30" align="left" valign="middle" border="0" cellpadding="0" cellspacing="0" class="tablet-button" st-button="edit">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td width="auto" align="center" valign="middle" height="30" style=" background-color:#98d06e; border-top-left-radius:4px; border-bottom-left-radius:4px;border-top-right-radius:4px; border-bottom-right-radius:4px; background-clip: padding-box;font-size:13px; font-family:Helvetica, arial, sans-serif; text-align:center;  color:#ffffff; font-weight: 300; padding-left:18px; padding-right:18px;">

                                                                           <span style="color: #ffffff; font-weight: 300;">
                                                                              <a style="color: #ffffff; text-align:center;text-decoration: none;" href="{{ URL::to('admin/dealer/requests') }}">požiadavka pohľad</a>
                                                                           </span>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                    </td>
                                 </tr>
                                 <!-- /button -->
                                 <!-- Spacing -->
                                 <tr>
                                    <td width="100%" height="20"></td>
                                 </tr>
                                 <!-- Spacing -->
                                 <!-- content -->
                                 <tr>
                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #666666; text-align:left;line-height: 24px;" st-content="rightimage-paragraph">
                                       <P>Náš tip!</P>
                                        <P>Cenovú ponuku je najlepšie poslať ihneď. Zanechá to dobrý dojem a môže pomôcť k úspešnému predaju. Pošlite aj sprievodnú správu.
                                          </P>
                                        <P>Všetci naši užívatelia sú osoby s overenými emailovými adresami, aby sme predišli nežiadúcemu spamu.</P>

                                        <P>Ak máte nejaké otázky, tak sa na nás obráďte na poradna@autoogle.eu.</P>
                                        <p>Prajeme úspešný predaj auta.</p>
                                        <p style="font-style:italic;">Spoločnosť Autoogle</p>

                                    </td>
                                 </tr>
                                 <!-- end of content -->
                              </tbody>
                           </table>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </td>
         </tr>
      </tbody>
   </table>
</div>
@endsection
