@extends('template')

@section('content')
<div class="container">

  @include('elements.wizard_header')

  @include('elements.step-bar',array('curStep' => 3))

  <div class="row">
  	<div class="col-md-12">
    	<h2>{{ Lang::get('messages.wizard_btn_select_engine') }}</h2>
    </div>
  </div>

  <div class="row">
  	<div class="col-md-4">
    	<strong>{{ Lang::get('messages.label_text_fuel') }}</strong>
        <br/>
        <div id="fuel" class="btn-group" role="group" aria-label="...">
		  <button type="button" class="btn btn-default active" data-id="all">{{ Lang::get('messages.btn_text_any') }}</button>
		  <button type="button" class="btn btn-default" data-id="1">{{ Lang::get('messages.btn_text_petrol') }}</button>
		  <button type="button" class="btn btn-default" data-id="2">{{ Lang::get('messages.btn_text_diesel') }}</button>
		</div>
    </div>
    <div class="col-md-4">
    	<strong>{{ Lang::get('messages.label_text_gearbox') }}</strong>
        <br/>
        <div id="gears" class="btn-group" role="group" aria-label="...">
		  <button type="button" class="btn btn-default active" data-id="all">{{ Lang::get('messages.btn_text_any') }}</button>
		  <button type="button" class="btn btn-default" data-id="1">{{ Lang::get('messages.btn_text_manual') }}</button>
		  <button type="button" class="btn btn-default" data-id="2">{{ Lang::get('messages.btn_text_automatic') }}</button>
		</div>
    </div>
    <div class="col-md-4">
    </div>
  </div>

  <div class="row">
  	<div class="col-md-12">
    	<hr />
    </div>
  </div>

  <!-- Makes -->
  <div class="row">
  	<div class="col-sm-3 col-xs-9">
    	<h4>{{ Lang::get('messages.tbl_header_engine') }}</h4>
    </div>
    <div class="col-sm-2 hidden-xs">
    	<h4>{{ Lang::get('messages.tbl_header_bhp') }}</h4>
    </div>
    <div class="col-sm-2 hidden-xs">
    	<h4>{{ Lang::get('messages.tbl_header_mpg') }}</h4>
    </div>
    <div class="col-sm-2 col-xs-2">
    	<h4>{{ Lang::get('messages.wizard-engine-price-from') }}</h4>
    </div>
  </div>
  <div class="" id="engines">
	@if(sizeof($engines) > 0)
	    @foreach($engines as $e)
	    <a href="{{ URL::to( Lang::get('routing.select-trim').'/'.$e->id) }}" class="no-style">
	       <div class="row gears-{{ $e->gearbox->id }} fuel-{{ $e->fueltype->id }}" style="margin-top:10px;	">
            <div class="col-sm-3 col-xs-9">
                <p>
                	<strong>{{ $e->name }}</strong><br />
                	{{ $e->doors }} {{ Lang::get('messages.label_text_doors') }} {{ $e->gearbox->getLocalisedName() }} {{ $e->fueltype->getLocalisedName()}}
                </p>
            </div>
            <div class="col-sm-2 hidden-xs">
            	<p>{{ round($e->horsepower/1.36) }}kW / {{ $e->horsepower }}k</p>
            </div>
            <div class="col-sm-2 hidden-xs">
            	<p>{{ $e->consumption }}</p>
            </div>
            <div class="col-sm-2 col-xs-2">
            	<p>{{ $e->trim->get(0)->getLocalizedPrice($e->getMinPrice(),false) }}</p>
            </div>
            <div class="col-xs-3 hidden-xs" align="right">
            	<p><span class="btn btn-primary">{{ Lang::get('messages.wizard_btn_select_engine') }} <span class="glyphicon glyphicon-chevron-right"></span></span></p>
            </div>
            <div class="col-xs-1 visible-xs-block" align="right">
            	<span class="glyphicon glyphicon-chevron-right"></span>
            </div>
            <div class="col-xs-12" style="border-bottom:#CCC solid 1px;">
            </div>
          </div>
          </a>
	    @endforeach
	@endif
    </div>
  <!-- ./Makes -->
</div>
@endsection

@section('script')
<script type="text/javascript">
	var filters = { "gears":"all","fuel":"all" };

	$(function() {
		$('#gears button').click(function(e) {
			e.preventDefault();
			$('#gears button').removeClass('active');
			$(this).addClass('active');

			filters.gears = $(this).data("id");
			applyFilter();
		});

		$('#fuel button').click(function(e) {
			e.preventDefault();
			$('#fuel button').removeClass('active');
			$(this).addClass('active');

			filters.fuel = $(this).data("id");
			applyFilter();
		});
	});

	function applyFilter() {
		var list = $('#engines');
		console.log(filters.gears);
		if(filters.gears == "all" && filters.fuel == "all") {
			$('.row',list).slideDown();
		}
		else if(filters.gears == "all") {
			list.find(".row.fuel-" + filters.fuel + "").slideDown();
			list.find(".row:not(.fuel-" + filters.fuel + ")").slideUp();
		}
		else if(filters.fuel == "all") {
			list.find(".row.gears-" + filters.gears + "").slideDown();
			list.find(".row:not(.gears-" + filters.gears + ")").slideUp();
		}
		else {
			list.find(".row.gears-" + filters.gears + ".fuel-" + filters.fuel).slideDown();
			list.find(".row:not(.gears-" + filters.gears + ".fuel-" + filters.fuel+")").slideUp();
		}

	}
</script>
@endsection
