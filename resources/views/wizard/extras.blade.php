@extends('template')

@section('content')
<div class="container">
  @include('elements.wizard_header')
  
  @include('elements.step-bar',array('curStep' => 4))
  
  <div class="row">
    <div class="col-md-12">
      <h2>{{ Lang::get('messages.wizard_select_extras') }}</h2>
    </div>
  </div>
  
  <form id="extras_form" method="post" action="{{ URL::to('finish') }}">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  
  <div class="row">
  	<div class="col-md-4 col-md-offset-8" align="right">
    	<button class="btn btn-primary">{{ Lang::get('messages.btn_text_configure_later') }} <span class="glyphicon glyphicon-chevron-right"></span></button>
    </div>
  </div>
  
  <div class="row">
  	<div class="col-md-12">
    <br />
  <div class="panel-group" id="extras" role="tablist" aria-multiselectable="true">
  	
    @foreach($category as $i => $c)
    	@if(sizeof($c->items) > 0)
	    <div class="panel panel-default">
	      <div class="panel-heading" role="tab" id="headingOne">
	        <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#extras" href="#{{ str_replace(' ','_',$c->getLocalisedName()) }}" aria-expanded="true" class="{{ $i != 0 ? 'collapsed' : '' }}" aria-controls="collapseOne"> {{ $c->getLocalisedName() }} <span class="pull-right"><span class="glyphicon glyphicon-chevron-down"></span></span></a> </h4>
	      </div>
	      <div id="{{ str_replace(' ','_',$c->getLocalisedName()) }}" class="panel-collapse collapse {{ $i == 0 ? 'in' : '' }}" role="tabpanel" aria-labelledby="headingOne">
	        <div class="panel-body extras">
            	@foreach($c->items as $i)
                 @foreach($i->trim as $t)
                	@if($c->type == 0)
                    <div class="radio">
					  <label for="{{ $c->getLocalisedName().' '.+$i->id }}">
                      	<table class="table table-hover">
                        	<tr>
                            	<td class="extras-line">
                                	<input type="radio" name="{{ $c->getLocalisedName() }}" id="{{ $c->getLocalisedName().' '.+$i->id }}" 
                        	value="{{ $i->id }}" data-rrp="{{ $t->pivot->price }}">
                           		</td>
                                <td class="extras-radio">
                                	<span class="box">
                                    	&nbsp;
                                    </span>
                                </td>
                                <td class="extras-name">{{ $i->getLocalisedName() }}</td>
                                <td class="extras-price">{{ Lang::get('messages.currency') }}{{ $t->pivot->price }}</td>
                            </tr>
                        </table>
					  </label>
					</div>
                    @else
                    <div class="checkbox">
					    <label for="{{ $c->getLocalisedName().' '.+$i->id }}">
                        	<table class="table table-hover">
	                        	<tr>
	                            	<td class="extras-line">
	                                	<input type="checkbox" value="{{ $i->id }}" name="{{ $c->getLocalisedName() }}[]" 
	                            	id="{{ $c->getLocalisedName().' '.+$i->id }}" data-rrp="{{ $t->pivot->price  }}">
	                           		</td>
	                                <td class="extras-checkbox">
	                                	<span class="box">
	                                    	<span class="glyphicon glyphicon-ok">&nbsp;</span>
	                                    </span>
	                                </td>
	                                <td class="extras-name">{{ $i->getLocalisedName() }}</td>
	                                <td class="extras-price">{{ Lang::get('messages.currency') }}{{ $t->pivot->price }}</td>
	                            </tr>
	                        </table>
						</label>
					</div>
                    @endif
                  @endforeach
                @endforeach
            </div>
	      </div>
	    </div>
	@endif
    @endforeach
    
  </div>
  </div>
  </div>
  
  <div class="row">
  	<div class="col-md-4 col-md-offset-8">
    	<strong>{{ $engine->name }}</strong> {{ Lang::get('messages.currency') }}<span id="carRRP">{{ $engine->trim[0]->pivot->price }}</span> <br />
        <strong>Extras</strong> {{ Lang::get('messages.currency') }}<span id="extrasRRP">0</span> <br/>
        <div>
        	<strong>Total</strong> {{ Lang::get('messages.currency') }}<span id="totalRRP">{{ $engine->trim[0]->pivot->price }}</span>
        </div>
    </div>
  </div>
  <div class="row">
  	<div class="col-md-4 col-md-offset-8" align="right">
    	<button class="btn btn-primary">{{ Lang::get('messages.btn_text_finish') }} <span class="glyphicon glyphicon-chevron-right"></span></button>
    </div>
  </div>
  
  </form>
</div>
@endsection

@section('script')
<script type="text/javascript">
	$(function() {
		$('#extras_form input').change(function(e) {
			updateChecks();
			
			var val = 0;
			$('#extras_form input:checked').each(function(i) {
				val += $(this).data('rrp');
			});
			
			$('#extrasRRP').fadeOut('fast',function() {
				$(this).html(val).fadeIn('fast');
			});
			$('#totalRRP').fadeOut('fast',function() {
				$(this).html(parseInt($('#carRRP').html()) + val).fadeIn('fast');
			});
		});
		
		updateChecks();
		$( window ).resize(function() {
			screenChk();
		});
		screenChk();
	});
	
	function screenChk() {
		if($( window ).width() > 768) {
			$('#extras a[data-toggle="collapse"]').data('parent',null);
		}
		else {
			$('#extras a[data-toggle="collapse"]').data('parent','#extras');
		}
	}
	
	function updateChecks() {
		$('#extras_form input').parents("label").removeClass('selected');
		$('#extras_form input:checked').parents("label").addClass('selected');
	}
</script>
@endsection