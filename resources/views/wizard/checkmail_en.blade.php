@extends('template')

@section('content')
<div class="container" style="padding-top:20px;">

  <!-- Thank you -->
  <div class="row">
  	<div class="col-md-12 text-center">
		<h1 class="text-success text-center"><i class="glyphicon glyphicon-ok"></i> <br/>Verify your email</h1>
		<p>Thank you, please check your address for our verification email.</p>
		<p>Click on the button found in the email</p>
	</div>
	<div class="col-md-4">

	</div>
	<div class="col-md-4">

	</div>
  </div>
  <!-- ./Thank you -->
</div>
@endsection
