@extends('template')

@section('content')
<div class="container" style="padding-top:20px;">

  <!-- Thank you -->
  <div class="row">
  	<div class="col-md-12 text-center">
		<h1 class="text-success text-center"><i class="glyphicon glyphicon-ok"></i> <br/>Submission Completed</h1>
		<p>Thank you, we will now send your request to our very best dealers to get the best deals for you.</p>
		<p>You will be contacted when our dealers reply, meanwhile while not add another car?</p>
	</div>
	<div class="col-md-4">

	</div>
	<div class="col-md-4 text-center">
		<a href="{{ URL::to(Lang::get('routing.select-make')) }}" class="btn btn-primary">Select Make</a>
	</div>
	<div class="col-md-4">

	</div>
  </div>
  <!-- ./Thank you -->
</div>
@endsection
