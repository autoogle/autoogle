@extends('template')

@section('content')
<?php $cols = sizeof($models) / 3; ?>
<div class="container">
  
  @include('elements.wizard_header')
  
  @include('elements.step-bar',array('curStep' => 2))
  
  <div class="row">
  	<div class="col-md-12">
    	<h2>{{ Lang::get('messages.wizard_btn_select_model') }}</h2>
    </div>
  </div>
  
  <!-- Makes -->
  <div class="row">
  	@if(sizeof($models) > 0)
        @if($cols < 1)
        	<div class="col-xs-12 col-md-4" align="center">
            	<ul class="nav nav-pills nav-stacked">
    			@foreach($models as $m)
                	<li><a href="{{ URL::to( Lang::get('routing.select-engine').'/'.$m->id) }}">{{ $m->name }}</a></li>
                @endforeach
                </ul>
    		</div>
        @else
        	@foreach($models->chunk(ceil($cols)) as $c)
            	<div class="col-xs-12 col-md-4" align="center">
                	<ul class="nav nav-pills nav-stacked">
	    			@foreach($c as $m)
	                	<li><a href="{{ URL::to( Lang::get('routing.select-engine').'/'.$m->id) }}">{{ $m->name }}</a></li>
	                @endforeach
	                </ul>
                </div>
            @endforeach
        @endif
    @endif
  </div>
  <!-- ./Makes -->
</div>
@endsection