@extends('template')

@section('content')
<div class="container">
  
  @include('elements.wizard_header')
  
  @include('elements.step-bar',array('curStep' => 4))
  
  <div class="row">
  	<div class="col-md-12">
    	<h2>{{ Lang::get('messages.wizard_btn_select_trim') }}</h2>
    </div>
  </div>
  
  <!-- Trims -->
  <div class="" id="engines">
	@if(sizeof($trims) > 0)
	    @foreach($trims as $i => $t)
	    <a href="{{ URL::to( Lang::get('routing.finish').'/'.$t->id) }}" class="no-style">
	       <div class="row" style="margin-top:10px;">
            <div class="col-sm-7 col-xs-11">
				<div class="pull-right visible-xs-block">{{ Lang::get('messages.currency') }}{{ $t->pivot->price }}</div>
                
                @if($t->getLocalisedDescription() != null && $t->getLocalisedDescription() != '')
                <div class="txt-collaspe">
                	<strong>{{ $t->name }}</strong><br />
                	{!! $t->getLocalisedDescription() !!}
                </div>
                @else
                	<strong>{{ $t->name }}</strong><br />
                @endif
            </div>
	    <div class="col-sm-2 hidden-xs">
            	<p>
            	@if($i == 0)
            		<strong>{{ Lang::get('messages.wizard-trims-rrp-price') }}</strong><br />
            	@endif
            	
            	{{ $t->getLocalizedPrice($t->pivot->price,false) }}
            	</p>
            </div>
            <div class="col-xs-3 hidden-xs" align="right">
            	<p><span class="btn btn-primary">{{ Lang::get('messages.wizard_btn_select_trim') }} <span class="glyphicon glyphicon-chevron-right"></span></span></p>
            </div>
            <div class="col-xs-1 visible-xs-block" align="right">
            	<span class="glyphicon glyphicon-chevron-right"></span>
            </div>
            @if($t->getLocalisedDescription() != null && $t->getLocalisedDescription() != '')
			<div class="col-xs-12">
				<span class="more-btn">More</span>
			</div>
	    @endif
            <div class="col-xs-12" style="border-bottom:#CCC solid 1px;">
            </div>
          </div>
          </a>
	    @endforeach
	@endif
    </div>
  <!-- ./Trims -->
</div>
@endsection

@section('script')
<script type="text/javascript">
	$(function() {
		$('.more-btn').click(function(e) {
			e.preventDefault();
			$(this).parents('a').toggleClass('expanded');
			if($(this).parents('a').is('.expanded')) $(this).text('Less');
			else $(this).text('More');
		});
	});
</script>
@endsection