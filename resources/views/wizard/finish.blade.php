@extends('template')

@section('content')
<div class="container">

  @include('elements.wizard_header')

  @include('elements.step-bar',array('curStep' => 5))

  <div class="row">
  	<div class="col-md-12">
    	<h2>{{ Lang::get('messages.wizard_btn_finish') }}</h2>
    </div>
  </div>

  <div class="row">
  	<div class="col-md-12" align="center">
    	<p>{{ Lang::get('messages.loc_text_help') }}</p>
        <br />
    </div>
    <div class="col-sm-12 col-md-6 col-md-offset-3" align="center">
    	<form method="POST" action="{{ URL::to(Lang::get('routing.thank-you')) }}">
			{!! Form::token() !!}
    	<div id="postcode">
    		<input type="text" class="form-control" name="postcode" required placeholder="{{ Lang::get('messages.loc_text_enter_postcode') }}" />
			<span class="help-block"></span>
        	<a href="#" class="btn btn-primary btn-block">{{ Lang::get('messages.loc_btn_final_step') }}</a>
        </div>
        <div id="email" style="display:none;">
        	<input type="text" data-valid="email" class="form-control" name="email" required placeholder="{{ Lang::get('messages.loc_text_enter_email') }}" />
			<span class="help-block"></span>
            <button class="btn btn-success btn-block">{{ Lang::get('messages.loc_text_show_me_offers') }}</button>
        </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	$(function() {
		$('#postcode a').click(function(e) {
			e.preventDefault();

			if(valid($('input[name=postcode]'))) {
				var pc = $('#postcode input').val();
				if(pc != null) {
					$('#postcode').fadeOut('fast',function() {
						$('#email').fadeIn('fast');
					});
				}
			}
		});

		$('form').submit(function(e) {
			if(!valid($('input[name=email]'))){
				e.preventDefault();
			}
		});
	});

	function valid(el) {
		var outer = el[0];
		var group = el.parent();
		var val = el.val();

		group.removeClass('has-error');

		var errors = [];
		var valid = true;

		if(outer.hasAttribute('required')) {
			if(val == null || val == '') {
				errors.push("{{ Lang::get('messages.validation_required') }}");
				valid = false;
			}
		}

		if(valid && el.data('valid') == 'email') {
			if(!validateEmail(val)) {
				errors.push("{{ Lang::get('messages.validation_email') }}");
				valid = false;
			}
		}

		if(!valid) {
			group.addClass('has-error');
			var list = $('<ul/>').addClass('list-unstyled');
			$.each(errors,function(i,itm) {
				list.append($('<li/>').html(itm));
			});
			$('.help-block',group).html("").append(list);
		}

		return valid;
	}

	function validateEmail(email) {
		var re = /^([\w-+]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		return re.test(email);
	}
</script>
@endsection
