@if(sizeof($requests) > 0)
  @if(sizeof($requests) == 1)
    <li><a href="{{ URL::to(Lang::get('routing.user').'/'.Lang::get('routing.user-dashboard')) }}">{{ Lang::get('users.my-offers') }}</a></li>
  @else
  <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Lang::get('users.my-offers') }} <span class="caret"></span></a>
        <ul class="dropdown-menu">
          @foreach($requests as $r)
            <li><a href="{{ URL::to(Lang::get('routing.user').'/'.Lang::get('routing.user-dashboard').'/'.$r->id) }}"><strong>{{ $r->engine->model->make->name }} {{ $r->engine->model->name }} </strong>{{ $r->trim->name }}</a></li>
          @endforeach
        </ul>
      </li>
  @endif

<li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Lang::get('users.my-account') }}<span class="caret"></span></a>
  <ul class="dropdown-menu">
      <li><a href="{{ URL::to('auth/logout') }}">{{ Lang::get('users.logout') }}</a></li>
  </ul>
</li>
@endif
