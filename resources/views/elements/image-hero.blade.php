<div class="image-hero">
	<div class="container text-center">
    	<div class="image-hero-content">
        	<div class="row">
            	<div class="col-md-12">
    		    	<h1 class="text-center">{{ Lang::get('messages.hme_hero_new_way') }}</h1>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-12">
                	<a href="{{ URL::to( Lang::get('routing.select-make')) }}" class="btn btn-primary btn-lg">{{ Lang::get('messages.hme_btn_select_car') }}</a>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-12">
      	            <button class="btn btn-default btn-lg">{{ Lang::get('messages.hme_btn_latest_deal') }}</button>
                </div>
            </div>
           	<div class="row">
            	<div class="col-md-12">
      	            <a id="wBtn" href="#works">{{ Lang::get('messages.hme_btn_how_it_works') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>