<?php $isUser = $m->from_id == Auth::id(); ?>
<li class="{{ $isUser ? 'left' : 'right' }} clearfix"><span class="chat-img {{ $isUser ? 'pull-left' : 'pull-right' }}">
    @if($isUser)
    <img src="http://placehold.it/50/55C1E7/fff&text={{ Lang::get('users.me') }}" alt="User Avatar" class="img-circle" />
    @else
    <img src="http://placehold.it/50/FA6F57/fff&text={{ $m->fromuser->getInitials() }}" alt="User Avatar" class="img-circle" />
    @endif
</span>
    <div class="chat-body clearfix">
        <div class="header">
            @if($isUser)
            <strong class="primary-font">{{ Lang::get('users.me') }}</strong>
            <small class="pull-right text-muted"><span class="glyphicon glyphicon-time"></span>{{ $m->created_at->diffForHumans() }}</small>
            @else
            <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>
                {{ $m->created_at->diffForHumans() }}</small>
            <strong class="pull-right primary-font">{{ $m->fromuser->name }}</strong>
            @endif
        </div>
        <p>
            {{ $m->message }}
        </p>
    </div>
</li>
