<p>AutoSoup is for people who just don't want to deal with the whole process - people who value their time far more than running around 20 different car dealerships to get their best price.</p>

<p>Use our website to build your new car and let dealers compete against each other to give you the best new car discount and delivery times on your car.</p>

<p>Within few hours, you can get upfront pricing information from our Certified Dealers and know how those prices compare to the current market.</p>