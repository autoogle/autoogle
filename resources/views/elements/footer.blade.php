<div class="container footer">
  <div class="row ">
    <div class="col-md-6">
      <h5 class="title">{{ Lang::get('messages.about-us') }}</h5>
      @include('elements.about_'.Lang::getLocale())
    </div>
    <div class="col-md-2">

    </div>
    <div class="col-md-4">
      <ul>
        <li class="title">{{ Lang::get('messages.navigation') }}</li>
        <li><a href="{{ URL::to( Lang::get('routing.select-make')) }}">{{ Lang::get('messages.wizard_btn_select_make') }}</a></li>
        <li><a href="#about">{{ Lang::get('messages.best_offers') }}</a></li>
        <li><a href="{{ URL::to( Lang::get('routing.car-reviews')) }}">{{ Lang::get('messages.car-reviews') }}</a></li>
		<li><a href="{{ URL::to( Lang::get('routing.faq')) }}">{{ Lang::get('messages.faq') }}</a></li>
      </ul>
    </div>
  </div>
</div>
