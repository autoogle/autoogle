<p>Autoogle je pre ľudí, ktorý nechcú prechádzať celým procesom pri kúpe nového auta - ľudí ktorý vedia využiť ich čas lepšie ako sa naháňať po auto dealeroch aby zjednali čo najlepšiu cenu.</p>
<p>Použite náš jednoduchý auto configurátor and nechajte dealerov aby suťažili medzi sebou o najlepšiu cenovú ponuku na vaše nové auto.</p>
<p>V priebehu 24 hodín dostanete konkrétne ponuky od piatich lokálnych dealerov, ktoré sú v priemere o 1500 euro lepšie ako štandartné predajné ceny.</p>
