<!-- Progress Bar -->
  <div class="row bs-wizard" style="border-bottom:0;">
    <div class="col-xs-2 bs-wizard-step {{ $curStep == 1 ? 'active' : ($curStep > 1 ? 'complete' : 'disabled') }}">
      <div class="text-center bs-wizard-stepnum">{{ Lang::get('messages.wizard_nav_select_make') }}</div>
      <div class="bs-wizard-step-inner">
	      <div class="progress">
    	    <div class="progress-bar"></div>
    	  </div>
      	<a href="{{ URL::to( Lang::get('routing.select-make')) }}" class="bs-wizard-dot"></a>
      </div>
    </div>
    <div class="col-xs-2 bs-wizard-step {{ $curStep == 2 ? 'active' : ($curStep > 2 ? 'complete' : 'disabled') }}">
      <!-- complete -->
      <div class="text-center bs-wizard-stepnum">{{ Lang::get('messages.wizard_select_model') }}</div>
      <div class="bs-wizard-step-inner">
	      <div class="progress">
    	    <div class="progress-bar"></div>
    	  </div>
      	<a href="{{ URL::to( Lang::get('routing.select-model')) }}" class="bs-wizard-dot"></a>
      </div>
    </div>
    <div class="col-xs-3 bs-wizard-step {{ $curStep == 3 ? 'active' : ($curStep > 3 ? 'complete' : 'disabled') }}">
      <!-- complete -->
      <div class="text-center bs-wizard-stepnum">{{ Lang::get('messages.wizard_select_engine') }}</div>
      <div class="bs-wizard-step-inner">
	      <div class="progress">
    	    <div class="progress-bar"></div>
    	  </div>
      	<a href="{{ URL::to( Lang::get('routing.select-engine')) }}" class="bs-wizard-dot"></a>
      </div>
    </div>
    <div class="col-xs-3 bs-wizard-step {{ $curStep == 4 ? 'active' : ($curStep > 4 ? 'complete' : 'disabled') }}">
      <!-- active -->
      <div class="text-center bs-wizard-stepnum">{{ Lang::get('messages.wizard_select_trims') }}</div>
      <div class="bs-wizard-step-inner">
	      <div class="progress">
    	    <div class="progress-bar"></div>
    	  </div>
      	<a href="{{ URL::to( Lang::get('routing.select-trim')) }}" class="bs-wizard-dot"></a>
      </div>
    </div>
    <div class="col-xs-2 bs-wizard-step {{ $curStep == 5 ? 'active' : ($curStep > 5 ? 'complete' : 'disabled') }}">
      <!-- active -->
      <div class="text-center bs-wizard-stepnum">{{ Lang::get('messages.wizard_finished') }}</div>
      <div class="bs-wizard-step-inner">
	      <div class="progress">
    	    <div class="progress-bar"></div>
    	  </div>
      	<a href="{{ URL::to( Lang::get('routing.finish')) }}" class="bs-wizard-dot"></a>
      </div>
    </div>
  </div>
  <!-- ./Progress Bar -->