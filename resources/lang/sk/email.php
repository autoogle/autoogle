<?php

return [
  'confirm_subject' => 'Welcome to Autoogle, please verify your account',
  'new_offer_subject' => 'Nová ponuka na požiadanie',
  'offer_updated_email' => 'Ponuka bola aktualizovaná',
  'new_request_subject' => 'Máte novú cenovú žiadosť o cenovú ponuku',
  'offer_accepted_subject' => 'Gratulujeme, vaša cenová ponuka bola akceptovaná.',
  'user_replied_subject' => 'Užívatel reagoval na vašu cenovú ponuku',
  'unsub_text' => 'Ak si neprajete dostávať od nás informácie, prosíme ',
  'unsub_btn' => 'odhláste sa',
  'nav_text_home' => 'DOMOV',
  'nav_text_new_request' => 'NOVÉ PONUKY',
  'nav_text_faq' => 'ČASTÉ OTÁZKY',
];
