<?php

return [
    'select-make' => 'vybrat-znacku',
	'select-model' => 'vybrat-model',
	'select-engine' => 'vybrat-motor',
	'select-trim' => 'vybrat-vybavu',
	'finish' => 'koniec',
	'thank-you' => 'ďakujeme',
  'car-reviews' => 'recenzie-aút',
  'faq' => 'najčastejšie-otázky',
  	'tnc' => 'podmienky-používania',
  	'privacy' => 'ochrana-údajov',

    'user' => 'užívatel',
    	'user-dashboard' => 'ovládací-panel',
    	'user-offer' => 'ponuka',
    	'user-confirm' => 'potvrdiť',
];
