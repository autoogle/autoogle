<?php

return [
  'your-offers' => 'Vaše ponuky',
  'your-offer' => 'Vaša ponuka',
  'best-offer'  => 'Najlepšia ponuka',
  'miles-away' => 'kilometrov vzdialené',
  'saving' => 'zľava',
  'no-offers' => 'Ešte neboli poslané žiadne ponuky. ',
  'no-requests' => 'Žiadne žiadosti o cenovú ponuku, vytvorte si novú',
  'request-info' => 'Požiadať o informácie',
  'created' => 'Vytvorené',
  'create-password-desc' => 'Vytvorte si heslo, aby ste sa mohli prihlásiť priamo',
  'create-password-now' => 'Teraz si vytvorte heslo',
  'new-request' => 'Nová žiadosť o cenu',
  'new-request-desc' => 'Nenašli ste to čo ste hladali? Prečo nepožiadať o novú cenovú ponuku.',
  'finance-option' => 'Financovanie',
  'rrp' => 'Predajná cena: ',
  'finance-not-available' => 'Financovanie nie je k dispozícii',
  'your-car' => 'Vaše Auto',
  'no-messages' => 'Žiadne správy',
  'send' => 'Poslať',
  'me' => 'Ja',
  'details' => 'Detaily',
  'dealer' => 'Dealer',
  'messages' => 'Správy',
  'accept-offer' => 'Prijať ponuku',
  'free-delivery' => 'Doručenie zadarmo',
  'full-tank' => 'Plná nádrž zadarmo',
  'my-offers' => 'Moje ponuky',
  'my-account' => 'Môj účet',
  'logout' => 'Odhlásiť sa',
];
