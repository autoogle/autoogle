<?php

return [
    	'select-make' => 'select-make',
	'select-model' => 'select-model',
	'select-engine' => 'select-engine',
	'select-trim' => 'select-trim',
	'finish' => 'finish',
	'thank-you' => 'thank-you',
    	'car-reviews' => 'car-reviews',
	'faq' => 'frequently-asked-questions',
  	'tnc' => 'terms',
  	'privacy' => 'privacy',

	   'user' => 'user',
  	'user-dashboard' => 'dashboard',
  	'user-offer' => 'offer',
  	'user-confirm' => 'confirm',

];
