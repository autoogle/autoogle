<?php

return [
  'confirm_subject' => 'Welcome to Autoogle, please verify your account',
  'new_offer_subject' => 'You have a new offer on your request',
  'offer_updated_email' => 'An offer has been updated',
  'new_request_subject' => 'You have a new request',
  'offer_accepted_subject' => 'Congratulations, offer has been accepted',
  'user_replied_subject' => 'User replied to offer',
  'unsub_text' => 'If you don\'t want to receive updates. please  ',
  'unsub_btn' => 'unsubscribe',
  'nav_text_home' => 'HOME',
  'nav_text_new_request' => 'NEW REQUEST',
  'nav_text_faq' => 'FAQ',
];
